function [  ] = CollectAll( runs )
% This function collects all the run tables saved in the results folder and
% appends them together. It should be fault tollerant because there may be
% runs of particular variant models that cannot be completed and thus will
% not generate data tables.

    varPrefs = {'Var1V_','Var2I_','Var3VI_','Var4VT_','Var5VIT_'};
    
    cd Runs
    % Compile experiments 1 and 2 accross runs
    for v=1:5
        
        tableAll = [];
        
        for r=1:runs

            try
                
                fileName = ['Run' num2str(r) varPrefs{v} 'exp12.txt'];
                
                table = readtable(fileName);

                if isempty(tableAll)
                      tableAll = table;
                else
                      tableAll = [tableAll; table];
                end

                disp(['Found ' fileName])

            catch
                %disp(['Could Not Find ' 'Run' num2str(r) varPrefs{v} '_exp12'])
            end

        end
        

        % Save compilation table
        if ~isempty(tableAll)
            writetable(tableAll, [varPrefs{v} 'exp12.txt']);
        end
    end
    
    
    % Compile experiment 3 accross runs
    for v=1:5
        
        tableAll = [];
        
        for r=1:runs

            try
                
                fileName = ['Run' num2str(r) varPrefs{v} 'exp3.txt'];
                
                table = readtable(fileName);

                if isempty(tableAll)
                      tableAll = table;
                else
                      tableAll = [tableAll; table];
                end

                disp(['Found ' fileName])

            catch
                %disp(['Could Not Find ' 'Run' num2str(r) varPrefs{v} '_exp12'])
            end

        end
        

        % Save compilation table
        if ~isempty(tableAll)
            writetable(tableAll, [varPrefs{v} 'exp3.txt']);
        end
    end
    

    cd ..

end

