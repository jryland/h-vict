function [  ] = Build(  )
% put custom code for creating a specific network here, using
% trainLayerBatch, and runLayerOn. The network stucture and input
% information should be defined by creating networkInfo, and inputInfo
% structs as shown 
% 
% ::::::::::::::::::::::::::EXAMPLE:::::::::::::::::::::::
%     networkInfo.baseRez;      % the rezolution of the base feature representations
%     networkInfo.filtRez;      % the rezolution of the filters
%     networkInfo.numChan;      % the number of input channels
%     networkInfo.numOrients;   % the number of orientations to project to
%     networkInfo.numScales;    % the number of scales to project to
%     networkInfo.numRep;       % the number of high level feature representations
%     networkInfo.recodeStyle   % the output style [vector, multi-chan] 
%     networkInfo.startRate     % this is the initial learning rate
%     networkInfo.stepUpTolerance = 1.1; % tolerance for raising error
%     
%     inputInfo.inRez;          % the rezolution to scale the stimuli to be
%     inputInfo.inNum;          % the number of stimuli to be used
%     inputInfo.inStyle;        % the style of the input images (grey, multi-chan, vector)
%     inputInfo.inSample;       % how the images are sampled
%     inputInfo.inHidden;       % whether to look in hidden image folders or original stimuli folder 
% ::::::::::::::::::::::::EXAMPLE END:::::::::::::::::::::

    %Setup Layer 1 and run stimuli

    
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez =  13;      % the rezolution of the base feature representations
    networkInfo.filtRez = 11;      % the rezolution of the filters
    networkInfo.numChan = 1;      % the number of input channels
                                  % if input is vector coded 4*numRep
                                  % input
    networkInfo.numOrients = 12;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 6;       % the number of high level feature representations
    networkInfo.startRate = .4;
    networkInfo.rRate = .1; %1*10^14;
    networkInfo.oRate = .0001; %.00005; %1000000;
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 4000; %1.10;
    
    inputInfo.inRez = 40;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50;          % the number of stimuli to be used
    inputInfo.inStyle = 'grey';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'original';
    inputInfo.numOrients = 1;          % the number of variations of each

    inputFolderName = 'Stimuli_Pack'; %'Stimuli_SimpleTest'; 
    paramFileName = 'layer1_Param.mat';
    outFolderName = 'layer1_HiddenImages';
    
    % Train on multiple orientations
    inputInfo.inRez = 40;
    inputFolderName = 'Stimuli_Varied';
    inputInfo.inNum = 10*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle =  'ReLu';%'full-sparsity';
    
    %trainLayerAnneal(inputFolderName, inputInfo, networkInfo, paramFileName, 500);
    
    % Test on more stimuli
    inputInfo.inRez = 40;
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    %networkInfo.sparseStyle = 'ReLu';
    
    %saveOut = 'yes';
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);

    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');

    


%     % Setup Layer 2 and run on stimuli
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez = 13;       % the rezolution of the base feature representations
    networkInfo.filtRez = 11;      % the rezolution of the filters
    networkInfo.numChan = 6;      % the number of input channels
    networkInfo.numOrients = 12;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 10;       % the number of high level feature representations
    networkInfo.startRate = .5;    % this is the initial learning rate
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 400;

    
    inputInfo.inRez = 40;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50*12;          % the number of stimuli to be used
    inputInfo.inStyle = 'multi-chan';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'hidden';      % original or hidden, tells program where to look for the images
    
    inputFolderName = 'layer1_HiddenImages';
    paramFileName = 'layer2_Param.mat';
    outFolderName = 'layer2_HiddenImages';
    
    
    % Train on multiple orientations
    inputInfo.inRez = 40;
    inputInfo.inNum = 10*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'Relu';
    
    %trainLayerAnneal(inputFolderName, inputInfo, networkInfo, paramFileName, 500);
    
    % Test on more stimuli
    inputInfo.inRez = 40;
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu';
    
    %saveOut = 'yes';
    
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);
     
    layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
    % best conjunction filtering
    %inputFolderName = 'layer2_HiddenImages';
    %outFolderName = 'layer2_ConjunctionImages';
    %conjunctionProcess(inputFolderName, inputInfo, outFolderName, 20);
    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
    
    
    
    
    
    % Setup Layer 3 and run on stimuli
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 36;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez = 13;      % the rezolution of the base feature representations
    networkInfo.filtRez = 11;      % the rezolution of the filters
    networkInfo.numChan = 4;      % the number of input channels
    networkInfo.numOrients = 12;
    networkInfo.numScales = 2; 
    networkInfo.numRep = 14;       % the number of high level feature representations
    networkInfo.startRate = .5;    % this is the initial learning rate
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 1.01;

    
    inputInfo.inRez = 60;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 3 *12;          % the number of stimuli to be used
    inputInfo.inStyle = 'multi-chan';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'hidden';      % original or hidden, tells program where to look for the images
    
    inputFolderName = 'layer2_HiddenImages';
    paramFileName = 'layer3_Param.mat';
    outFolderName = 'layer3_HiddenImages';
    
    
    % Train on multiple orientations
    inputInfo.inRez = 40;
    inputInfo.inNum = 3*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'full-sparsity';
    
    testOnline(inputFolderName, inputInfo, networkInfo, paramFileName, 5);
    
    % Test on more stimuli
    inputInfo.inRez = 40;
    inputInfo.inNum = 50*12;
    inputInfo.numOrients = 12;
    %networkInfo.sparseStyle = 'ReLu';
    
    
    %saveOut = 'yes';
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);
    
    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
    %displayFilters(paramFileName, inputInfo);


    


end

