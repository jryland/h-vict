

function [  ] = BuildOnline_Multi_Rez()
% put custom code for creating a specific network here, using
% trainLayerBatch, and runLayerOn. The network stucture and input
% information should be defined by creating networkInfo, and inputInfo
% structs as shown 
% 
% ::::::::::::::::::::::::::EXAMPLE:::::::::::::::::::::::
%     networkInfo.baseRez;      % the rezolution of the base feature representations
%     networkInfo.filtRez;      % the rezolution of the filters
%     networkInfo.numChan;      % the number of input channels
%     networkInfo.numOrients;   % the number of orientations to project to
%     networkInfo.numScales;    % the number of scales to project to
%     networkInfo.numRep;       % the number of high level feature representations
%     networkInfo.recodeStyle   % the output style [vector, multi-chan] 
%     networkInfo.startRate     % this is the initial learning rate
%     networkInfo.stepUpTolerance = 1.1; % tolerance for raising error
%     
%     inputInfo.inRez;          % the rezolution to scale the stimuli to be
%     inputInfo.inNum;          % the number of stimuli to be used
%     inputInfo.inStyle;        % the style of the input images (grey, multi-chan, vector)
%     inputInfo.inSample;       % how the images are sampled
%     inputInfo.inHidden;       % whether to look in hidden image folders or original stimuli folder 
% ::::::::::::::::::::::::EXAMPLE END:::::::::::::::::::::

    %Setup Layer 1A and run stimuli Rez 60

    
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez =  15;      % the rezolution of the base feature representations
    networkInfo.filtRez = 13;      % the rezolution of the filters
    networkInfo.numChan = 1;      % the number of input channels
                                  % if input is vector coded 4*numRep
                                  % input
    networkInfo.numOrients = 12;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 4;       % the number of high level feature representations
    networkInfo.startRate = 30;
    networkInfo.rRate = .1; %1*10^14;
    networkInfo.oRate = .005; %.00005; %1000000;
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 4000; %1.10;
    
    networkInfo.spatialFreq = .75; 
    
    
    inputInfo.inRez = 50;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50;          % the number of stimuli to be used
    inputInfo.inStyle = 'grey';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'original';
    inputInfo.numOrients = 1;          % the number of variations of each

    inputFolderName = 'Stimuli_Pack'; %'Stimuli_SimpleTest'; 
    paramFileName = 'layer1A_Param.mat';
    outFolderName = 'layer1A_HiddenImages';
    
    % Train on multiple orientations
    inputInfo.inRez = 50; % 40
    inputFolderName = 'Stimuli_Varied';
    inputInfo.inNum = 40*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle =  'ReLu'; % 'combo';% 'full-sparsity'; %'ReLu'; %'none';
    
    %trainLayerOnline(inputFolderName, inputInfo, networkInfo, paramFileName, 250);
    
    % Test on more stimuli
    inputInfo.inRez = 50; % 40
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu'; % change back to 'none' 
    
    saveOut = 'yes';
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);

    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');

   
%Setup Layer 1B and run stimuli Rez 60

    
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez =  15;      % the rezolution of the base feature representations
    networkInfo.filtRez = 13;      % the rezolution of the filters
    networkInfo.numChan = 1;      % the number of input channels
                                  % if input is vector coded 4*numRep
                                  % input
    networkInfo.numOrients = 12;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 4;       % the number of high level feature representations
    networkInfo.startRate = 30;
    networkInfo.rRate = .1; %1*10^14;
    networkInfo.oRate = .005; %.00005; %1000000;
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 4000; %1.10;
    
    networkInfo.spatialFreq = 1.5; 
    
    
    inputInfo.inRez = 50;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50;          % the number of stimuli to be used
    inputInfo.inStyle = 'grey';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'original';
    inputInfo.numOrients = 1;          % the number of variations of each

    inputFolderName = 'Stimuli_Pack'; %'Stimuli_SimpleTest'; 
    paramFileName = 'layer1B_Param.mat';
    outFolderName = 'layer1B_HiddenImages';
    
    % Train on multiple orientations
    inputInfo.inRez = 50; % 40
    inputFolderName = 'Stimuli_Varied';
    inputInfo.inNum = 40*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle =  'ReLu'; % 'combo';% 'full-sparsity'; %'ReLu'; %'none';
    
    %trainLayerOnline(inputFolderName, inputInfo, networkInfo, paramFileName, 250);
    
    % Test on more stimuli
    inputInfo.inRez = 50; % 40
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu'; % change back to 'none' 
    
    saveOut = 'yes';
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);

    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');


    %Setup Layer 1C and run stimuli Rez 60

    
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 3;
    
    networkInfo.baseRez =  15;      % the rezolution of the base feature representations
    networkInfo.filtRez = 13;      % the rezolution of the filters
    networkInfo.numChan = 1;      % the number of input channels
                                  % if input is vector coded 4*numRep
                                  % input
    networkInfo.numOrients = 12;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 4;       % the number of high level feature representations
    networkInfo.startRate = 30;
    networkInfo.rRate = .1; %1*10^14;
    networkInfo.oRate = .005; %.00005; %1000000;
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 4000; %1.10;
    
    networkInfo.spatialFreq = 2.25; 
    
    
    inputInfo.inRez = 50;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50;          % the number of stimuli to be used
    inputInfo.inStyle = 'grey';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'original';
    inputInfo.numOrients = 1;          % the number of variations of each

    inputFolderName = 'Stimuli_Pack'; %'Stimuli_SimpleTest'; 
    paramFileName = 'layer1C_Param.mat';
    outFolderName = 'layer1C_HiddenImages';
    
    % Train on multiple orientations
    inputInfo.inRez = 50; % 40
    inputFolderName = 'Stimuli_Varied';
    inputInfo.inNum = 40*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle =  'ReLu'; % 'combo';% 'full-sparsity'; %'ReLu'; %'none';
    
    %trainLayerOnline(inputFolderName, inputInfo, networkInfo, paramFileName, 250);
    
    % Test on more stimuli
    inputInfo.inRez = 50; % 40
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu'; % change back to 'none' 
    
    saveOut = 'yes';
    %runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);

    %layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');


    
    CombineLayers({'layer1A_HiddenImages','layer1B_HiddenImages', 'layer1C_HiddenImages'}, 'layer1ABC_HiddenImages');

    %layerPerformance('layer1ABC_HiddenImages', inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
    
    
    
% Sharpen Layer    
    
    networkInfo.repType = 'grid'
    networkInfo.numSpokes = 40;
    networkInfo.numLevels = 8;
    networkInfo.interval = 2;
    
    networkInfo.baseRez = 1;       % the rezolution of the base feature representations
    networkInfo.filtRez = 3;      % the rezolution of the filters
    networkInfo.numChan = 12;      % the number of input channels
    networkInfo.numOrients = 1;
    networkInfo.numScales = 1; 
    networkInfo.numRep = 15;       % the number of high level feature representations
    networkInfo.startRate = 300;    % this is the initial learning rate
    networkInfo.recodeStyle = 'multi-chan';
    networkInfo.stepUpTolerance = 400;

    networkInfo.spatialFreq = 1.2; 
    
    inputInfo.inRez = 30;          % the rezolution to scale the stimuli to be
    inputInfo.inNum = 50*12;          % the number of stimuli to be used
    inputInfo.inStyle = 'multi-chan';        % the style of the input images
    inputInfo.inSample = 'full';         % how the images are sampled
    inputInfo.inHidden = 'hidden';      % original or hidden, tells program where to look for the images
    
    inputFolderName = 'layer1ABC_HiddenImages';
    paramFileName = 'layer2_Param.mat';
    outFolderName = 'layer2_HiddenImages';
    
    
    % Train on multiple orientations
    inputInfo.inRez = 30;
    inputInfo.inNum = 40*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle =  'ReLu'; %'combo'; %'full-sparsity'; %'Relu';
    
    trainLayerOnline(inputFolderName, inputInfo, networkInfo, paramFileName, 250);
    
    % Test on more stimuli
    inputInfo.inRez = 30;
    inputInfo.inNum = 150*12;
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu';
    
    saveOut = 'yes';
    runLayerOn(inputFolderName, inputInfo, networkInfo, paramFileName, outFolderName, saveOut);
     
    layerPerformance(outFolderName, inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
    
    

end