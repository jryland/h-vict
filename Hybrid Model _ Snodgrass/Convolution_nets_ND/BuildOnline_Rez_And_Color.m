%_______________USED IN EXPERIMENTS____________________


function [ ] = BuildOnline_Rez_And_Color(  )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    % Test on more stimuli
    inputInfo.inRez = 50; % 40
    inputInfo.inNum = ones(1,260);
    inputInfo.numOrients = 12;
    networkInfo.sparseStyle = 'ReLu'; % change back to 'none' 
    
    
    CombineLayers({'layer2Col_HiddenImages','layer2_HiddenImages'}, 'layer1_Rez_Col_HiddenImages');
    
    layerPerformance('layer1_Rez_Col_HiddenImages', inputInfo.inNum, inputInfo.numOrients, 'match to upright');
    
end

