function [ hiddenIm] = CleanHiddenIm( hiddenIm, thresh )

     % clean up images with simple threshold
    cleanUpThresh = thresh;
    hiddenIm = hiddenIm.*(hiddenIm > (cleanUpThresh*max(hiddenIm(:))));


end

