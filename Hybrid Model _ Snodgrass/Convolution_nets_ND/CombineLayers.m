function [  ] = CombineLayers( layerFolders, outFolder )
% resolutions must be the same size.

    numLayers = length(layerFolders);
    
    cd '..//Hidden_Image_Sets/';
    
    layers{1} = cell(1,1);
    
    listing = {};
   
    for j = 1:numLayers
        cd(layerFolders{j});
        listing = dir();    
    
        layers{j} = cell(1,1);
    
        for i=3:length(listing)
            temp = struct2cell(load(listing(i).name));
            layers{j}{i-2} = temp{1};
        end
        
        cd('..//')
    end
    
    
    
    exist(outFolder, 'dir')  
    
    if 7 == exist(outFolder, 'dir')     
        rmdir(outFolder, 's');
    end
    mkdir(outFolder);
    
    cd(outFolder);
    
    
    numStim = length(layers{j});
    
    for i = 1:numStim
        
        
        ind = 1;
        combo = [];
        
        for j = 1:numLayers
            
            disp(['Stimulus: ' num2str(i)])
            disp(['  Layers: ' num2str(j)])
            
            numChan = size(layers{j}{1}, 3 );
            
            %ind 
             
            rez = size(layers{j}{i});
            rez = rez(1:2);
            
            disp(['    Rez: ' mat2str(rez)])
            
            combo(:,:,(ind):(ind+numChan-1)) = layers{j}{i};
            
            ind = ind + numChan;
        
            
        end
        
        [~, name, ~] = fileparts(listing(i+2).name);
        save([ name '.mat'], 'combo');
        
    end
    
    cd ../../Convolution_nets_ND;


end

