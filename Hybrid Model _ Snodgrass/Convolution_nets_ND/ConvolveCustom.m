function [  compOutIm ] = ConvolveCustom( compIm, filtTenser, filtShape, filtInvert, sumAccrossChannel, padRemoval, shapeSwitch, useGPU)
%CONVOLVECUSTOM This is a custom convolution operator that will convolve
%accross the spacial dimensions, rotations, scale, and featureRep, but not
%accross input channel.

% This function is supposed to be generic enough to help in finding the hidden
% image, the reconstructed image, and the filter Error 

    
    imRez = size(compIm,1);
    numChan = filtShape(3);
    
    if size(compIm,3)~=size(filtTenser,3) && ~strcmp(padRemoval, 'valid');
        disp('ALARM: number of channels does not match');
    end
    
    if strcmp(filtInvert, 'invert')
        % undoes the later flip, however not sure if I will need to flip
        % the other besides channel as well, I think I may need to.
        filtTenser = flipAll( filtTenser, [1 1 0 1 1 1]);
    end
    
    pad_filtTenser = floor(size(filtTenser,1)/2);
    if mod(size(filtTenser,1), 2) == 0
        pad_filtTenser = pad_filtTenser-.5;
    end
    
    % actual size will include a padding on imRez
    compOutIm_raw_indChan = [];

    borderMethod = 'full';    
    if strcmp(padRemoval, 'valid')
        borderMethod = 'valid';
    end

    
    % Convole over each channel seperately, give to GPU if possible
    if strcmp(useGPU, 'GPU')
        
        
        compOutIm_raw_indChan_GPU = gpuArray(zeros(imRez+pad_filtTenser*2, imRez+pad_filtTenser*2, filtShape(3), filtShape(4), filtShape(5), filtShape(6)));
        
        if strcmp(padRemoval, 'valid')
            
            % All of this is only an issue because I want to preallocate
            % for the array
            compOutIm_raw_indChan_GPU = gpuArray(zeros(imRez-pad_filtTenser*2, imRez-pad_filtTenser*2, filtShape(3),1,1,1));
            
            if strcmp(shapeSwitch, 'combine')
                compOutIm_raw_indChan_GPU = gpuArray(zeros([imRez-pad_filtTenser*2 imRez-pad_filtTenser*2 filtShape(3) filtShape(4:6)]));
            end
            
        end
        
        
        
        A = gpuArray(compIm(:,:,:,:,:,:));
        B = gpuArray(flipAll(filtTenser(:,:,:,:,:,:), [1 1 0 0 0 0]));
        
        
        for c=1:numChan
            
            % when we are dealing with a hidden image the number of
            % channels is 1; So we will expand chan by the filters channels
            cs = c;
            if strcmp(padRemoval, 'valid')
                cs = 1;
            end
            
            % For A last three indicies are dummies 
            
            compOutIm_raw_indChan_GPU(:,:,c,:,:,:) = convn(A(:,:,cs,:,:,:), B(:,:,c,:,:,:), borderMethod);   
            
        end
        
        compOutIm_raw_indChan = gather(compOutIm_raw_indChan_GPU);

    else %% non-gpu method much slower
        A = compIm(:,:,:,:,:,:);
        B = flipAll(filtTenser(:,:,:,:,:,:), [1 1 0 0 0 0]);
        for c=1:numChan
            
            cs = c;
            if strcmp(padRemoval, 'valid')
                cs = 1;
            end
            compOutIm_raw_indChan(:,:,c,:,:,:) = convn(A(:,:,cs,:,:,:), B(:,:,c,:,:,:), borderMethod);   
            
        end
    end
    
    
    if strcmp(sumAccrossChannel, 'sum')
        compOutIm = sum(compOutIm_raw_indChan,3);
    else
        compOutIm = compOutIm_raw_indChan;
    end
    
    %if strcmp(padRemoval, 'valid')
    %    compOutIm = removePad(compOutIm, pad_filtTenser);
    %end
    

end



