function [ RadialBasisMat_All, tenserShape, numVariations ] = GridRepresentation( rez, baseRez, numOrients, numScales, test)
% The goal of this function is to relate one image in standard grid
% coordinates to a series of images coordinates that represent different
% scalings and roations of the original image. YAY.



    % check for null parameters and set to default where nescessary
    if isempty(rez)
        rez = 15;       % grid coordinate pixel rezolution x and y
    end
    if isempty(baseRez)
        baseRez = 15; % Number of points on the polar axis
    end
    if isempty(numOrients)
        numOrients = 16; % number of orientaions
    end
    if isempty(numScales);
        numScales = 3;  % number of scales
    end
    if isempty(test)
        test = 'no test';
    end
    
    numBase = baseRez*baseRez; % number of polar coordinates
    numGrid = rez*rez; % number of grid coordinates
    scalingFactor = .75; % This relates the scale of the filters
    numVariations = numOrients*numScales;
    
    tenserShape = [numBase, numGrid, numOrients, numScales];
    
    % There is something really going wrong with this right now!!! 
    spreadScale = .00000003% .00000003; % this controls how much neighboring units effect eachother
    % I think there is a huge problem with how the variance is calculated
    % look to the original RadialRepresentation, the way it figures out how
    % big to set the variance seems to work where as here it seems to hyper
    % inflated... Perhaps it has to do with area vs length...
    
    denseNormOpt = 'raw';
    
    % this is where the full weight matrix will go
    RadialBasisMat_All = zeros(numBase, numGrid*numOrients*numScales);


    % create grid coordinates spanning [-.5 .5] along two axes
    [G1, G2] = meshgrid(1:rez,1:rez);
    G1 = (G1-.5-rez/2)/(rez); 
    G2 = (G2-.5-rez/2)/(rez);
    gridSpacing = (1/2)/rez; % spacing between the gridpoints

    G1vec = G1(:); % reads columnwise into a column vector
    G2vec = G2(:);
    


    % Display the grid coordinates
    if(strcmp(test, 'test'))
        fig1 = figure();
        colormap('gray');
        subplot(3,3,1); gridScatterHandle = scatter(G1vec, G2vec); daspect([1 1 1]);
    end
    
    % setup the polar coordinates
    radiansPerOrient = 2*pi/numOrients; % the number of radians between orienations
    [B1, B2] = meshgrid(1:baseRez,1:baseRez);
    B1 = (B1-.5-baseRez/2)/(baseRez);
    B2 = (B2-.5-baseRez/2)/(baseRez);
    B1vec = B1(:);
    B2vec = B2(:);
    baseSpacing = (1/2)/(baseRez);
    maxRadius = (baseRez/2)/(baseRez);
    maxInfluenceDist = 2*gridSpacing;
    
    % convert to polar coordinates to make it easier to transform, them
    [theta, radius] = cart2pol(B1vec, B2vec);
    
    
    for sc = 1:numScales % iterates over scale

        for or = 1:numOrients % iterates over orientations 

            orient = radiansPerOrient*(or-1);
            scale = scalingFactor^(sc-1);
            
            
            
            %Polar shifted and scaled to grid Coordinates 
            [T1vec T2vec] = pol2cart(theta+orient, radius*scale);

            
            %display coordinates
            if (or == 1) && (strcmp(test, 'test'))
                subplot(3,3,1);
                hold on;
                polarScatterHandle = scatter(T1vec, T2vec); daspect([1 1 1]); 
                for i=1:numBase
                    X = [T1vec(i); (T1vec(i)+ (baseSpacing*scale).^2)];
                    Y = [T2vec(i); T2vec(i)];
                    line(X,Y);
                end
                hold off;
                
            end
            
                
            % create distance matricies over both dimensions
            T1toG1dist = ( T1vec*ones(numGrid,1)' ) - (G1vec*ones(numBase,1)')';
            T2toG2dist = ( T2vec*ones(numGrid,1)' ) - (G2vec*ones(numBase,1)')'; 
            TtoGdist = (T1toG1dist.^2 + T2toG2dist.^2).^5; %euclidian distance


            % create a RBF weight matrix using stable variance (Grid variance)
            
            
            fixedVar = ((baseSpacing*scale).^2)*spreadScale;
            
            % just a gaussian with scaler multiples removed.
            RadialBasisMat_gridSpacing = exp(- (TtoGdist.^2)/(fixedVar^2) )/fixedVar;
           
            % capping the max distance nodes can interact
            RadialBasisMat_gridSpacing = RadialBasisMat_gridSpacing.*(TtoGdist<maxInfluenceDist);
                
            % This maskes the sub weight matrix so that pixels to far away
            % from radial units will not be connected, a bit like a disk
            % mask
            maskG = ((G1vec.^2+G2vec.^2).^.5)<(scale*maxRadius);
            maskMatG = ones(numBase, 1)*maskG';
            maskT = ((T1vec.^2+T2vec.^2).^.5)<(scale*maxRadius);
            maskMatT = maskT*ones(1,numGrid);
            maskGT = maskMatG.*maskMatT;
            
            RadialBasisMat_gridSpacing = RadialBasisMat_gridSpacing.*maskMatG.*maskMatT; 
            
            
             % This modifies the connections so that the relative number
            % and strength of connections or (connection density) of
            % the polar nodes are roughly equal. Currently recommended
            % turned off, as the native method tends to create a relatively
            % level connection density. Soften should be used before flat.
            if isempty(denseNormOpt)
                denseNormOpt = 'flat';
            end
            softenLevel = .1;
            densityNorm = [];
            if (strcmp(denseNormOpt, 'flat'))
                densityNorm = ones(numBase, 1) * sum(RadialBasisMat_gridSpacing,1);
            elseif (strcmp(denseNormOpt, 'soften'))
                densityNorm = ones(numBase, 1) * sum(RadialBasisMat_gridSpacing,1).^(1-softenLevel);
            else
                densityNorm = max(sum(RadialBasisMat_gridSpacing,1));
            end
            densityNorm(densityNorm==0) = 1; 
            RadialBasisMat_gridSpacing = RadialBasisMat_gridSpacing./(densityNorm);
            
            
            
            if (strcmp(test, 'test'))&&(sc<=3)&&(or==1)
                subplot(3,3,3+sc); imagesc(reshape(maskG,rez,rez)); daspect([1 1 1]);
            end
            
            diskMean = sum(RadialBasisMat_gridSpacing(:))/nnz(maskGT);
            RadialBasisMat_gridSpacing(maskGT~=0) = RadialBasisMat_gridSpacing((maskGT~=0)) - diskMean;
            
            % place the specific scale/orientation RBF matrix in the
            % combined RBF weight matrix.
            subStart = (numGrid*numOrients)*(sc-1)+(numGrid * (or-1)+1);
            subEnd   = (numGrid*numOrients)*(sc-1)+(numGrid * or);
            RadialBasisMat_All(:, subStart:subEnd) = RadialBasisMat_gridSpacing;

        end

    end
   
    %max(RadialBasisMat_All(:))
    RadialBasisMat_All = RadialBasisMat_All / norm(RadialBasisMat_All(:));
    %max(RadialBasisMat_All(:))
    
    
    
    if(strcmp(test, 'test'))
        test_extended(RadialBasisMat_All, numBase, numGrid, numOrients, numScales, rez, baseRez);
    end
    
    
end

%
% THE REST IS NOTHING BUT TESTING UTILITIES
%

function [] = test_extended(RadialBasisMat_All, numBase, numGrid, numOrients, numScales, rez, baseRez)
   
    
    % Display connection matrix for a single scale and orientation, and
    % another density test for the whole weight matrix
    
    %subplot(3,3,4:6); imagesc(RadialBasisMat_All(:,1:numGrid));
    subplot(3,3,7:9); imagesc(RadialBasisMat_All);
    RadialBasisTens_All = reshape(RadialBasisMat_All, numBase, numGrid, numOrients, numScales); % this is a tenser that organizeses the weight matrix in an orderly fashion
    denseTestScaleTens = sum(sum(sum(RadialBasisTens_All(:,:,:,1), 4), 3), 1);
    denseTestScaleIm = reshape(denseTestScaleTens, rez, rez);
    subplot(3,3,3); surfHandle = surf(denseTestScaleIm/max(denseTestScaleIm(:)), 'EdgeColor', 'none');
    
    
    
    % Test and display a feature learned at the highest resolution,
    fig2 = figure();
    colormap('gray');
    RadialBasisTens_All = reshape(RadialBasisMat_All, numBase, numGrid, numOrients, numScales);
    RadialBasisMat_Sub = reshape(RadialBasisTens_All(:,:,1,1), numBase, numGrid);
    [radialRep, testIm, testRec] = trainRadialRep(RadialBasisMat_Sub, rez, baseRez);
    [featureImages] = getFeatureImages(radialRep, RadialBasisMat_All, rez, numOrients, numScales);
    subplot(4, 3, 1); imagesc(testIm); daspect([1 1 1]);
    subplot(4, 3, 2); imagesc(reshape(testRec, rez, rez)); daspect([1 1 1]);
    subplot(4, 3, 4:6); imagesc(featureImages); daspect([1 1 1]);

    subplot(4, 3, 3); imagesc(reshape(radialRep, baseRez, baseRez)); daspect([1 1 1]);
    
    % Test and display a feature learned a middle resolution,
    RadialBasisTens_All = reshape(RadialBasisMat_All, numBase, numGrid, numOrients, numScales);
    RadialBasisMat_Sub = reshape(RadialBasisTens_All(:,:,1,round(end/2)), numBase, numGrid);
    [radialRep] = trainRadialRep(RadialBasisMat_Sub, rez, baseRez);
    [featureImages] = getFeatureImages(radialRep, RadialBasisMat_All, rez, numOrients, numScales);
    subplot(4, 3, 7:9); imagesc(featureImages); daspect([1 1 1]);
    
        
    % Test and display a feature learned at the lowest resolution,
    RadialBasisTens_All = reshape(RadialBasisMat_All, numBase, numGrid, numOrients, numScales);
    RadialBasisMat_Sub = reshape(RadialBasisTens_All(:,:,1,end), numBase, numGrid);
    [radialRep] = trainRadialRep(RadialBasisMat_Sub, rez, baseRez);
    [featureImages] = getFeatureImages(radialRep, RadialBasisMat_All, rez, numOrients, numScales);
    subplot(4, 3, 10:12); imagesc(featureImages); daspect([1 1 1]);    

end

% trains a representation at one scale/orientation then displays its
% genererlized learned features
function [radialRep, testIm, testRec] = trainRadialRep(RadialBasisMat, rez, baseRez)
    
    %load clown;
    %test = sum(X,3);
    test = zeros(rez,rez);
    test(floor(rez/2)+2,floor(rez/2)+5) = 1;
    test = double(test>mean(test(:)));
    test = fspecial('motion', rez, 45);
    blur = fspecial('gaussian',[5 5], .5);
    test = convn(test,blur, 'same');
    testIm = imresize(test, [rez rez]);
    testVec = testIm(:);
    testVec = (testVec-mean(testVec));
    
    testVec = testVec/max(abs(testVec));
    testVec = testVec;
    testIm = reshape(testVec, rez, rez);
    
    % this will be the radial representation that is learned upon
    radialRep = RadialBasisMat*testVec;
    
    testRec = zeros(rez^2,1);
    
    ErrorRec = [];
    
    % Trains the representation iteratively/actually could be solved using
    % a simple psuedo inverse solution.
    disp('Training Representation');
    for i = 1:1000

        testRec = RadialBasisMat'*radialRep;
        ErrorRec = -(testVec-testRec);
        ErrorRadialRep = RadialBasisMat*ErrorRec;
        %ErrorRepIm = convn( reshape(ErrorRadialRep, baseRez,baseRez), blur, 'same' );
        %ErrorRadialRep = reshape(ErrorRepIm, baseRez^2,1);
        
        sigma = 10/rez^2;

        radialRep = radialRep - sigma*ErrorRadialRep;
        
        
        disp(['Error ' mat2str(sum(ErrorRec.^2)) ]);
        
        if max(ErrorRadialRep.^2)<.000001
            break;
        end
    end
    
    
    disp(['Error ' mat2str(sum(ErrorRec.^2)) ]);
end

function [featureImages] = getFeatureImages(radialRep, RadialBasisMat_All, rez, numOrients, numScales)

    recAll = RadialBasisMat_All'*radialRep;

    recAllTens = reshape(recAll, rez, rez, numOrients, numScales);
    
    recAllIm = zeros(rez*numScales, rez*numOrients);
    
    for i = 1:numScales
        for j = 1:numOrients
            xInd = (rez*(i-1)+1):(rez*(i));
            yInd = (rez*(j-1)+1):(rez*(j));
            recAllIm(xInd,yInd) = recAllTens(:,:, j, i); 
        end
    end
    
    featureImages = recAllIm;

end
