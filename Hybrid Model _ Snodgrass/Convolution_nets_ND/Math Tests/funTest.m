function [C] = funTest()
% each dimesnion must match or be 1;

    collapseDim = 4;

    A = rand(1, 1, 1, 8, 9);
    B = rand(5, 6, 7, 8, 1);
    
    aIndNum = length(size(A));
    bIndNum = length(size(B));
    maxInd = max([length(size(A)) length(size(B))]);
    aSize = ones(1, maxInd);
    bSize = ones(1, maxInd);
    aSize(1, 1:aIndNum) = size(A);
    bSize(1, 1:bIndNum) = size(B);
    
    
    gpu = 'no';
    
    if strcmp(gpu, 'yes')
        A = gpuArray(A);
        B = gpuArray(B);
    else
        maxI = max([aSize; bSize],[], 1);
        
        Arep = -ones(1, maxInd);
        Arep(aSize==1) = maxI(aSize==1);
        Arep(aSize==maxI) = 1;
        Brep = -ones(1, maxInd);
        Brep(bSize==1) = maxI(bSize==1);
        Brep(bSize==maxI) = 1;
        
        if (min(Arep)==(-1))||(min(Brep)==(-1))
            disp('Dimesnsion Mismatch, will throw non-integer exception');
        end
        
        A = repmat(A, Arep);
        B = repmat(B, Brep);
        
    end
    
    
    C = arrayfun(@multiply, A, B);
    
    if strcmp(gpu, 'yes')
        C = gather(C);
    end
    
    if ~isempty(collapseDim)&&(collapseDim>=1)
        C = sum(C, collapseDim);
    end
    
    function [c] = multiply(a, b)
        c = a*b;
    end

    
    
    

end

