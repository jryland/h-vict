function [  ] = MultiDimensionalTest(  )
% Always remember convn flips every dimension of a filter, this is
% unimportant for a learning machinge in the first two dimensions of space
% but for keeping track of which outputs go to what feature channel it is
% critical to know that the order will be flipped, filter going in is
% flipped along all dimensions. It may be a good idea to make a wrappered
% version of flip like flipdims or flip_all that allows u to flip any and all
% dimensions you wish too.

% So a full filter set may look something like this tenser
% allFilts( res, res, compositeChannel,   Oreintation, Scale, baseFeature) 
% allFilts_fullIndex( res, res, compChannel,   Oreintation* Scale* baseFeature)

% in addition you should modularize and make a function for each
% convolution step for running a convolution network. Yay...

% make it so it goes compIm -> (compHiddenIm, ErrorRec)

    
    compIm = zeros(100,100,3);
    compIm(40,40,1) = 1;
    compIm(50,50,2) = 1;
    compIm(60,60,3) = 1;
    
    
    
    compFilt = zeros(15, 15, 3);
    compFilt(:,:,1) = fspecial('Gaussian', [15 15], 2);
    compFilt(:,:,2) = fspecial('Average', [15 15]);
    compFilt(:,:,3) = fspecial('Disk', 7);
    compFilt(1:7,1:7,:) = 0;
    compFilt = compFilt/max(compFilt(:));
    pad_compFilt = floor(size(compFilt,1)/2);
    
    % magic of arrays
    compIm_padded = addPad(compIm, pad_compFilt);
    compHiddenIm = convn(compIm_padded, flip(compFilt,3), 'valid');
    
    
    figure();
    subplot(1,3,1); image(compIm/max(compIm(:))); daspect([1 1 1]);
    subplot(1,3,2); image(compFilt/max(compFilt(:))); daspect([1 1 1]);
    subplot(1,3,3); imagesc(compHiddenIm/max(compHiddenIm(:)));  daspect([1 1 1])
    


end


function [] = compositeImageFilter()

end


% add or remove padding due
function [Im_padded] = addPad(Im,pad)
    [s1, s2, s3] = size(Im);
    Im_padded = zeros(s1+pad*2, s2+pad*2, s3);
    size( Im_padded((pad+1):(end-pad),(pad+1):(end-pad),:) )
    Im_padded((pad+1):(end-pad),(pad+1):(end-pad),:) = Im;
end



function [Im] = removePad(Im_padded, pad)
    Im = Im_padded((pad+1):(end-pad-1),(pad+1):(end-pad-1),:);
end

