function [RadialBasisMatAllR, tenserShape] = RadialRepresentation( rez, numSpokes, numLevels, density, interval, scaleInterval, numScales, displayTest )
% The goal of this function is to create a polar-Log coordinate
% representation that is self similar over scaling and rotation, and an
% accomanying distance matrix and radius matrix that can be used in image
% coordinate conversion.
% Such a representation allows for a fast and simple learning scheme for
% features that are thought to be usefull at multiple orientations and
% scales. The RadialBasisMatAllR is a connection matrix which can be used
% as a constraint in learning features for a convolution network.


% Rewrite all of this and make it less sketchy

    % Set up verticy centers with relative position
    
    %suggested values
    if isempty(rez)||rez==0
        rez = 31;       % 11 % grid coordinate pixel rezolution x and y
        numSpokes = 72; % 36
        numLevels = 16; % 8
        density = 4;
        interval = 4;
        scaleInterval = 4;
        numScales = 3;
        disp('using default values');
    end
    
    
    
    
    
    numOrients = numSpokes/interval;
    
    tenserShape = [numSpokes*numLevels, rez^2, numOrients, numScales];
    
    distPerLevel = 1;
    distLevel = 1;
    distFactor = 2;
    
    radiansPerSpoke = 2*pi/numSpokes;
    
    [level spoke] = meshgrid(1:numLevels,0:(numSpokes-1)); 
    
    thetas = spoke*radiansPerSpoke;
    radius = distFactor.^(level/density);
    interRadialDistance = distFactor.^((level+1)/density) - distFactor.^((level-1)/density);
    
    %Polar-Log Coordinates 
    X1 = radius.*cos(thetas);
    X2 = radius.*sin(thetas);
    
    X1vec = X1(:);
    X2vec = X2(:);
    
    %Create normal grid coordinates to circumscribe the polar-log
    maxLength = (distFactor^((numLevels+1)/density));

    [B1 B2] = meshgrid(1:rez,1:rez);
    B1 = (B1-1)/floor(rez/2)*maxLength-maxLength; 
    B2 = (B2-1)/floor(rez/2)*maxLength-maxLength; 
    
    B1vec = B1(:); % creates a column wise read column vector
    B2vec = B2(:);
    
    %Test
    %Display overlay of grid and Polar-log Coordinates
    if strcmp(displayTest, 'test');
        fig1 = figure();
        subplot(3,4,1);
        sc1 = scatter(X1vec, X2vec); daspect([1 1 1]); hold on;
        sc2 = scatter(B1vec, B2vec); set(sc2, 'MarkerEdgeColor', 'red');
        hold off;
    end
    
    % Create Distance Matrix
    XvecLength = length(X1vec);
    BvecLength = length(B1vec);
    
    %distance on each spatial axis
    X1toB1dist = ( X1vec*ones(BvecLength,1)' ) - (B1vec*ones(XvecLength,1)')';
    X2toB2dist = ( X2vec*ones(BvecLength,1)' ) - (B2vec*ones(XvecLength,1)')';
    
    XtoBdist = (X1toB1dist.^2 + X2toB2dist.^2).^5;
    
    Radii = radius(:)*ones(BvecLength,1)';
    InterRadii = interRadialDistance(:)*ones(BvecLength,1)';
    
    % Radii and Inter Radii need to be closely related for stability
    % concerns
    
    %Radial Basis Matrix: this tells how much one node influences another
    
    Var = 3*(InterRadii.*(2*pi*Radii./numSpokes));
    ringDensity = 1./Var;
    RadialBasisMat = exp(- (XtoBdist).^2./(Var.^2))./Var;
    
    %RadialBasisMat = exp(- ((XtoBdist).^2./(2 * Var))) ./ (Var.*((2*pi)^.5));
    
    %hack normalization
    densityNorm =  ones(XvecLength, 1) * sum(RadialBasisMat,1);
    densityNorm(densityNorm==0) = 1; 
    RadialBasisMat = RadialBasisMat./(densityNorm.^(1));
    
    %Test
    %Display the distance matrix & and some of its derivative thingies
    if strcmp(displayTest, 'test');
        subplot(3,4,2); imagesc(XtoBdist); daspect([1 1 1]);
        subplot(3,4,3); imagesc(InterRadii); daspect([1 1 1]);
        subplot(3,4,4); imagesc(RadialBasisMat); daspect([1 1 1]);
        colormap('gray');
    end
    
    disp(['The size of distance matrix is ' mat2str(size(XtoBdist))]);
    
    disp(['The number of nonzeros elements in the radial basis net is ' mat2str(nnz(RadialBasisMat)) ]);
    
    
    %Density Test
    if strcmp(displayTest, 'test');
        dense = sum(RadialBasisMat, 1)';
        denseIm = reshape(dense, rez, rez);
        subplot(3,4,5); surf(denseIm); daspect([1 1 1]);
    end
    %Test and view the Transformation Matrix in action
    %im = checkerboard(2);
    %test = imrotate(im,10);
    %load clown;
    %test = X;
    %test = sum(test,3);
    test = fspecial('motion', rez, 45);
    testIm = imresize(sum(test,3), [rez rez]);
    testVec = testIm(:);
    testVec = (testVec-mean(testVec));
    testVec = testVec/max(abs(testVec));
    radialRep = RadialBasisMat*testIm(:);
    testRec = RadialBasisMat'*radialRep; %reconstruction of test image
    testRecIm = reshape(testRec, rez, rez);
    
    if strcmp(displayTest, 'test');
        %Test Learn Rep
        for i = 1:200

            testRec = RadialBasisMat'*radialRep;
            ErrorRec = -(testVec-testRec);
            ErrorRadialRep = RadialBasisMat*ErrorRec;

            disp(['Error ' mat2str(sum(ErrorRec.^2)) ]);

            sigma = 100 * 1/rez^2;

            radialRep = radialRep - sigma*ErrorRadialRep; 

            radialRepIm = reshape(radialRep,numSpokes,numLevels);
            testRecIm = reshape(testRec, rez, rez);

            %Test
            if mod(i,10) == 0
                subplot(3,4,6); imagesc(testIm); daspect([1 1 1]);
                subplot(3,4,7); imagesc(radialRepIm); daspect([1 1 1]);
                subplot(3,4,8); imagesc(testRecIm); daspect([1 1 1]);
            end
            pause(.001);
        end
        disp('Press Enter to Proceed');
        pause();
    end
    
    % Create weight matrix to simultaniously represent the radial
    % representation at multiple orientations and scales
    RadialBasisMatAllR = zeros(numSpokes*numLevels, BvecLength*numSpokes/interval*numScales);
    numRadNodes = XvecLength;
    numGridNodes = BvecLength;
    
    
    for j = 1:numScales % iterate over scale
        for i = 1:(numSpokes/interval) % iterate over orientation

            % Method 1 use simple shift
            RadialBasisTens = reshape(RadialBasisMat, numSpokes, numLevels, BvecLength);
            RadialBasisTens(:,1:((numScales-1)*scaleInterval),:) = zeros(numSpokes,(numScales-1)*scaleInterval,BvecLength);
            RadialBasisTensRot = circshift(RadialBasisTens, [(i*interval) 0 0]);
            RadialBasisTensRotSC = circshift(RadialBasisTensRot, [0 ((j-1)*scaleInterval) 0]);
            if j>1
                RadialBasisTensRotSC(:,1:((j-1)*scaleInterval),:) = zeros(numSpokes,(j-1)*scaleInterval,BvecLength);
                
            end
            RadialBasisMatRotSC = reshape(RadialBasisTensRotSC, XvecLength, BvecLength);
            
            subStart = (numGridNodes*numSpokes/interval)*(j-1)+(numGridNodes*(i-1)+1);
            subEnd   = (numGridNodes*numSpokes/interval)*(j-1)+(numGridNodes*i);
            
            % Method 2 redo weight matrix with new parameters.
            % Try black whole approximation first.
            
            
            
            
            %Gain Control
            radiusMax = distFactor.^((numLevels-1)/density);
            currentLevelMax = numLevels-j*scaleInterval;
            currentRadius = distFactor.^(currentLevelMax/density);
            RadialBasisMatRotSC = RadialBasisMatRotSC/(currentRadius^2)*(radiusMax^2);
            
            RadialBasisMatAllR(1:numRadNodes, subStart:subEnd) = RadialBasisMatRotSC;
            
           

        end
    end
    
    %Test
    if strcmp(displayTest, 'test');
        recAllR = RadialBasisMatAllR'*radialRep;
        
        recAllRIm = reshape(recAllR, rez, rez*numSpokes/interval, numScales);
        
        recAll = [];
        for i = 1:(numScales)
            recAll = [recAll; recAllRIm(:,:,i)];
        end
        
        subplot(3, 4, 10:12 ); imagesc(recAll); daspect([1 1 1]);
        pause();

    end
   
end

