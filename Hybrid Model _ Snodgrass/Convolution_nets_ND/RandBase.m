function [ baseRep ] = RandBase( networkInfo )
    
    numSpokes = networkInfo.numSpokes;
    numLevels = networkInfo.numLevels;
    baseRez = networkInfo.baseRez;  % the rezolution of the base feature representations
    filtRez = networkInfo.filtRez;  % the rezolution of the filters
    numChan = networkInfo.numChan;  % the number of input channels
    numRep = networkInfo.numRep;    % the number of high level feature representations
    numOrients = networkInfo.numOrients;
    numScales = networkInfo.numScales;
    learnRate = networkInfo.startRate;
    recodeStyle = networkInfo.recodeStyle;


    if strcmp(networkInfo.repType, 'grid')

        baseRep = zeros(baseRez, baseRez, numChan, numRep);
        size(baseRep)

        noise = rand(200+baseRez, 200+baseRez, numChan, numRep);
        disk = fspecial( 'Disk', 9);
        noise = convn(noise, disk, 'same');
        subset = 100+(1:baseRez);
        baseRep(:,:,:,:) = noise(subset,subset,:,:);
        size(baseRep)

        %normalize and correctly shape baseRep
        baseRep = reshape(baseRep,baseRez^2, numChan, numRep);
        baseRep = (baseRep-mean(baseRep(:)));
        baseRep = .1*baseRep/mean(abs(baseRep(:))); %/(numChan);
    
    elseif strcmp(networkInfo.repType, 'radial')
        
        baseRep = zeros( numSpokes, numLevels, numChan, numRep);
        noise = rand(numSpokes+10, numLevels+10, numChan, numRep);
        
        disk = fspecial( 'Disk', 4);
        noise = convn(noise, disk, 'same');
        
        noiseSubset = noise((10+(1:numSpokes)), (10+(1:numLevels)), 1:numChan, 1:numRep);
        size_noiseSub = size(noiseSubset)
        size_baseRep = size(baseRep)
        
        baseRep = noiseSubset;
        
        baseRep = reshape(baseRep, numSpokes*numLevels, numChan, numRep);

        %normalize and correctly shape baseRep
        baseRep = (baseRep-mean(baseRep(:)));
        baseRep = .1*baseRep/mean(abs(baseRep(:))); %(numLevels*numSpokes*numChan*numOrients*numScales*numRep);
        
    end
    
    %randBaseRep_size = size(baseRep)
end

