function [  ] = TestOnline(  )
%TESTONLINE
% remember to fix things where the shapes of the multidimensional arrays so
% sizes of 1 will not cause problems!

    %Some initialization parameters
    baseRez = 15;
    filtRez = 15;
    numChan = 1;
    numRep = 2;
    learnRate = .001000000;
    minLearnRate = 10^(-10);
    epochs = 100; % number of times to go through the whole training set;
    
    
    % load cell array of multi-channel stimuli/ or composite images 
    compIm_c = loadStimuli(25, 100, 'grey');
    %compIm_c = loadNoiseStimuli(25, 50, 'grey', 'circle');
    
    numS = length(compIm_c);
    
    % Create a radial basis constraint matrix to relate a set of filters to
    % a set of base feature space
    [RBTenser, tenserShape, matShape, numChan_, numRep_] = getMultiRBFmat(baseRez, filtRez, numChan, numRep );
    
    
    % Generate a random set of base features
    baseRep = rand(baseRez^2, numChan, numRep);
    baseRep = (baseRep-mean(baseRep(:)));
    baseRep = .1 * baseRep/mean(abs(baseRep(:)));
    
    figure();
    pause(.1)
    
    compHiddenIm = [];
    compIm = [];
    compRecIm = [];
    ErrorNorm = 0;
    errorDecreasingRun = 1;
    
    
    baseErrorTenser_Batch = zeros(size(baseRep));
    
    for ep = 1:epochs
        
        ErrorNormNew = 0;

        suppressItemOutput = 1;

        if(~suppressItemOutput)
            disp(['Epoch: ' mat2str(ep) '  Stimuli: '  mat2str(s)]);
        elseif s == 1
            disp(['Epoch: ' mat2str(ep) ]);
        end
        compIm = compIm_c{s};

        % get filter stack 
        %disp('1. Getting Filters from feature Template');
        [filtTenser, filtShape] = getFilts(baseRep, RBTenser, tenserShape, matShape, numChan, numRep);

        % get hidden image
        %disp('2. Getting Hidden Image from Input Image');
        [ compHiddenIm ] = getHiddenIm( compIm, filtTenser, filtShape);

        % get reconstructed input image
        %disp('3. Getting Reconstructed Input Image from Hidden Image');
        [ compRecIm ] = getRecIm(compHiddenIm, filtTenser, filtShape);


        % get convolution derivative
        %disp('4. Getting Reconstructed Input Image from Hidden Image');
        [ filtErrorTenser, compImError] = getConvDeriv(compIm, compHiddenIm, compRecIm);

        ErrorNormNew = ErrorNormNew + norm(compImError(:))^2/numS;


        % get error projected into base feature space

        % apply gradient descent to base features
        %disp('5. Apply Gradiant Descent on Feature Template');
        %filtErrorTenser = zeros(filtShape);
        baseErrorTenser = baseProjection(RBTenser, matShape, filtErrorTenser, filtShape);

        baseErrorTenser_Batch = baseErrorTenser/numS + baseErrorTenser_Batch; 

        if(~suppressItemOutput)
            disp(['Reconstruction Error: ' mat2str(norm(compImError(:))^2)]);
            disp(['Magnitude Im          ' mat2str(norm(compIm(:))   )]);
            disp(['Magnitude Rec         ' mat2str(norm(compRecIm(:)))]);
            disp(' ');
        end

        % display some pics :) to check what you are doing
        if mod(ep*numS+s, 2)==0
            outIm = compIm; 
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(1,4,1); image(outFill); daspect([1 1 1]);

            outIm = squeeze(compHiddenIm(:,:,1,1,1,:)); 
            outIm = max(outIm,0);
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(1,4,3); imagesc(outFill); daspect([1 1 1]);

            outIm = squeeze(compRecIm(:,:,:,1,1,1)); 
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(1,4,2); image(outFill); daspect([1 1 1]);


            [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
            subplot(1,4,4);
            [handles] = dispRecodedIm(recodedHiddenIm);

            pause(.1);
        end
          
        
        %apply gradient descent batch style
        baseRep = baseRep - learnRate * baseErrorTenser_Batch;
        
%         % check to see if step size should be lowered
%         if (ErrorNormNew>=ErrorNorm || isnan(ErrorNormNew) )&& (ep>1)
%             learnRate = learnRate * .1;
%             baseRep = oldBaseRep;
%             disp('**** Step Size Decreased Progress Halt ****');
%             
%             disp(['Step Size             ' mat2str(learnRate)]);
%             
%             if learnRate < minLearnRate
%                disp('Step Size Too Small Halting Completely');
%                break;
%             end
%             
%             errorDecreasingRun = 0;
%             
%         % check to see if step size should be raised
%         elseif errorDecreasingRun>3
%             learnRate = learnRate * 1.5;
%             disp('**** Step Size Raised ****');
%             
%             disp(['Step Size             ' mat2str(learnRate)]);
%             
%             errorDecreasingRun = 0;
%             
%             oldBaseRep = baseRep;
%             ErrorNorm = ErrorNormNew;
%         else
%             errorDecreasingRun = errorDecreasingRun + 1;
%             
%             oldBaseRep = baseRep;
%             ErrorNorm = ErrorNormNew;
%         end
%         
%         
%         
%         disp(['Epoch Reconstruction Error:        ' mat2str(ErrorNorm)]);
%         disp(' ');

    end
    
    disp('Saveing Network Parameters');
    network.baseRep = baseRep;
    network.RBF = RBTenser;
    network.tenserShape = tenserShape;
    network.matShape = matShape;
    network.filtShape = filtShape;
    
    save networkParam.mat network;
    
    


end

