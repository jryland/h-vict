function [  ] = UpProject( prevFolder, currentFolder, outFolder,  reSize, fSize)
    
    
    % eventually replace with loadInputFrom 

    % Load the previous layer

    cd(['..//Hidden_Image_Sets/' prevFolder] );
    listing = dir();    
    
    prevLayer = cell(1,1);
    
    for i=3:length(listing)
        temp = struct2cell(load(listing(i).name));
        prevLayer{i-2} = imresize(temp{1}, [reSize reSize]);
    end
   


    % Load the current layer
    
    cd(['..//' currentFolder] );
    listing = dir();    
    
    
    curLayer = cell(1,1);
    
    for i=3:length(listing)
        temp = struct2cell(load(listing(i).name));
        curLayer{i-2} = temp{1};
    end

    prevLayer{1}
    
    
    padSz = (fSize-1)/2;
    pImSz = size(prevLayer{1},1)
    cImSz = size(curLayer{1},1)
    
    pChan = size(prevLayer{1},3)
    cChan = size(curLayer{1},3)
    
    a = padSz+1;
    b = padSz+pImSz;
    
    cd ../../Convolution_nets_ND;
    
    figure();
    
    pause(.1);
    
    % Up project and combine Currently with no resize
    for i=1:length(prevLayer)
        
        combo = zeros(cImSz, cImSz, pChan+cChan);
        
        size(prevLayer);
        
        combo(a:b,a:b,1:pChan) = prevLayer{i};
        
        combo(:,:,(pChan+1):(pChan+cChan)) = curLayer{i};
        
        comboLayer{i} = combo;
        
        if i == 1
            dispRecodedIm(combo , 'multi-chan');
        
            pause(.05);
        end
    end
    
    outFolder
    cd '..//Hidden_Image_Sets/';
    exist(outFolder, 'dir')  
    
    if 7 == exist(outFolder, 'dir')     
        rmdir(outFolder, 's');
    end
    mkdir(outFolder);
    
    cd(outFolder); 

    
    for i=3:length(listing)
        
        i-2
        
        combo = comboLayer{i-2};
        [~, name, ~] = fileparts(listing(i).name);
        save([ name '.mat'], 'combo');
        
        pause(.005);
    end
        
    cd ../../Convolution_nets_ND;
    
end

