function [ baseRepError ] = baseProjection( RBTenser, matShape, filtErrorTenser, filtShape, coding )
%BASEPROJECTION This function essentially performs the reverse of get filts
%by projecting a filterTenser back into the baseSpace, this is usefull for
%projecting the error signal of the filters back into the higher level
%feature representation for learning.

% FiltErrorTenser(rez, rez, chan, orient, scale, rep )
% baseRep(baseUnits, chan, rep);

    numRep = filtShape(6);
    numChan = filtShape(3);
    filtRez = filtShape(1);

    numBase = matShape(1);
    
    RBMat = reshape(RBTenser, matShape); 
    
    
    if strcmp(coding, 'full_orient_chan') % can only handle 1 multi orient feature currently
        for i=1:numChan
            filtErrorTenser(:,:,:,i,:,:) = circshift(filtErrorTenser(:,:,:,i,:,:), [0 0 (numChan-i) 0 0 0] );
        end
        
    end
    
    
    % normalize directional coding if vector coding is used
    if strcmp(coding, 'vector')
        [filtErrorTenser] = filterVecCoding( filtErrorTenser, filtShape, 'reverse' );
    end
    
    
    %transform into more usefull alternalte shape
    filtShapeAlt = [ filtRez*filtRez*filtShape(4)*filtShape(5), numChan, numRep];
    filtShapeAlt2 = [ filtRez, filtRez, filtShape(4), filtShape(5), numChan, numRep];
    
    %filtErAlt2 = permute(filtErrorTenser, [1 2 5 3 4 6]); %This may have
    %been incorrect...
    
    % Undo the permutation from base project, it was not a simple swap
    filtErAlt2 = permute(filtErrorTenser, [1 2 4 5 3 6]); %This may be right
    
    
    filtErAlt = reshape(filtErAlt2, filtShapeAlt);
    
    baseRepError = zeros(numBase,numChan,numRep);
    
    for rep = 1:numRep
        for chan=1:numChan
             baseRepError(:,chan,rep) = RBMat*filtErAlt(:,chan,rep);
        end
    end

end

