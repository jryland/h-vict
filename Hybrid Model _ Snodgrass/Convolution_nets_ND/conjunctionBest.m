function [ conjMat ] = conjunctionBest( im_c, topConjNum )
% this function finds the top X conjunction in terms of firing frequency

    numChan = size(im_c{1},3)
     
    [row, col] = find(triu(ones(numChan),1));
     
    conjMatAll = [row, col];
     
    conjTotals = zeros(size(row));
     
    % find the total activations of each
    for i = 1:length(im_c);
         
        [conjIm, ~] = conjunctionIm(abs(im_c{i}), conjMatAll);
         
        conjTotals = conjTotals + permute(sum(sum(conjIm, 1), 2), [3 1 2] );
             
    end
     
    % sort the cojunctions
    [~, ix] = sort(conjTotals); 
    conjMatAll = conjMatAll(ix,:);
     
    % select the top X conjuctions
    conjMat = conjMatAll(1:topConjNum,:);
    

end

