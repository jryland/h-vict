function [ conjIm_c ] = conjunctionIms( im_c, conjMat )
% this processes a group of multi-channel images to produce conunction maps
% and returns them in the form of a new set of multi-channeled images

    conjIm_c = cell(1,length(im_c));

    for i = 1:length(im_c);
         
        im_c{i} = convn(im_c{i}, fspecial('Gaussian', [11 11], .5), 'same'); 
        
        [conjIm, ~] = conjunctionIm(im_c{i}, conjMat);
         
        conjIm_c{i} = conjIm;
             
    end


end

