function [  ] = conjunctionProcess( inputFolder, inputInfo, outFolder, numConj )
    
    cd ..;
    cd Hidden_Image_Sets;
    
    outFolder
    exist(outFolder, 'dir')  
    
    if 7 == exist(outFolder, 'dir')     
        rmdir(outFolder, 's');
    end
    mkdir(outFolder);
    cd ..;
    cd Convolution_nets_ND;
    
    
    inRez = inputInfo.inRez;
    inNum = inputInfo.inNum;
    inStyle = inputInfo.inStyle;
    inHidden = 'hidden';
    
    
    [compIm_c, name_c] = loadInputFrom(inputFolder, inRez, inNum, inStyle, inHidden);
    
    compIm_c
    
    conjMat = conjunctionBest(compIm_c, numConj);
    
    conjIm_c = conjunctionIms(compIm_c, conjMat);
    
    
    
    figure();
    
    for i = 1:length(conjIm_c)
        conjIm = conjIm_c{i};
        
        conjIm = CleanHiddenIm(conjIm, .05);
        
        dispRecodedIm(conjIm, 'multi-chan');
        
        
        cd ..; % changing directory every time is wastefull
        cd Hidden_Image_Sets;
        cd(outFolder);
        save([name_c{i} '.mat'], 'conjIm');
        pause(.005);
        cd ..;
        cd ..;
        cd Convolution_nets_ND;
    end
    
    
    
end

