function [  ] = displayFilters( paramFileName, inputInfo )
% display the filters
    
    cd ..;
    cd Layer_Params;
    temp = load( paramFileName);
    cd ..;
    cd Convolution_nets_ND;

    network = temp.network;
    
    
    
    % setup some parameters;
    baseRep = network.baseRep;
    
    
    RBTenser = network.RBF;
    tenserShape = network.tenserShape;
    matShape = network.matShape;
    filtShape = network.filtShape;
    
    numChan = filtShape(3);
    numRep = filtShape(6);
    
    inRez = inputInfo.inRez;
    inNum = inputInfo.inNum;
    inStyle = inputInfo.inStyle;
    inSameple = inputInfo.inSample;
    inHidden = inputInfo.inHidden;
    
    % change base rep in order to test YAY!!!
    
    [filtTenser, filtShape] = getFilts(baseRep, RBTenser, tenserShape, matShape, numChan, numRep, inStyle);
    
    fig2 = figure();
    numOrients = filtShape(4);
    
    if strcmp(inStyle, 'vector')
        for rep = 1:numRep
            for or = 1:numOrients
                subplot(numRep,numOrients,or+numOrients*(rep-1));
                exampleFilt = squeeze(filtTenser(:,:,1:4,or,1,rep));
                [handles] = dispRecodedIm(exampleFilt);
            end
        end
    end
    pause(.1);
    
end

