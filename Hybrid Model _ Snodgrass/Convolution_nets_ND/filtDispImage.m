function [ featureImage ] = filtDispImage( filtTensor, filtShape)

    % FiltTenser(rez, rez, chan, orient, scale, rep )

    rez = filtShape(1);
    numChan = filtShape(3);
    numOrient = filtShape(4);
    numScale = filtShape(5);
    numRep = filtShape(6);
    
    
    % FiltTenser_alt(rez, chan, rep, rez, orient, scale )
    filtTensor_alt = permute(filtTensor, [1 3 6 2 4 5]);
    
    featureImage = reshape(filtTensor_alt, rez*numChan*numRep, rez*numOrient*numScale);

end

