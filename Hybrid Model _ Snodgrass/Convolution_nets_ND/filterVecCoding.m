function [ filtTenser ] = filterVecCoding( filtTenser, filtShape, direction )
%FILTERVECCODING Perform multiplication by a series of rotation matricies
%to correctly code orientation in the filters. This is done by multiplying
%sections of the filterTenser by specific rotation matricies designed to
%rotate their vectors according to how rotated the filter is in pixel
%coordinates. Setting direction to reverse will use the transposed rotation
%matrices which are equivolent to the originals inverse. 
    
    numRep = filtShape(3)/4;

    numOrients = filtShape(4);
    numScales = filtShape(5);
    
    thetasOr = (1:numOrients)'/numOrients * 2 * pi;
    thetasSc =  (1:numScales)'/numScales * 1 * pi;
    
    for rep = 1:numRep
        for or = 1:numOrients

            % check this transpose I think it is right...
            rotOr(1,1,1:2,1,1,1,1:2) = [cos(thetasOr(or)) -sin(thetasOr(or)); sin(thetasOr(or)) cos(thetasOr(or))]';
            if strcmp(direction, 'reverse')
                rotOr(1,1,1:2,1,1,1,1:2) = [cos(thetasOr(or)) -sin(thetasOr(or)); sin(thetasOr(or)) cos(thetasOr(or))]; % no transpose
            end

            
            a = (2*(rep-1)+1);
            b = (2*(rep-1)+2);
            
            % use mdaTimes Here and see if it is easier to work with.
            temp = mdaTimes(filtTenser(:,:, a:b, or, :, :), rotOr, 3, 'yes');
            filtTenser(:,:,a:b,or,:,:) = permute( temp, [1 2 7 4 5 6 3]);
            % ya thats 2 lines hella easy!!
            
        end

        for sc = 1:numScales

            rotOr(1,1,1:2,1,1,1,1:2) = [cos(thetasSc(sc)) -sin(thetasSc(sc)); sin(thetasSc(sc)) cos(thetasSc(sc))]';
            if strcmp(direction, 'reverse')
                rotOr(1,1,1:2,1,1,1,1:2) = [cos(thetasSc(sc)) -sin(thetasSc(sc)); sin(thetasSc(sc)) cos(thetasSc(sc))]; % no transpose
            end
            a = (2*(rep-1)+3);
            b = (2*(rep-1)+4);
            
            % use mdaTimes Here and see if it is easier to work with.
            temp = mdaTimes(filtTenser(:,:, a:b, :, sc, :), rotOr, 3, 'yes');
            filtTenser(:,:,a:b,:,sc,:) = permute( temp, [1 2 7 4 5 6 3]);
            % ya thats 2 lines hella easy!!

        end
    end

end

