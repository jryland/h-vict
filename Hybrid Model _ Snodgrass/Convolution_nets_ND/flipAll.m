function [ NDarray ] = flipAll( argsIn1, argsIn2)
%FLIPALL flips all the dimesnions of an n-dimensional array, this is
%usefull for preflipping a filter before using it in a convolution. This is
%because a convolution as defined in such a way that the filter will be
%revesered when it is applied, this can cause problems potentially.

    NDarray = argsIn1;
    start = 1;
    
    if nargin == 2
        flipVec = argsIn2;
    end

    for i = start:ndims(NDarray)
        if flipVec(i)==1
            NDarray = flip(NDarray, i);
        end
    end
end

