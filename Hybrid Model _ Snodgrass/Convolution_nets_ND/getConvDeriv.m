function [ filtErrorTensor, compImError, filtErrorTensorR] = getConvDeriv( compIm, compHiddenIm,compRecIm, rRate)
%GETCONVDERIV

% should really call this the reconstruction derivative + reg derivative

% essentially  convn(compIm, flipAll(flipAll(compHiddenIm)), full);
% then remove the padding added by compHiddenIm, and remember the flip here
% may be unnesscessary as convn already flips as compared to filtering.
% Perhaps only the 1st three elements need to be flipped
% really it just needs to be self consistent 

% this is almost precisely getHiddenIm but with the input reversed
%  filterDerivative(imRez, imRez, numChan, orient, scale, rep )


    compImError = -(compIm-compRecIm);
    
    filterShape = ones(1,6);
    sz = size(compHiddenIm);
    filterShape(1,1:length(sz)) = sz;
    compImShape = [size(compIm,1) size(compIm,2) size(compIm,3) 1 1 1];
    
    combinedShape = [compImShape(1:3) filterShape(4:6)];
    

    
    filtErrorTensor  = ConvolveCustom(flipAll(compHiddenIm, [1 1 0 0 0 0]),compImError,combinedShape, 'invert','','valid', 'combine','GPU');
    
    
    



end

