function [ filtTenser, filtShape ] = getFilts( baseRep, RBTenser, tenserShape, matShape, numChan, numRep, coding)
%GETFILTS converts the base feature representation into a series of
%oriented and scaled versions of itself using the RBTenser which is a
%series of corresponding image transformation matricies.

% the organization of the filt tenser will be as such
% FiltTenser(rez, rez, chan, orient, scale, rep )

% This organization can be used almost directly for convolutions against a
% composite image.

% In order to get there we need to rearange the order and loop over
% channel and rep.

% baseRep(baseUnits, chan, rep);  
    

    filtArea = tenserShape(2);
    filtRez = filtArea^.5;
    
    RBMat = reshape(RBTenser, matShape);
    
    filtShape = [ filtRez, filtRez, numChan, tenserShape(3), tenserShape(4), numRep ];
    
    filtVecShape = [prod(filtShape), 1];
    
    filtVec = zeros(filtVecShape);
    
    filtShapeAlt = [ filtRez*filtRez*tenserShape(3)*tenserShape(4), numChan, numRep];
    filtShapeAlt2 = [ filtRez, filtRez, tenserShape(3), tenserShape(4), numChan, numRep];
    
    filtAlt = zeros(filtShapeAlt);
    
    for rep = 1:numRep
        for chan=1:numChan

            filtAlt(:,chan,rep) = RBMat'*baseRep(:,chan,rep);
            
            % esnure that the filters are 0 centered
            filtVec = filtAlt(:,chan,rep);
            filtAlt(:,chan,rep) = filtVec - mean(filtVec);
        end
    end
    
    filtAlt2 = reshape(filtAlt, filtShapeAlt2);
    filtTenser = permute(filtAlt2, [1 2 5 3 4 6]); % may be incorrect
    
    % should make sure the STD is normalized accross orientation and scale,
    % this is a simple way of normalizing the contrast between orienations.
    % This is need because the misalignment from 90 degree orientations
    % sytematically reduces the contrast/amplitude of the filters.
    % Normalizing the by the STD produces filters with more similar
    % response levels.
    filtTenserMeansPer = mean(mean(abs(filtTenser), 1), 2);
    filtTenserMeans = mean(mean(filtTenserMeansPer, 4), 5);
    filtTenserRatio = mdaTimes(filtTenserMeans, 1/filtTenserMeansPer, [], 'yes');
    
    filtTenser = mdaTimes(filtTenser,filtTenserRatio, [], 'yes');
    
    numChan
    size(filtTenser)
    
    if strcmp(coding, 'full_orient_chan') % can only handle 1 multi orient feature currently
        for i=1:numChan
            filtTenser(:,:,:,i,:,:) = circshift(filtTenser(:,:,:,i,:,:), [0 0 (i) 0 0 0] );
        end
        
    end
    
    
    
    % Even out error due to grid mismatch this will take time to figure out
    % adjMethod = max(max(max(filtTenser,[],1),[],2),[],3,4)
    
    % WE NEED TO ADD CODE FOR HANDELING ROTATION VECTORS HERE!
    % WE ALSO NEED INPUT TO TELL US IF WE NEED VECTOR RECODING,THIS IS ONLY
    % RELEVENT IF THE INPUT TO THE NETWORK HAS VECTOR CODES
    % FOR ORIENTATION AND SCALE,
    
    if strcmp(coding, 'vector')
        [filtTenser] = filterVecCoding( filtTenser, filtShape, 'forward' );
    end
    
    
end

