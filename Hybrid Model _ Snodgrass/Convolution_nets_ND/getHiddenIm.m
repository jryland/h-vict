function [ compHiddenIm, compHiddenIm_raw] = getHiddenIm( compIm, filtTenser, filtShape, style )
%GETHIDDENIM this produces the hidden image and applies the sparsity
%function to produce a composite hidden image

% the resulting hidden image will be (imRez+pad*2, imRez+pad*2, 1, orient, scale, rep )

    
  compHiddenIm_raw = ConvolveCustom(compIm,filtTenser,filtShape,'no-invert','sum','', '','GPU');
  compHiddenIm = getHiddenSparse(compHiddenIm_raw, style);

end

