function [ sparseHiddenIm ] = getHiddenSparse( compHiddenIm_raw, sparsity )
%GETHIDDENSPARSE this function controls how sparsity is enforced in the
%hidden image.

% style full-sparsity, presentation, ReLu, none

% hidden image (imRez, imRez, 1, orient, scale, rep )

    numOrients = size(compHiddenIm_raw, 4);
    numScales = size(compHiddenIm_raw, 5);
    numRep = size(compHiddenIm_raw, 6);

    % rectified linear
    compHiddenIm = max(compHiddenIm_raw, 0);

    
    % find the maximum values and make all other nodes zeros
    % should produce super sparcity!
    
  
    maxValsHidden = [];
    
    if strcmp(sparsity, 'presentation')
        % just competition between orientations and scales
        expandVec = ones(1, 1, 1, numOrients, numScales, 1);
        maxValsHidden = convn( max(max(compHiddenIm,[], 4),[], 5), expandVec );
        
    elseif sum(strcmp(sparsity, {'full-sparsity', 'combo'}))
        % stronger spatial competition between features version
        % two features cannot be present at the same location
        expandVec = ones(1, 1, 1, numOrients, numScales, numRep);
        maxValsHidden = convn( max(max(max(compHiddenIm,[], 4),[], 5),[], 6 ), expandVec );
    end
    
    
    % all but the components of the winning nodes should be 0
    %sparsity = 'full-sparsity';
    %sparsity = 'none';
    
    if sum(strcmp(sparsity, {'full-sparsity', 'presentation'})) % these are also Relus
        sparseHiddenIm = compHiddenIm.*(maxValsHidden == compHiddenIm & maxValsHidden>0);
    elseif strcmp(sparsity, 'ReLu')
        sparseHiddenIm = max(compHiddenIm_raw, 0);
    elseif strcmp(sparsity, 'combo');
        sparseHiddenIm = compHiddenIm.*(maxValsHidden == compHiddenIm & maxValsHidden>0)/2 + compHiddenIm/2;
    else
        sparseHiddenIm = compHiddenIm;
    end
end

