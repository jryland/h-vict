function [ RadialBasisTens, tenserShape, matShape, numChan, numRep] = getMultiRBFmat(networkInfo )
%GETMULTIRBFMAT create a readial basis weight matrix for converting between
%base feature representations and the filter representations. This will be
%a tenser heirarchically organized by dimension.

%Organization
% RadialBasisTens( baseUnits, filtUnits, Orient, Scale)

% Channel and representation will be covered with loops

% may want to silently loop accross channel seeing as we are making large
% matricies that are majority sparse.
    
    type = networkInfo.repType;
    numSpokes = networkInfo.numSpokes;
    numLevels = networkInfo.numLevels;
    interval = networkInfo.interval;
    
    baseRez = networkInfo.baseRez;
    filtRez = networkInfo.filtRez;
    numChan = networkInfo.numChan;
    numOrients = networkInfo.numOrients;
    numScales = networkInfo.numScales
    numRep = networkInfo.numRep;
    
    
    RadialBasisMat_oneChan = [];
    
    
    if strcmp(type, 'grid');
        [RadialBasisMat_oneChan, tenserShape] = GridRepresentation(filtRez,baseRez, numOrients, numScales, 'no test');
    elseif strcmp(type, 'radial');
        % Produces 12 orientations 1 scale
        [RadialBasisMat_oneChan, tenserShape] = RadialRepresentation(filtRez,numSpokes,numLevels,4,interval,4,1,'no test');
    end
    
    RadialBasisTens = reshape(RadialBasisMat_oneChan, tenserShape);
    
    % good with sizes and ensures that the size vec is the right length
    tenserShape = ones(1,4);
    sizeVec = size(RadialBasisTens);
    tenserShape(1,1:length(sizeVec)) = sizeVec
    
    matShape = [tenserShape(1) prod(tenserShape(2:4))]
    
    prod(matShape)
    

end

