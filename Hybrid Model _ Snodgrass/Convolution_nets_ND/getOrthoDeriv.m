function [orthoError, OrthoErrorTensor ] = getOrthoDeriv( sigs, s, compHiddenIm, compIm, filtShape  )

    numOrients = filtShape(4);
    
    size(sigs)
    size(sigs,1)
    
    I = eye(size(sigs,1)/numOrients);
    
    I = kron(I, ones(numOrients)) .* ((sum(abs(sigs),2)>0)* ones(1, size(sigs,1)) );

    nnz(I)
    
    similarities = sigs * (sigs(s,:)');
    
    orthoError = sum((I(:,s)-similarities).^2);
    
    
    % Back prop Yay
    
    Error = -2*((I(:,s)-similarities));
    
    OrthoSigErrorD = (sigs'*Error); % no nonlinearities assumed
    
    size(OrthoSigErrorD)
    
    OrthoSigErrorDTensor(1,1,1,1,1,:) = OrthoSigErrorD(:,1);
    
    expand = ones(size(compHiddenIm));
    expand(6) = 1;
    
    orthoHiddenErrorIm = mdaTimes(OrthoSigErrorDTensor, expand, [], 'yes');
    
    combinedShape = [ size(compIm, 1) size(compIm, 2) size(compIm, 3) size(compHiddenIm, 4) size(compHiddenIm, 5) size(compHiddenIm, 6) ];
    
    OrthoErrorTensor  = ConvolveCustom(flipAll(orthoHiddenErrorIm, [1 1 0 0 0 0]), compIm, combinedShape, 'invert','','valid', 'combine','GPU');
    
end

