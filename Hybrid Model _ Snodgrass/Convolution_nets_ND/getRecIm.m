function [ recIm ] = getRecIm( compHiddenIm, filtTenser, filtShape  )
%GETRECIM Summary of this function goes here
%   Detailed explanation goes here

    recIm_raw = ConvolveCustom(compHiddenIm,filtTenser,filtShape,'invert','','valid', '','GPU');
    
    
    recIm = recIm_raw/prod(filtShape(4:6)); 
    
end

