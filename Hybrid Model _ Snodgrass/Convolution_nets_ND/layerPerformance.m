function [  ] = layerPerformance( hiddenFolder, numStimuli, inOrients, task )
% display some graphs quantifying the performance of the layer's
% representation for the purposes of differentiating the stimuli.
    
    numStimuli

    % go to the right directories and pull saved hidden representations
    im_c = loadInputFrom(hiddenFolder, [], numStimuli, 'multi-chan', 'hidden');
    numS = length(im_c);
    
    % display the first image
    fig1 = figure();
    [handles] = dispRecodedIm(im_c{1}, 'multi-chan');
    pause(.001);
    
    im = [];
    sSignatures = [];
    sHiddenVecs = [];
    
    %imFig = figure();
    
    % process representations into usable forms
    for s = 1:numS
        im = im_c{s};
        
        %set(0, 'CurrentFigure', imFig);
        %[handles] = dispRecodedIm(im, 'multi-chan');
        
        sDensity = sum(sum(sum(sum(sum(im,1),2),4),5),6)/numS;
        sSignatures(s,:) = sDensity(:)/norm(sDensity(:));
        
        
        % do a little blurring avg radius 7
        blur = 0;
        if blur
            disp(['Blurring Stim ' num2str(s)]);
            blurFilt = fspecial('disk', 11);
            im = convn(im, blurFilt, 'same');
            im_c{s} = im;
        end

        sHiddenVecs(s,:) = im(:)/norm(im(:))/numS;
        
    end
    
    % display the first image
    fig1 = figure();
    [handles] = dispRecodedIm(im_c{1}, 'multi-chan');
    pause(.001);
    
    % remove the means
    mSig = sSignatures-ones(numS,1)*mean(sSignatures,1);
    mHid = sHiddenVecs-ones(numS,1)*mean(sHiddenVecs,1);
    
    % image info
    disp(['Image Dimensions' mat2str(size(im))])
    disp(['Rep1-simple     ' mat2str(size(sSignatures))]);
    disp(['Rep2-simple     ' mat2str(size(sHiddenVecs))]);
    
    % currently removing the means
    [figGCNN, GCNN_performance] = dispRepStats(mSig, inOrients, task);
    [figLIN, LIN_performance] = dispRepStats(mHid, inOrients, task);
   
    oldFolder = cd('..');
    cd Results;
    dateStr = datestr(date,'mmm_dd_yy');
    fullTime = clock;
    save(['GCNN_' dateStr '_h' num2str(fullTime(4)) '.mat'], 'GCNN_performance');
    savefig(figGCNN ,['GCNN_' dateStr '_h' num2str(fullTime(4)) '.fig']);
    cd ..;
    cd(oldFolder);
    
    oldFolder = cd('..');
    cd Results;
    dateStr = datestr(date,'mmm_dd_yy');
    fullTime = clock;
    save(['LIN_' dateStr '_h' num2str(fullTime(4)) '.mat'], 'LIN_performance');
    savefig(figLIN ,['LIN_' dateStr '_h' num2str(fullTime(4)) '.fig']);
    cd ..;
    cd(oldFolder);
    
    
    

end

function [figOrients, performance] = dispRepStats(sSignatures, inOrients, task)
    
    train = 'full';
    if strcmp(task, 'match to upright');
        train = 'upright';
    end


    numS = size(sSignatures, 1);
    numC = size(sSignatures, 2);
    
    numS
    numS/inOrients
    inOrients
    
    % display similarity statistics
    similarityFig = figure();
    set(0, 'CurrentFigure', similarityFig);
    colormap('hot');
    subplot(2,4,5:8);
    imagesc(sSignatures'); daspect([1/numC 4/numS 1]);
    subplot(2,4,1);
    
    similarityMatrix = sSignatures*sSignatures';
    covMatrix = cov(sSignatures');
    imagesc( (similarityMatrix)); daspect([1 1 1/numS]);

    % linsolve to find 1-step best fit weights
    % show classiffication confidence
    train = 'upright';
    R = [];
    if strcmp(train, 'full')
        I = eye(numS);
        A = sSignatures;
        B = I';
        X = linsolve(A, B);

        R = X'*A';
    else % upright
        I = eye(numS/inOrients);
        A = sSignatures(1:inOrients:numS,:);
        A2 = sSignatures;
        B = I';
        X = linsolve(A, B);

        
        R = X'*A2';
        size(R)
        upVec = zeros(inOrients, 1); upVec(1) = 1;
        R = kron(R, upVec);
        
        size(R)
        
    end

    R_raw = R;
    
    % only test against upright first image
    if strcmp(task, 'match to upright')
        up = ones(inOrients, inOrients); 
        up(1:1,:) = 0;
        upMask = 1-kron(eye(numS/inOrients),up);
        size(R)
        size(upMask)
        
        R = R.*upMask;
    end

    
    % standard display stuff
    subplot(2,4,2);
    imagesc(R_raw); daspect([1 1 1]);
    
    RmaxV = repmat(max(R, [], 1), [numS 1]);
    
    
    Rmax = R==RmaxV;
    
    subplot(2,4,3);
    imagesc(Rmax); daspect([1 1 1]);
    
    subplot(2,4,4); 
    
    % Get correct per orientation
    corCheck = kron(eye(numS/inOrients),ones(inOrients));
    incorCheck = 1-corCheck;
    
    total = sum(reshape(sum(Rmax, 1), inOrients, numS/inOrients),2);
    totalCor = sum(reshape(sum(Rmax.*corCheck, 1), inOrients, numS/inOrients), 2);
    
    figOrients = figure();
    plot((0:(inOrients-1))/inOrients*360, totalCor./total*100);
    ylim([0 100]);  daspect([1/100 1/360 1]);
    
    
    performance.itemByOrient = reshape(sum(Rmax.*corCheck, 1), inOrients, numS/inOrients)'; 
    
    %imagesc(performance.itemByOrient);
    
    %figure();
    %imagesc(upMask);
    
end

