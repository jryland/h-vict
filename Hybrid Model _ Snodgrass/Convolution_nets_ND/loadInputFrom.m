function [ S_c,  name_c] = loadInputFrom(folderName, Srez, mask, style, hidden)
%

    numStimuli = size(mask,2);
    

    im_c = {};
    name_c = {};
    
    cd ..;
    if strcmp(hidden, 'hidden')
        cd Hidden_Image_Sets;
    end
    cd(folderName);
    listing = dir();
    
    iterator = 1;
    iterator2 = 1;
    for i = 3:min(length(listing),numStimuli+3)   
        
        [pathstr,name,ext] = fileparts(listing(i).name);
        
        if ~listing(i).isdir &&( sum(strcmp(ext,{'.mat', '.MAT'}))==1 )
            
            if mask(iterator)
                disp(['Load Image ' name]);
                temp = load(listing(i).name);
                temp = struct2cell(temp);
                im_c{iterator2} = temp{1};
                name_c{iterator2} = name;
                iterator2 = iterator2+1;
            end
            
            iterator = iterator+1;
        end
    end
    cd ..;
    if strcmp(hidden, 'hidden')
        cd ..;
    end
    cd Convolution_nets_ND;
    
    S_c = {};
    
    numS = size(im_c, 2)
    
    pause(.5)
    
    if isempty(Srez)
        Srez = length(im_c{1});
    end
    
    
    for s = 1:numS
        
        disp(['Process Image ' mat2str(s)]);
        
        im = im_c{s};
        
        if strcmp(style, 'grey')
            im = sum(im, 3); 
        end
        
        if ~isempty(Srez)
            im = imresize(im,[Srez Srez]);
        end
        
        % maybe turn this off for hiddenLayer images
        % Should really only do this for the original raw image
  
        
        coding = style;
        
        if strcmp(hidden, 'original');
            
            % Low Wavelength removal and invert stimuli?
            lowPass = fspecial('gaussian', 31, 7.5);
            
            highPass = [];
            
            if strcmp(style, 'grey')
                highPass = fspecial('gaussian', 31, 9.5); % use with mono
                
            else
                highPass = fspecial('gaussian', 11, 2.5); % use with color
                % Setup to use GPU computing
            end
            %highPass = fspecial('disk', 17);
            
            
            [S, ~, chan] = size(im);
            imPad = zeros(S+40,S+40, chan);
            
            
            % Blur High Wavelenth data;
            imPad((20*(1:S)), (20*(1:S)), :) = im(1,1,1)-im;
            imPad = convn(imPad, highPass, 'same');
            im = imPad((20*(1:S)), (20*(1:S)), :);
            
            
            %im = im - convn(im, lowPass, 'same');
            
            %remove edge artifacts 
            %imArea = ones(size(im))*im(1,1,1);
            %obscura = convn(imArea, lowPass, 'same');
            %im = im-obscura;
            % End
        
            %im = im-mean(im(:));
            im = im/max(abs(im(:)));
        end
        
        if sum(strcmp(coding, {'multi-chan', 'full_orient_chan'}))
            %im = max(im,0);
%             % Low Wavelength removal and invert stimuli?
%             lowPass = fspecial('gaussian', 31, 7.5);
%             im = im-convn(im, lowPass, 'same');
%             % remove edge artifacts 
%             imArea = ones(size(im))*im(1,1,1);
%             obscura = convn(imArea, lowPass, 'same');
%             im = im+obscura;
%             % End
            %im = im-mean(im(:)); % wait why are we doing this?
            im = im/max(abs(im(:)));
        end
        
        if strcmp(coding, 'raw')
            im = im;
        end
        
        S_c{s} = im;
    end

    if length(S_c)>numStimuli
        S_c = S_c(1:numStimuli);
    end
    
end

