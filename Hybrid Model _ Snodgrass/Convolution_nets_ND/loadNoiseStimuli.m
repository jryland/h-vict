function [ S_c  ] = loadNoiseStimuli( Srez, numStimuli, style, type)
%LOADSTIMULI Summary of this function goes here
%   Detailed explanation goes here
    %load images
    
    % also need to do a circle stimuli to test oreintation coding !!!
    
    S_c = {};
    
    numS = numStimuli;
    
    if isempty(Srez)
        Srez = 100;
    end
    
    
    for s = 1:numS
        
        im = [];
        
        if strcmp(type, 'circle');
            im = zeros(Srez, Srez, 3);
            disk = fspecial('Disk', 10);
            in1 = ceil(rand(1,1)*Srez);
            in2 = ceil(rand(1,1)*Srez);
            im(in1, in2) = 1;
            im = convn(im, disk, 'same');
        else
            im = rand(Srez, Srez, 3);
            gau = fspecial('Gaussian', 5);
            im = convn(im, gau, 'same');
        end
        
        
        if strcmp(style, 'grey')
            im = sum(im, 3); 
        end
        
        im = im-mean(im(:));
        im = im/max(abs(im(:)));
        S_c{s} = im;
    end

end
