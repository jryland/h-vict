function [ S_c ] = loadStimuli( Srez, numStimuli, style)
%
    im_c = {};
    
    cd ..;
    cd('Stimuli_Pack');
    listing = dir();
    
    iterator = 1;
    for i = 3:min(length(listing),numStimuli+2)   
        
        [pathstr,name,ext] = fileparts(listing(i).name);
        
        if ~listing(i).isdir &&( sum(strcmp(ext,{'.mat', '.MAT'}))==1 )
            temp = load(listing(i).name);
            temp = struct2cell(temp);
            im_c{iterator} = temp{1};
            iterator = iterator+1;
        end
    end
    cd ..;
    cd Convolution_nets_ND;
    
    S_c = {};
    
    numS = size(im_c, 2);
    
    if isempty(Srez)
        Srez = size(im_c{1});
    end
    
    
    for s = 1:numS
        
        im = im_c{s};
        
        if strcmp(style, 'grey')
            im = sum(im, 3); 
        end
        
        im = imresize(im,[Srez Srez]);
        
        %im = convn(im, log, 'same');
        
        im = im-mean(im(:));
        im = im/max(abs(im(:)));
        S_c{s} = im;
    end

end

