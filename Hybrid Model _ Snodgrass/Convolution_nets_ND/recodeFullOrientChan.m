function [ recodedHiddenIm ] = recodeFullOrientChan( compHiddenIm, filterTenserShape )
% This function will recode oriented features as channels

%array indexing to keep in mind
%FiltTenser(rez, rez, chan, orient, scale, rep )
%compHiddenIm(imrez, imrez, 1, orient, scale, rep);

%recoded (imrez, imrez, numParam*numRep, 1, 1, 1,);

    imrez = size(compHiddenIm, 1);
    numOrients = size(compHiddenIm, 4);
    numScales = size(compHiddenIm, 5);
    numReps = size(compHiddenIm, 6);
    
    
    recodedHiddenIm = reshape(compHiddenIm, imrez, imrez, numOrients * numScales * numReps);
    

    size(recodedHiddenIm)

end

