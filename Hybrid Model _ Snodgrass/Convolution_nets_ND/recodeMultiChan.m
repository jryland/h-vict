function [ recodedHiddenIm ] = recodeMultiChan( compHiddenIm, filterTenserShape )
%RECODEMULTICHAN This function will recode a hidden image into a simple
%multi-channel image where each channel represents the presence of a
%feature, it will be mutually exclusive too.

%array indexing to keep in mind
%FiltTenser(rez, rez, chan, orient, scale, rep )
%compHiddenIm(imrez, imrez, 1, orient, scale, rep);

%recoded (imrez, imrez, numParam*numRep, 1, 1, 1,);

    % more differentiating
    compHiddenMax = max(max(compHiddenIm, [],4), [], 5);
    
    % more stable
    compHiddenMax = sum(sum(compHiddenIm,4), 5);
    
    
    recodedHiddenIm = permute(compHiddenMax, [1 2 6 4 5 3]);
    
    
end

