function [ recodedHiddenIm] = recodeSimple(compHiddenIm, filterTenserShape )
% this function will read the shape of the filters and use this to
% determine how to assign direction vectors in order to make a more
% compact version of the hiddenIm that also has desirable mathematical
% properties.

%array indexing to keep in mind
%FiltTenser(rez, rez, chan, orient, scale, rep )
%compHiddenIm(imrez, imrez, 1, orient, scale, rep);

%recoded (imrez, imrez, numParam*numRep, 1, 1, 1,);

    numOrients = filterTenserShape(4);
    numScales = filterTenserShape(5);
    
    thetasOr = (1:numOrients)'/numOrients * 2 * pi;
    thetasSc =  (1:numScales)'/numScales * 1 * pi;
    
    numRep = filterTenserShape(6);
    imRez = size(compHiddenIm, 1);
    
    numParam = 4;
    
    
    % orVecs (1, 1, components, orient, 1, 1)
    orVecs(1, 1, :, :, 1, 1) = [cos(thetasOr) sin(thetasOr)]';
    
    orVecsAlt = convn(orVecs, ones(imRez, imRez, 1, 1, numScales, numRep));
    
    % scVecs (1, 1, components, 1,  scale, 1)
    scVecs(1, 1, :, 1, :, 1) = [cos(thetasSc) sin(thetasSc)]';
    scVecsAlt = convn(scVecs, ones(imRez, imRez, 1, numOrients, 1, numRep));
    
    
    % ensure competitive sparsity
    expandVec = ones(1, 1, 1, numOrients, numScales, 1);
    
    maxValsHidden = convn( max(max(compHiddenIm,[], 4),[], 5), expandVec );
    
    % all but the components of the winning nodes should be 0
    %sparseHiddenIm = compHiddenIm.*(maxValsHidden == compHiddenIm);
    
    sparseHiddenIm = compHiddenIm.*(maxValsHidden == compHiddenIm & maxValsHidden>0);
    countHiddenIm = (maxValsHidden == compHiddenIm & maxValsHidden>0);
    
    
    % number of winners in case of tie
    sparsePositiveSum = convn( sum(sum(countHiddenIm>0,4),5), ones(1, 1, 2, 1, 1, 1) );
    sparsePositiveSum( sparsePositiveSum < 1 ) = 1;
    
    sparsePositiveSumAlt = convn(sparsePositiveSum, ones(1, 1, 1, numOrients, numScales, 1));
    
    % apply the recode by taking the mean of winners vector components
    
    recodedHiddenImOrAlt = sum(sum(  convn(sparseHiddenIm, ones(1,1,2)).*orVecsAlt./sparsePositiveSumAlt  ,4),5);
    
    
    recodedHiddenImScAlt = sum(sum(  convn(sparseHiddenIm, ones(1,1,2)).*scVecsAlt./sparsePositiveSumAlt  ,4),5);
    
    
    recodedHiddenIm(:,:,1:2, 1, 1, :) = recodedHiddenImOrAlt;
    recodedHiddenIm(:,:,3:4, 1, 1, :) = recodedHiddenImScAlt;
    
    
    
    recodedHiddenIm = reshape(recodedHiddenIm, [imRez, imRez, numParam*numRep, 1, 1, 1]);
    
    
    
    
    
    

    

    
end

