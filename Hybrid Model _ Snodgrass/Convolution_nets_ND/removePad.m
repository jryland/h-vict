function [Im] = removePad(Im_padded, pad)
% removes a padding on the first 2 dimension of a compositeImage
    Im = Im_padded((pad+1):(end-pad-1),(pad+1):(end-pad-1),:,:,:,:);
end

