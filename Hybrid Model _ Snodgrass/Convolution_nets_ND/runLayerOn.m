function [ ] = runLayerOn( inputFolder, inputInfo, networkInfo, networkParam, outFolder, saveOut )

    cd ..;
    cd Layer_Params;
    temp = load( networkParam);
    cd ..;
    cd Convolution_nets_ND;
    
    cd ..;
    cd Hidden_Image_Sets;
    
    outFolder
    exist(outFolder, 'dir')  
    
    if 7 == exist(outFolder, 'dir')     
        rmdir(outFolder, 's');
    end
    mkdir(outFolder);
    cd ..;
    cd Convolution_nets_ND;
    
    
    network = temp.network;
    
    baseRep = network.baseRep;
    RBTensor = network.RBF;
    TensorShape = network.TensorShape;
    matShape = network.matShape;
    filtShape = network.filtShape;
    recodeStyle = network.recodeStyle;
    repDensity = network.repDensity;
    
    sparseStyle = networkInfo.sparseStyle;
        
    
    numChan = filtShape(3);
    numRep = filtShape(6);
    
    inRez = inputInfo.inRez;
    inNum = inputInfo.inNum;
    inStyle = inputInfo.inStyle;
    inSample = inputInfo.inSample;
    inHidden = inputInfo.inHidden;
    inOrients = inputInfo.numOrients;
    
    pauseTime = .001;
    
    [compIm_c, name_c] = loadInputFrom(inputFolder, inRez, inNum, inStyle, inHidden);
    numS = length(compIm_c);
    
    
    
    filtTensor = [];
    filtShape = [];
    outIm = [];
    compHiddenIm = [];
    compRecIm = [];
    
    sSignatures = zeros(numS, numRep);
    similarityMatrix = [];
    
    displayFig = figure();
    
    for s = 1:numS
        
        disp(['Processing Image ' mat2str(s)]);

        compIm = compIm_c{s};
        
        blurIn = 1;
        if  blurIn %~strcmp(inHidden, 'hidden') && blurIn
            compIm = convn( compIm, fspecial('gaussian', [11 11], networkInfo.spatialFreq), 'same');
        end
        edgeIn = 1;
        if  edgeIn %~strcmp(inHidden, 'hidden') && edgeIn
            compIm = convn( compIm, fspecial( 'log'), 'same');
        end
        
        
        % get filter stack 
        %disp('1. Getting Filters from feature Template');
        [filtTensor, filtShape] = getFilts(baseRep, RBTensor, TensorShape, matShape, numChan, numRep, inStyle);

        % get hidden image
        %disp('2. Getting Hidden Image from Input Image');
        [ compHiddenIm, compHiddenIm_raw] = getHiddenIm( compIm, filtTensor, filtShape, sparseStyle);

        % get reconstructed input image
        %disp('3. Getting Reconstructed Input Image from Hidden Image');
        [ compRecIm ] = getRecIm(compHiddenIm, filtTensor, filtShape);
        
        % usefull things for figuring out seperability
        sDensity = sum(sum(sum(sum(sum(compHiddenIm,1),2),3),4),5)/numS;
        sSignatures(s,:) = sDensity(:)/norm(sDensity(:));
            
        
        set(0, 'CurrentFigure', displayFig);
        
        % write a function for this...
        % display what you are doing for checking
        if strcmp(inStyle, 'vector')
            subplot(2,5,1);
            [handles] = dispRecodedIm(compIm);
            subplot(2,5,2);
            [handles] = dispRecodedIm(compRecIm);
            
        elseif strcmp(inStyle, 'multi-chan');
            subplot(2,5,1);
            [handles] = dispRecodedIm(compIm, 'multi-chan');
            subplot(2,5,2);
            [handles] = dispRecodedIm(compRecIm, 'multi-chan');
        else            
            outIm = compIm; 
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(1,5,1); image(outFill); daspect([1 1 1]);

            outIm = squeeze(compRecIm(:,:,:,1,1,1)); 
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(1,5,2); image(outFill); daspect([1 1 1]);
        end
        
        % display non-sparse recoded hidden image
        subplot(1,5,3);
        [recodedHiddenIm_raw] = recodeMultiChan(compHiddenIm_raw, filtShape);
        [handles] = dispRecodedIm(recodedHiddenIm_raw, 'multi-chan');
        
        % disply non-sparse hidden image adjusted for repDensity
        subplot(1,5,4);
        minRepDensity = mean(repDensity(:))*.01;
        repDensity_norm = repDensity;
        repDensity_norm(repDensity<(minRepDensity)) = 1;
        
        
        disp(squeeze(repDensity_norm(1,1,1,1,1,:)));
        compHiddenIm_adj = mdaTimes(compHiddenIm_raw, 1/repDensity_norm, [], 'yes');        
        [recodedHiddenIm_adj] = recodeMultiChan(compHiddenIm_adj, filtShape);
        [handles] = dispRecodedIm(recodedHiddenIm_adj, 'multi-chan');

        
        recodedHiddenIm = [];
        subplot(1,5,5);
        if strcmp(recodeStyle, 'vector')
            [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'vector');
        elseif strcmp(recodeStyle, 'multi-chan')
            [recodedHiddenIm] = recodeMultiChan(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
        elseif strcmp(recodeStyle, 'full_orient_chan')
            [recodedHiddenIm] = recodeFullOrientChan(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
        end
        
        % clean up images with simple threshold
        
        recodedHiddenIm = CleanHiddenIm(recodedHiddenIm,.025);
        
        size(recodedHiddenIm)

        if strcmp(saveOut, 'yes')
            cd ..;
            cd Hidden_Image_Sets;
            cd(outFolder);
            save([name_c{s} '.mat'], 'recodedHiddenIm');
            %save([name_c{s} '.mat'], 'recodedHiddenIm_adj');
            cd ..;
            cd ..;
            cd Convolution_nets_ND;
        end
        pause(pauseTime);
        
    end
    
%     % display similarity statistics
%     similarityFig = figure();
%     set(0, 'CurrentFigure', similarityFig);
%     colormap('hot');
%     subplot(2,4,5:8);
%     imagesc(sSignatures'); daspect([1 1 1]);
%     subplot(2,4,1);
%     
%     similarityMatrix = sSignatures*sSignatures';
%     covMatrix = cov(sSignatures');
%     imagesc( (covMatrix)); daspect([1 1 1/numS]);
% 
%     % linsolve to find 1-step best fit weights
%     % show classiffication confidence
%     % this looks before the recode
%     I = eye(numS);
%     A = sSignatures;
%     B = I';
%     X = linsolve(A, B);
%     
%     R = X'*A';
%     subplot(2,4,2);
%     imagesc(R); daspect([1 1 1]);
%     
%     RmaxV = repmat(max(R, [], 1), [numS 1]);
%     Rmax = R==RmaxV;
%     
%     subplot(2,4,3);
%     imagesc(Rmax); daspect([1 1 1]);
%     
%     subplot(2,4,4); 
%     
%     % Get correct per orientation
%     numS
%     corCheck = kron(eye(numS/inOrients),ones(inOrients));
%     incorCheck = 1-corCheck;
%     
%     total = sum(reshape(sum(Rmax, 1), inOrients, numS/inOrients),2);
%     totalCor = sum(reshape(sum(Rmax.*corCheck, 1), inOrients, numS/inOrients), 2);
%     
%     plot((1:inOrients)/inOrients*360, totalCor./total*100);
%     ylim([0 100]);  daspect([1/100 1/360 1]);
    
    
    
end

