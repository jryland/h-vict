function [ ] = runLayerOnStimuli(  )

    load networkParam;
    baseRep = network.baseRep;
    RBTenser = network.RBF;
    tenserShape = network.tenserShape;
    matShape = network.matShape;
    filtShape = network.filtShape;
    
    numChan = filtShape(3);
    numRep = filtShape(6);
    
    compIm_c = loadStimuli(50, 50, 'grey');
    numS = length(compIm_c);
    
    outIm = [];
    compHiddenIm = [];
    compRecIm = [];
    
    fig = figure();
    
    for s = 1:numS
        
        disp(['Processing Image ' mat2str(s)]);

        compIm = compIm_c{s};
        
        % get filter stack 
        %disp('1. Getting Filters from feature Template');
        [filtTenser, filtShape] = getFilts(baseRep, RBTenser, tenserShape, matShape, numChan, numRep);

        % get hidden image
        %disp('2. Getting Hidden Image from Input Image');
        [ compHiddenIm, recodedHiddenIm_raw] = getHiddenIm( compIm, filtTenser, filtShape);

        % get reconstructed input image
        %disp('3. Getting Reconstructed Input Image from Hidden Image');
        [ compRecIm ] = getRecIm(compHiddenIm, filtTenser, filtShape);

        outIm = compIm; 
        outIm = outIm-min(outIm(:));
        outIm = outIm/max(outIm(:));
        nr = min(size(outIm, 3),3);
        outFill = zeros(size(outIm,1),size(outIm,2), 3);
        outFill(:,:,1:nr) = outIm(:,:,1:nr); 
        subplot(1,4,1); image(outFill); daspect([1 1 1]);


        outIm = squeeze(compRecIm(:,:,:,1,1,1)); 
        outIm = outIm-min(outIm(:));
        outIm = outIm/max(outIm(:));
        nr = min(size(outIm, 3),3);
        outFill = zeros(size(outIm,1),size(outIm,2), 3);
        outFill(:,:,1:nr) = outIm(:,:,1:nr); 
        subplot(1,4,2); image(outFill); daspect([1 1 1]);
        
        [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
        subplot(1,4,4);
        [handles] = dispRecodedIm(recodedHiddenIm);
        
        pause(.3);
        
    end
    
    % more display stuff;
    outIm = compIm; 
    outIm = outIm-min(outIm(:));
    outIm = outIm/max(outIm(:));
    nr = min(size(outIm, 3),3);
    outFill = zeros(size(outIm,1),size(outIm,2), 3);
    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
    subplot(1,4,1); image(outFill); daspect([1 1 1]);
    
    outIm = squeeze(compRecIm(:,:,:,1,1,1)); 
    outIm = outIm-min(outIm(:));
    outIm = outIm/max(outIm(:));
    nr = min(size(outIm, 3),3);
    outFill = zeros(size(outIm,1),size(outIm,2), 3);
    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
    subplot(1,4,2); image(outFill); daspect([1 1 1]);
    
    for i=1:size(compHiddenIm,4)
        
        
        outIm = squeeze(compHiddenIm(:,:,1,i,1,:)); 
        outIm = max(outIm,0);
        outIm = outIm/max(outIm(:));
        nr = min(size(outIm, 3),3);
        outFill = zeros(size(outIm,1),size(outIm,2), 3);
        outFill(:,:,1:nr) = outIm(:,:,1:nr); 
        subplot(1,4,3); imagesc(outFill); daspect([1 1 1]);
        pause(.1);
    end


end

