function [  ] = trainLayerAnneal( inputFolder, inputInfo, networkInfo, paramFileName, numEpochs)
% I'm Sorry this is a very long function that can and should be simplified
% in the future. The step size manipulation logic could be mostly removed
% and the gradient descent should work fine with a small enough step size.
% also Tensor is consistently mispelled Tensor. When I feel like maybe
% breaking everything, I will fix that.

% I would suggest keeping the cooldownTimeGPU if your computer does not
% cool its GPU in the best manner. This will cause the process to run
% slower than nescessary, while still orders of magnitude faster than
% computing the convolutions with CPUs.

% For most people I would recomment modifying the Build function rather
% than directly manipulating this one.

    %Some initialization parameters
    baseRez = networkInfo.baseRez;  % the rezolution of the base feature representations
    filtRez = networkInfo.filtRez;  % the rezolution of the filters
    numChan = networkInfo.numChan;  % the number of input channels
    numRep = networkInfo.numRep;    % the number of high level feature representations
    numOrients = networkInfo.numOrients;
    numScales = networkInfo.numScales;
    learnRate = networkInfo.startRate;
    recodeStyle = networkInfo.recodeStyle;
    sparseStyle = networkInfo.sparseStyle;
    
    
    % may remove
    rRate = networkInfo.rRate;
    oRate = networkInfo.oRate;
    stepUpTolerence = networkInfo.stepUpTolerance;
    
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    
    if strcmp(inStyle, 'vector')&&mod(numChan, 4)~=0
        disp('Error: number of channels must be a multiple of 4, to code orientation and scale as two 2D vectors.');
        disp('If input is not vector coded please change inputInfo.inStyle to grey, full, or []');
    end
    
    minLearnRate = learnRate*10^(-8);
    epochs = numEpochs; % number of times to go through the whole training set;
    
    cooldownTimeGPU = 10;
    
    
    % load cell array of multi-channel stimuli/ or composite images 
    compIm_c = loadInputFrom(inputFolder, inRez, inNum, inStyle, inHidden);
    %compIm_c = loadNoiseStimuli(25, 50, 'grey', 'circle');
    
    numS = length(compIm_c)
    if numS>inNum
        numS = inNum
        compIm_c = compIm_c(1:inNum);
    end
    
    % Create a radial basis constraint matrix to relate a set of filters to
    % a set of base feature space
    [RBTensor, TensorShape, matShape, numChan_, numRep_] = getMultiRBFmat(networkInfo);
    
    
    % Generate a random set of base features
    baseRep = RandBase(networkInfo);
    baseRepOld = baseRep;
    
    pause(.1)
    
    compHiddenIm = [];
    compIm = [];
    compRecIm = [];
    ErrorNorm = 0;
    errorDecreasingRun = 1;
    errorIncreasingRun = 0;
    stop = 0;
    filtTensor = [];
    filtShape = [];

    repDensity = zeros(1,1,1,1,1,numRep);
    
    resample = 0;    
    
    numNodesH = [];
    
    bestBaseRep = [];
    bestError = [];
    
    gain = 1;
    
    errorFig = figure();
    errorNum = [0];

    objHist = 0;
    oldObj = 0;
    obj = 0;
    bestObj = 0;
    lineHandle4 = plot(objHist,errorNum);
    set(lineHandle4, 'YData', objHist, 'XData', errorNum);
    
    
    similarityFig = figure();
    sSignatures = zeros(numS, numRep);
    sSig_raw = zeros(numS, numRep);
    similarityMatrix = [];
    
    
    disp(['Number of Stimuli ' num2str(numS)]);
    
    mainFigure = figure();
    
    
    for ep = 1:epochs
        
        
        if mod(ep,50)==0
            disp('$$$$$$$ Allowing GPU to cool 30 seconds $$$$$$');
            pause(cooldownTimeGPU);
        end
        
        % important variables to reset for correctly calculating batch
        % averages
        ErrorNormNew = 0;
        objNew = 0;
        avgHiddenResponse = 0;
        sumHiddenSquares = 0;
        
        % this will help adjust the gain
        averageInMag = 0;
        averageRecMag = 0;
        
        %use this if you are doing inverse frequency densities 
        %repDensity = zeros(1,1,1,1,1,numRep);
        
        % randomly very the the feature templates

        oldBaseRep = baseRep;
        
        old = .6;
        
        if ep~=1
            
            randRepNum = ceil(rand(1,1)*numRep)
            randRepNum = randRepNum + (randRepNum==0)
            randAll = RandBase(networkInfo);
            baseRep(:,:,randRepNum) = old*baseRep(:,:,randRepNum) + (1-old)*randAll(:,:,randRepNum);
            
        end
        
        baseRep_norm = (baseRep-mean(baseRep(:)));
        baseRep_norm = baseRep_norm/mean(abs(baseRep_norm(:)));
            
        %  find the gradient and error for the batch
        for s=1:numS
            
            
            suppressItemOutput = 1;
            
            if(~suppressItemOutput)
                disp(['Epoch: ' mat2str(ep) '  Stimuli: '  mat2str(s)]);
            elseif s == 1
                disp(['Epoch: ' mat2str(ep) ]);
            end
            compIm = compIm_c{s};
            

            % get filter stack 
            %disp('1. Getting Filters from feature Template');
            [filtTensor, filtShape] = getFilts(baseRep_norm, RBTensor, TensorShape, matShape, numChan, numRep, inStyle);

            
            
            % get hidden image
            %disp('2. Getting Hidden Image from Input Image');
            [ compHiddenIm, compHiddenIm_raw] = getHiddenIm( compIm, filtTensor, filtShape, sparseStyle);
            
            % get reconstructed input image
            %disp('3. Getting Reconstructed Input Image from Hidden Image');
            [ compRecIm ] = getRecIm(compHiddenIm, filtTensor, filtShape);
            
            % adjust the gain on the compRecIm 
            compRecIm = compRecIm*gain; %compRecIm*gain;
            
            
            % collecting things for later
            numNodesH = length(compHiddenIm(:));
            avgHiddenResponse = avgHiddenResponse + mean(abs(compHiddenIm(:)/numNodesH))/numS;
            sumHiddenSquares = sumHiddenSquares+sum(compHiddenIm(:).^2)/numS;
            
            
            
            %sDensity = sum(sum(sum(sum(sum(compHiddenIm_raw,1),2),3),4),5)/numS;
            sDensity = sum(sum(sum(sum(sum(compHiddenIm,1),2),3),4),5)/numS;
            repDensity = repDensity+sDensity;
            sSignatures(s,:) = sDensity(:)/norm(sDensity(:));
            sSig_raw(s,:) = sDensity(:);
            sHiddenImMats(:,:,:,:,:,:,s) = compHiddenIm;
            sCompIms(:,:,:,s) = compIm;
            
            averageInMag = averageInMag + norm(compIm(:))/numS;
            averageRecMag = averageRecMag + norm(compRecIm(:))/numS;
            
            
            
            
            % display some pics :) to check what you are doing %%%%%%%%%%%
            if mod(ep*numS+s, 2)==0
                
                set(0, 'CurrentFigure', mainFigure);
                
                if strcmp(inStyle, 'vector')
                    subplot(3,4,1);
                    [handles] = dispRecodedIm(compIm);
                    subplot(3,4,2);
                    [handles] = dispRecodedIm(compRecIm);
                elseif strcmp(inStyle, 'multi-chan');
                    subplot(3,4,1);
                    [handles] = dispRecodedIm(compIm, 'multi-chan');
                    subplot(3,4,2);
                    [handles] = dispRecodedIm(compRecIm, 'multi-chan');
                else
                    outIm = compIm;
                    inMin = min(outIm(:));
                    inMax = max(outIm(:));
                    outIm = outIm-min(outIm(:));
                    outIm = outIm/max(outIm(:));
                    nr = min(size(outIm, 3),3);
                    outFill = zeros(size(outIm,1),size(outIm,2), 3);
                    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
                    subplot(3,4,1); image(outFill); daspect([1 1 1]);
                    
                    outIm = compRecIm;
                    outIm = outIm-min(outIm(:));
                    outIm = outIm/max(outIm(:));
                    %outIm = outIm-inMin;
                    %outIm = outIm/inMax;
                    outIm = max(min(outIm, 1), 0);
                    
                    nr = min(size(outIm, 3),3);
                    outFill = zeros(size(outIm,1),size(outIm,2), 3);
                    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
                    subplot(3,4,2); image(outFill); daspect([1 1 1]);
                end
                
                % display non-sparse recoded hidden image
                subplot(3,4,3);
                [recodedHiddenIm_raw] = recodeMultiChan(compHiddenIm_raw, filtShape);
                [handles] = dispRecodedIm(recodedHiddenIm_raw, 'multi-chan');
                
                % display recoded hidden image
                subplot(3,4,4);
                if strcmp(recodeStyle, 'vector')
                    [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
                    [handles] = dispRecodedIm(recodedHiddenIm, 'vector');
                elseif strcmp(recodeStyle, 'multi-chan')
                    [recodedHiddenIm] = recodeMultiChan(compHiddenIm, filtShape);
                    [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
                end
            end
            
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            pause(.01);
        end
        
        
        % Calculate similarity and seperability 
        mSig = sSignatures-ones(numS,1)*mean(sSignatures,1);
        
        similarityMatrix = mSig*mSig';
        covMatrix = cov(mSig');
        
        % linsolve to find 1-step best fit weights
        % show classiffication confidence
        I = eye(numS);
        A = mSig;
        B = I';
        X = linsolve(A, B);

        R = X'*A';

        RmaxV = repmat(max(R, [], 1), [numS 1]);
        Rmax = R==RmaxV;

        
        
        % Calculate Objective function
        O = 1 - kron(eye(numS/numOrients), ones(numOrients))*2;
        Q = (sSig_raw*sSig_raw');
        
        orthoReg1 = sum(O(:).*Q(:).^2);
        
        O2 = kron(eye(numS/numOrients), ones(numOrients));
        Q2 = sSignatures*sSignatures';
        orthoReg2 = sum((O2(:)-Q2(:)).^2);
        
        Q3 = R;
        orthoReg3 = sum((O2(:)-Q3(:)).^2)
        
        weightDecay = sum(baseRep(:).^2); 
        
        obj = orthoReg3;
        
        
        % Evaluate Objective function, and decide whether to keep old or
        % new
        
        temp = .999;
        moveToNew = 0;
        
        if obj<oldObj || ep==1
            
            baseRepOld = baseRep;
            oldObj = obj;
            bestObj = obj;
            bestBaseRep = baseRep;
            moveToNew = 1;
            
        else
            
            coin = rand(1,1)>temp;
            
            if coin
                baseRepOld = baseRep;
                oldObj = obj;
                moveToNew = 1;
            else
                baseRep = baseRepOld;
                obj = oldObj;
            end
        end
        
        
        if obj<bestObj
            bestObj = obj;
            bestBaseRep = baseRep;
        end
        
        
        
        
        % Image the filters
        set(0, 'CurrentFigure', mainFigure);
        
        subplot(3,4,[5 6 7 8 9 10 11 12]); imagesc(filtDispImage(filtTensor, filtShape));
        daspect([1 1 1]);
        
        colormap('gray');
        pause(.01);
        
        
        disp(['Average In  Mag: ' num2str(averageInMag)]);
        disp(['Average Out Mag: ' num2str(averageRecMag)]);
        %disp(['Epoch Reconstruction Error:        ' mat2str(ErrorNormNew)]);
        disp(['Epoch Ortho Reg Error              ' mat2str(orthoReg3)]);
        disp(['Epoch Objective function Error:    ' mat2str(obj)]);
        disp(' ');
        
        if stop
            break;
        end
        if learnRate == 0;
            disp('Display Finale Network Error');
            stop = 1;
        end
        
        
        % plot final error %%%%%%%%%%%%%%%%%%%%%%%%
        set(0, 'CurrentFigure', errorFig);
        errorNum = [errorNum ep];
        objHist = [objHist obj];
        set(lineHandle4, 'YData', objHist, 'XData', errorNum);
        
        
        % display similarity and seperability info %%%%%%
        if moveToNew
            
            set(0, 'CurrentFigure', similarityFig);
            colormap('hot');
            
            
            subplot(3,3,4:6);
            imagesc(sSignatures'); daspect([1 1 1]);

            subplot(3,3,7:9);
            imagesc(sSig_raw'); daspect([1 1 1]);
            
            %imagesc( (covMatrix)); daspect([1 1 1/numS]);

            subplot(3,3,1);
            imagesc( (similarityMatrix)); daspect([1 1 1/numS]);
            
            subplot(3,3,2);
            imagesc(R); daspect([1 1 1]);
            
            subplot(3,3,3);
            imagesc(Rmax); daspect([1 1 1]);
        end
        
        % update repDensity
        if moveToNew||ep==1
            % use this if basing on classification importance
            %Xsmall = X/max(abs(X(:)));
            %repDensity(1,1,1,1,1,:) = sum(abs(Xsmall), 2);
            
            % just do this if using inverse frequesncy norm
            repDensity = repDensity/max(repDensity(:));
            
        end
        
        
        
        % save parameters periodically %%%%%%%%%%%%%
        if mod(ep,2) == 0
            disp(' ');
            disp('-----------------------Saveing Network Parameters-----------------------');
            disp(' ');
            network.baseRep = bestBaseRep;
            network.RBF = RBTensor;
            network.TensorShape = TensorShape;
            network.matShape = matShape;
            network.filtShape = filtShape;
            network.recodeStyle = recodeStyle;
            network.repDensity = repDensity;
            cd ..;
            cd Layer_Params;
            save(paramFileName, 'network');
            cd ..;
            cd Convolution_Nets_ND;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    % final parameters
    disp(' ');
    disp('-----------------------Saveing Network Parameters-----------------------');
    disp(' ');
    network.baseRep = bestBaseRep;
    network.RBF = RBTensor;
    network.TensorShape = TensorShape;
    network.matShape = matShape;
    network.filtShape = filtShape;
    network.recodeStyle = recodeStyle;
    network.repDensity = repDensity;
    
    cd ..;
    cd Layer_Params;
    save(paramFileName, 'network');
    cd ..;
    cd Convolution_Nets_ND;
    
    


end

