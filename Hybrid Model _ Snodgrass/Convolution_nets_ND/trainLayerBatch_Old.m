function [  ] = trainLayerBatch_Old( inputFolder, inputInfo, networkInfo, paramFileName, numEpochs)
% I'm Sorry this is a very long function that can and should be simplified
% in the future. The step size manipulation logic could be mostly removed
% and the gradient descent should work fine with a small enough step size.
% also Tensor is consistently mispelled tenser. When I feel like maybe
% breaking everything, I will fix that.

% I would suggest keeping the cooldownTimeGPU if your computer does not
% cool its GPU in the best manner. This will cause the process to run
% slower than nescessary, while still orders of magnitude faster than
% computing the convolutions with CPUs.

% For most people I would recomment modifying the Build function rather
% than directly manipulating this one.

    %Some initialization parameters
    baseRez = networkInfo.baseRez;  % the rezolution of the base feature representations
    filtRez = networkInfo.filtRez;  % the rezolution of the filters
    numChan = networkInfo.numChan;  % the number of input channels
    numRep = networkInfo.numRep;    % the number of high level feature representations
    numOrients = networkInfo.numOrients;
    numScales = networkInfo.numScales;
    learnRate = networkInfo.startRate;
    recodeStyle = networkInfo.recodeStyle;
    sparseStyle = networkInfo.sparseStyle;
    
    
    % may remove
    rRate = networkInfo.rRate;
    oRate = networkInfo.oRate;
    stepUpTolerence = networkInfo.stepUpTolerance;
    
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    
    if strcmp(inStyle, 'vector')&&mod(numChan, 4)~=0
        disp('Error: number of channels must be a multiple of 4, to code orientation and scale as two 2D vectors.');
        disp('If input is not vector coded please change inputInfo.inStyle to grey, full, or []');
    end
    
    minLearnRate = learnRate*10^(-8);
    epochs = numEpochs; % number of times to go through the whole training set;
    
    cooldownTimeGPU = 30;
    
    
    % load cell array of multi-channel stimuli/ or composite images 
    compIm_c = loadInputFrom(inputFolder, inRez, inNum, inStyle, inHidden);
    %compIm_c = loadNoiseStimuli(25, 50, 'grey', 'circle');
    
    numS = length(compIm_c);
    
    % Create a radial basis constraint matrix to relate a set of filters to
    % a set of base feature space
    [RBTenser, tenserShape, matShape, numChan_, numRep_] = getMultiRBFmat(networkInfo);
    
    
    % Generate a random set of base features
    baseRep = RandBase(networkInfo);
    baseRepOld = baseRep;
    
    pause(.1)
    
    compHiddenIm = [];
    compIm = [];
    compRecIm = [];
    ErrorNorm = 0;
    errorDecreasingRun = 1;
    errorIncreasingRun = 0;
    stop = 0;
    filtTenser = [];
    filtShape = [];
    filtErrorTenser_batch = zeros(filtRez, filtRez, numChan, numOrients, numScales, numRep);
    repDensity = zeros(1,1,1,1,1,numRep);
    
    resample = 0;    
    
    numNodesH = [];
    
    bestBaseRep = [];
    bestError = [];
    
    gain = 1;
    
    errorFig = figure();
    errorHist = [0];
    errorNum = [0];
    subplot(1,4,1);
    lineHandle1 = plot(errorHist,errorNum);
    set(lineHandle1, 'YData', errorHist, 'XData', errorNum);
    
    avgRespHist = 0;
    avgHiddenResponse = 0;
    sumHiddenSquares = 0;
    subplot(1,4,2);
    lineHandle2 = plot(errorHist,errorNum);
    set(lineHandle2, 'YData', avgRespHist, 'XData', errorNum);
    
    orthoHist = 0;
    orthoReg = 0;
    subplot(1,4,3);
    lineHandle3 = plot(orthoHist,errorNum);
    set(lineHandle3, 'YData', orthoHist, 'XData', errorNum);
    
    
    objHist = 0;
    oldObj = 0;
    obj = 0;
    bestObj = 0;
    subplot(1,4,4);
    lineHandle4 = plot(objHist,errorNum);
    set(lineHandle4, 'YData', objHist, 'XData', errorNum);
    
    
    similarityFig = figure();
    sSignatures = zeros(numS, numRep);
    similarityMatrix = [];
    
    
    baseErrorTenser_Batch = zeros(size(baseRep));
    
    disp(['Number of Stimuli ' num2str(numS)]);
    
    mainFigure = figure();
    
    for ep = 1:epochs
        
        
        if mod(ep,20)==0
            disp('$$$$$$$ Allowing GPU to cool 30 seconds $$$$$$');
            pause(cooldownTimeGPU);
        end
        
        % important variables to reset for correctly calculating batch
        % averages
        ErrorNormNew = 0;
        objNew = 0;
        avgHiddenResponse = 0;
        sumHiddenSquares = 0;
        repDensity = zeros(1,1,1,1,1,numRep);
        
        % this will help adjust the gain
        averageInMag = 0;
        averageRecMag = 0;
        
        
        baseErrorTenser_Batch = zeros(size(baseRep));
        filtErrorTenser_batch = zeros(filtRez, filtRez, numChan, numOrients, numScales, numRep);
    
        
        %  find the gradient and error for the batch
        for s=1:numS
            
            
            suppressItemOutput = 1;
            
            if(~suppressItemOutput)
                disp(['Epoch: ' mat2str(ep) '  Stimuli: '  mat2str(s)]);
            elseif s == 1
                disp(['Epoch: ' mat2str(ep) ]);
            end
            compIm = compIm_c{s};

            % get filter stack 
            %disp('1. Getting Filters from feature Template');
            [filtTenser, filtShape] = getFilts(baseRep, RBTenser, tenserShape, matShape, numChan, numRep, inStyle);

            % get hidden image
            %disp('2. Getting Hidden Image from Input Image');
            [ compHiddenIm, compHiddenIm_raw] = getHiddenIm( compIm, filtTenser, filtShape, sparseStyle);
            numNodesH = length(compHiddenIm(:));
            avgHiddenResponse = avgHiddenResponse + mean(abs(compHiddenIm(:)/numNodesH))/numS;
            sumHiddenSquares = sumHiddenSquares+sum(compHiddenIm(:).^2)/numS;
            
            % get reconstructed input image
            %disp('3. Getting Reconstructed Input Image from Hidden Image');
            [ compRecIm ] = getRecIm(compHiddenIm, filtTenser, filtShape);
            
            % adjust the gain on the compRecIm 
            compRecIm = compRecIm*gain; %compRecIm*gain;
            
            % get convolution derivative
            %disp('4. Getting Reconstructed Input Image from Hidden Image');
            [ filtErrorTenser, compImError] = getConvDeriv(compIm, compHiddenIm, compRecIm, rRate);
            
            filtErrorTenser_batch = filtErrorTenser_batch + filtErrorTenser; 
            
            % collecting things for later
            ErrorNormNew = ErrorNormNew + norm(compImError(:))^2/numS;
            sDensity = sum(sum(sum(sum(sum(compHiddenIm_raw,1),2),3),4),5)/numS;
            repDensity = repDensity + sDensity;
            sSignatures(s,:) = sDensity(:)/norm(sDensity(:));
            sSig_raw(s,:) = sDensity(:);
            sHiddenImMats(:,:,:,:,:,:,s) = compHiddenIm;
            sCompIms(:,:,:,s) = compIm;
            
            averageInMag = averageInMag + norm(compIm(:))/numS;
            averageRecMag = averageRecMag + norm(compRecIm(:))/numS;
            
            
            % get error projected into base feature space

            % apply gradient descent to base features
            %disp('5. Apply Gradiant Descent on Feature Template');
            %filtErrorTenser = zeros(filtShape);
            baseErrorTenser = baseProjection(RBTenser, matShape, filtErrorTenser, filtShape, inStyle);
            
            baseErrorTenser_Batch = baseErrorTenser/numS + baseErrorTenser_Batch; 
            
            if(~suppressItemOutput)
                disp(['Reconstruction Error: ' mat2str(norm(compImError(:))^2)]);
                disp(['Magnitude Im          ' mat2str(norm(compIm(:))   )]);
                disp(['Magnitude Rec         ' mat2str(norm(compRecIm(:)))]);
                disp(' ');
            end
            
            
            
            % display some pics :) to check what you are doing %%%%%%%%%%%
            if mod(ep*numS+s, 2)==0
                
                set(0, 'CurrentFigure', mainFigure);
                
                if strcmp(inStyle, 'vector')
                    subplot(3,4,1);
                    [handles] = dispRecodedIm(compIm);
                    subplot(3,4,2);
                    [handles] = dispRecodedIm(compRecIm);
                elseif strcmp(inStyle, 'multi-chan');
                    subplot(3,4,1);
                    [handles] = dispRecodedIm(compIm, 'multi-chan');
                    subplot(3,4,2);
                    [handles] = dispRecodedIm(compRecIm, 'multi-chan');
                else
                    outIm = compIm;
                    inMin = min(outIm(:));
                    inMax = max(outIm(:));
                    outIm = outIm-min(outIm(:));
                    outIm = outIm/max(outIm(:));
                    nr = min(size(outIm, 3),3);
                    outFill = zeros(size(outIm,1),size(outIm,2), 3);
                    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
                    subplot(3,4,1); image(outFill); daspect([1 1 1]);
                    
                    outIm = compRecIm;
                    outIm = outIm-min(outIm(:));
                    outIm = outIm/max(outIm(:));
                    %outIm = outIm-inMin;
                    %outIm = outIm/inMax;
                    outIm = max(min(outIm, 1), 0);
                    
                    nr = min(size(outIm, 3),3);
                    outFill = zeros(size(outIm,1),size(outIm,2), 3);
                    outFill(:,:,1:nr) = outIm(:,:,1:nr); 
                    subplot(3,4,2); image(outFill); daspect([1 1 1]);
                end
                
                % display non-sparse recoded hidden image
                subplot(3,4,3);
                [recodedHiddenIm_raw] = recodeMultiChan(compHiddenIm_raw, filtShape);
                [handles] = dispRecodedIm(recodedHiddenIm_raw, 'multi-chan');
                
                % display recoded hidden image
                subplot(3,4,4);
                if strcmp(recodeStyle, 'vector')
                    [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
                    [handles] = dispRecodedIm(recodedHiddenIm, 'vector');
                elseif strcmp(recodeStyle, 'multi-chan')
                    [recodedHiddenIm] = recodeMultiChan(compHiddenIm, filtShape);
                    [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            pause(.1);
        end
        
        
        %CALCULATE THE THE ORTHOGANAL REGUALARIZATION TERM/DERIVATIVE %%%%%
        disp('Calculating Orthonal Regularization Term ');
        
        %Need sSignatures
        [filtErrorTensorO, orthoReg ]= getOrthoDeriv(sSig_raw, sHiddenImMats, sCompIms, inOrients, filtShape);
        
        % should only need to be done once
        baseErrorTensorO = baseProjection(RBTenser, matShape, filtErrorTensorO, filtShape, inStyle);
        
        
        baseErrorTenser_Batch = baseErrorTenser+baseErrorTensorO*oRate;
        
        disp('Calculating Orthonal Regularization Term Complete');
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        
        % Image the error
        set(0, 'CurrentFigure', mainFigure);
        
        subplot(3,4,[5 6 9 10]); imagesc(filtDispImage(filtTenser, filtShape));
        daspect([1 1 1]);
        subplot(3,4,[7 8 11 12]); imagesc(filtDispImage(-filtErrorTenser_batch, filtShape)); 
        daspect([1 1 1]);
        
        colormap('gray');
        pause(5);
        
        
        % apply limit to gradient
        gradMax = max(abs(baseErrorTenser_Batch(:)));
        gradNorm = norm(baseErrorTenser_Batch(:));
        
        disp(['Grad Max:   ' mat2str(gradMax)]);
        gradLimit = 1;
        if (gradMax>gradLimit);
            baseErrorTenser_Batch = baseErrorTenser_Batch*gradLimit/gradNorm;
        end
        
        %apply gradient descent batch style % check check the sign
        baseRepNew = baseRep - learnRate * baseErrorTenser_Batch;
        
        
        % full objective function .. with sparse learning..
        % obj = (ErrorNormNew + rRate * ((numNodesH*rGoal)^2 - avgHiddenResponse^2)^2);
        objNew = ErrorNormNew + rRate * sumHiddenSquares + oRate * orthoReg;
        
        
        
        % Check a liteny of things %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Adjust the gain in the first step
        if (ep==1)
            gain = averageInMag/averageRecMag;
            %gain = 1;
            disp('**** Adjusting Gain *****')
            
            learnRate = networkInfo.startRate;
            obj = objNew;
            baseRep = baseRepOld;
            ErrorNorm = ErrorNormNew;
            bestBaseRep = baseRep;
            bestError = ErrorNorm;
            
        % check to see if network should restart
        elseif errorIncreasingRun>10
            % Generate a random set of base features
            baseRep = RandBase(networkInfo);
            baseRepOld = baseRep;
            errorIncreasingRun = 0;
            resample = 1;
            disp('%%%%%%%%%%% Re-initializing with new Parameters');
            
        elseif resample
             % update parameters and error benchmark if restarting
            baseRepOld = baseRep;
            baseRep = baseRepNew;
            obj = objNew;
            ErrorNorm = ErrorNormNew;
            resample = 0;
            
        % check to see if step size should be lowered and keep the old
        % representation and error, or if the step size should be raised,
        % or simply update the old baseRep and ErrorNorm
        elseif ( ErrorNormNew>(stepUpTolerence*ErrorNorm) || isnan(ErrorNormNew)) && (ep>2) && (~resample)
            learnRate = learnRate * .1;
            
            baseRep = baseRepOld;
            
            disp('**** Step Size Decreased Progress Halt ****');
            disp('**** Next Is a Repeat of Previous      ****');
            
            disp(['Step Size             ' mat2str(learnRate)]);
            
            if learnRate < minLearnRate
               disp('Step Size Too Small Halting Completely');
               learnRate = 0;
            end
            
            errorDecreasingRun = 0;
            %errorIncreasingRun = errorIncreasingRun+1;
            
            
        else
            % keep track of how many times the error is raised or lowered
            % contiguousely...
            if errorDecreasingRun>3
                learnRate = learnRate * 5;
                disp('**** Step Size Raised ****');

                disp(['Step Size             ' mat2str(learnRate)]);

                errorDecreasingRun = 0;
            elseif ( ErrorNormNew<(ErrorNorm) )
                errorDecreasingRun = errorDecreasingRun + 1;
                errorIncreasingRun = 0;
                disp(['Error Decreasing Run ' num2str(errorDecreasingRun)]);
            else 
                errorDecreasingRun = 0;  
                errorIncreasingRun = errorIncreasingRun + 1;
                disp(['Error Increasing Run ' num2str(errorIncreasingRun)]);
            end
            
            
            % update parameters and error benchmark
            baseRepOld = baseRep;
            baseRep = baseRepNew;
            ErrorNorm = ErrorNormNew;
            obj = objNew;
        end
        
        
        %check if best bestBaseRep should be changed
        if ErrorNorm<bestError;
            disp('#### BEST ####');
            bestBaseRep = baseRep;
            bestError = ErrorNorm;
            bestObj = obj;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        disp(['Average In  Mag: ' num2str(averageInMag)]);
        disp(['Average Out Mag: ' num2str(averageRecMag)]);
        disp(['Epoch Reconstruction Error:        ' mat2str(ErrorNormNew)]);
        disp(['Epoch Average Hidden Response:     ' mat2str(sum(avgHiddenResponse(:)^2))]);
        disp(['Epoch Ortho Reg Error              ' mat2str(orthoReg)]);
        disp(['Epoch Objective function Error:    ' mat2str(obj)]);
        disp(' ');
        
        if stop
            break;
        end
        if learnRate == 0;
            disp('Display Finale Network Error');
            stop = 1;
        end
        
        % plot final error %%%%%%%%%%%%%%%%%%%%%%%%
        set(0, 'CurrentFigure', errorFig);
        errorHist = [errorHist ErrorNorm];
        errorNum = [errorNum ep];
        set(lineHandle1, 'YData', errorHist, 'XData', errorNum);
        
        avgRespHist = [avgRespHist avgHiddenResponse];
        set(lineHandle2, 'YData', avgRespHist, 'XData', errorNum);
        
        orthoHist = [orthoHist orthoReg];
        set(lineHandle3, 'YData', orthoHist, 'XData', errorNum);
        
        objHist = [objHist obj];
        set(lineHandle4, 'YData', objHist, 'XData', errorNum);
        
        % similarity and seperability info
        set(0, 'CurrentFigure', similarityFig);
        colormap('hot');
        subplot(2,3,4:6);
        
        mSig = sSignatures-ones(numS,1)*mean(sSignatures,1);
        
        imagesc(mSig'); daspect([1 1 1]);
        subplot(2,3,1);

        %similarityMatrix = sSignatures*sSignatures';
        %covMatrix = cov(sSignatures');
        %imagesc( (covMatrix)); daspect([1 1 1/numS]);

        similarityMatrix = mSig*mSig';
        covMatrix = cov(mSig');
        imagesc( (covMatrix)); daspect([1 1 1/numS]);
        
        
        % linsolve to find 1-step best fit weights
        % show classiffication confidence
        I = eye(numS);
        A = mSig;
        B = I';
        X = linsolve(A, B);

        R = X'*A';
        subplot(2,3,2);
        imagesc(R); daspect([1 1 1]);

        RmaxV = repmat(max(R, [], 1), [numS 1]);
        Rmax = R==RmaxV;

        subplot(2,3,3);
        imagesc(Rmax); daspect([1 1 1]);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % save parameters periodically %%%%%%%%%%%%%
        if mod(ep,10) == 0
            disp(' ');
            disp('-----------------------Saveing Network Parameters-----------------------');
            disp(' ');
            network.baseRep = bestBaseRep;
            network.RBF = RBTenser;
            network.tenserShape = tenserShape;
            network.matShape = matShape;
            network.filtShape = filtShape;
            network.recodeStyle = recodeStyle;
            network.repDensity = repDensity;
            cd ..;
            cd Layer_Params;
            save(paramFileName, 'network');
            cd ..;
            cd Convolution_Nets_ND;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    % final parameters
    disp(' ');
    disp('-----------------------Saveing Network Parameters-----------------------');
    disp(' ');
    network.baseRep = bestBaseRep;
    network.RBF = RBTenser;
    network.tenserShape = tenserShape;
    network.matShape = matShape;
    network.filtShape = filtShape;
    network.recodeStyle = recodeStyle;
    network.repDensity = repDensity;
    
    cd ..;
    cd Layer_Params;
    save(paramFileName, 'network');
    cd ..;
    cd Convolution_Nets_ND;
    
    


end

