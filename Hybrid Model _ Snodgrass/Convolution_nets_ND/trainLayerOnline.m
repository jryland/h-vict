function [  ] = trainLayerOnline( inputFolder, inputInfo, networkInfo, paramFileName, numEpochs)
% I'm Sorry this is a very long function that can and should be simplified
% in the future. The step size manipulation logic could be mostly removed
% and the gradient descent should work fine with a small enough step size.
% also Tensor is consistently mispelled Tensor. When I feel like maybe
% breaking everything, I will fix that.

% I would suggest keeping the cooldownTimeGPU if your computer does not
% cool its GPU in the best manner. This will cause the process to run
% slower than nescessary, while still orders of magnitude faster than
% computing the convolutions with CPUs.

% For most people I would recomment modifying the Build function rather
% than directly manipulating this one as it is really not well written.

    %Some initialization parameters
    baseRez = networkInfo.baseRez;  % the rezolution of the base feature representations
    filtRez = networkInfo.filtRez;  % the rezolution of the filters
    numChan = networkInfo.numChan;  % the number of input channels
    numRep = networkInfo.numRep;    % the number of high level feature representations
    numOrients = networkInfo.numOrients;
    numScales = networkInfo.numScales;
    learnRate = networkInfo.startRate;
    recodeStyle = networkInfo.recodeStyle;
    sparseStyle = networkInfo.sparseStyle;
    
    
    % may remove
    rRate = networkInfo.rRate;
    oRate = networkInfo.oRate;
    stepUpTolerence = networkInfo.stepUpTolerance;
    
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    
    if strcmp(inStyle, 'vector')&&mod(numChan, 4)~=0
        disp('Error: number of channels must be a multiple of 4, to code orientation and scale as two 2D vectors.');
        disp('If input is not vector coded please change inputInfo.inStyle to grey, full, or []');
    end
    
    minLearnRate = learnRate*10^(-8);
    epochs = numEpochs; % number of times to go through the whole training set;
    
    cooldownTimeGPU = 5;
    
    
    % load cell array of multi-channel stimuli/ or composite images 
    compIm_c = loadInputFrom(inputFolder, inRez, inNum, inStyle, inHidden);
    %compIm_c = loadNoiseStimuli(25, 50, 'grey', 'circle');
    
    %if length(compIm_c)>inNum
    %    compIm_c = compIm_c(1:(end-1))
    %end
    numS = length(compIm_c);
    
    
    
    
    % Create a radial basis constraint matrix to relate a set of filters to
    % a set of base feature space
    [RBTensor, TensorShape, matShape, numChan_, numRep_] = getMultiRBFmat(networkInfo);
    
    
    % Generate a random set of base features
    baseRep = RandBase(networkInfo);
    baseRepOld = baseRep;
    
    pause(.1)
    
    compHiddenIm = [];
    compIm = [];
    compRecIm = [];
    ErrorNorm = 0;
    errorDecreasingRun = 1;
    errorIncreasingRun = 0;
    stop = 0;
    filtTensor = [];
    filtShape = [];
    filtErrorTensor_batch = zeros(filtRez, filtRez, numChan, numOrients, numScales, numRep);
    repDensity = zeros(1,1,1,1,1,numRep);
    
    resample = 0;    
    
    numNodesH = [];
    
    bestBaseRep = [];
    bestError = [];
    
    gain = 1;
    
    
    errorFig = figure();
    errorHist = [0];
    errorNum = [0];
    
    avgRespHist = 0;
    avgHiddenResponse = 0;
    sumHiddenSquares = 0;
    
    orthoHist = 0;
    orthoReg = 0;
    
    
    objHist = 0;
    oldObj = 0;
    obj = 0;
    bestObj = 0;
    errorNum = [0];
    %subplot(1,4,4);
    lineHandle4 = plot(objHist,errorNum);
    set(lineHandle4, 'YData', objHist, 'XData', errorNum);
    
    
    %similarityFig = figure();
    %sSignatures = zeros(numS, numRep);
    %sSig_raw = zeros(numS, numRep);
    %similarityMatrix = [];
    
    
    baseErrorTensor_Batch = zeros(size(baseRep));
    
    
    sigs = zeros(numS, numRep);
    
    disp(['Number of Stimuli ' num2str(numS)]);
    
    mainFigure = figure();
    
    for ep = 1:epochs
        
        
        if mod(ep,100)==0
            disp('$$$$$$$ Allowing GPU to cool 30 seconds $$$$$$');
            pause(cooldownTimeGPU);
        end
        
        % important variables to reset for correctly calculating batch
        % averages
        ErrorNormNew = 0;
        objNew = 0;
        avgHiddenResponse = 0;
        sumHiddenSquares = 0;
        repDensity = zeros(1,1,1,1,1,numRep);
        
        % this will help adjust the gain
        averageInMag = 0;
        averageRecMag = 0;
        
        
        baseErrorTensor_Batch = zeros(size(baseRep));
        filtErrorTensor_batch = zeros(filtRez, filtRez, numChan, numOrients, numScales, numRep);
    
        
        % Pick a random image from the dataset with replacement!!
        s = ceil(rand(1,1)*numS); %1
        compIm = compIm_c{s};
        
        % make it so the target is reachable
        blurIn = 1;
        if  blurIn   %~strcmp(inHidden, 'hidden') && blurIn
            compIm = convn( compIm, fspecial('gaussian', [11 11], networkInfo.spatialFreq), 'same');
            
        end
        edgeIn = 1;
        if edgeIn %~strcmp(inHidden, 'hidden') && edgeIn
            compIm = convn( compIm, fspecial( 'log'), 'same');
        end
        
        
        suppressItemOutput = 0;

        if(~suppressItemOutput)
            disp(['Epoch: ' mat2str(ep) '  Stimuli: '  mat2str(s)]);
        end
        

        % get filter stack 
        %disp('1. Getting Filters from feature Template');
        [filtTensor, filtShape] = getFilts(baseRep, RBTensor, TensorShape, matShape, numChan, numRep, inStyle);

        % get hidden image
        %disp('2. Getting Hidden Image from Input Image');
        [ compHiddenIm, compHiddenIm_raw] = getHiddenIm( compIm, filtTensor, filtShape, sparseStyle);
        numNodesH = length(compHiddenIm(:));
        avgHiddenResponse = avgHiddenResponse + mean(abs(compHiddenIm(:)/numNodesH))/numS;
        sumHiddenSquares = sumHiddenSquares+sum(compHiddenIm(:).^2)/numS;

        % get reconstructed input image
        %disp('3. Getting Reconstructed Input Image from Hidden Image');
        [ compRecIm ] = getRecIm(compHiddenIm, filtTensor, filtShape);

        % adjust the gain on the compRecIm 
        compRecIm = compRecIm*gain; %compRecIm*gain;
        
        
        % get reconstructive convolution derivative
        %disp('4. Getting Reconstructed Input Image from Hidden Image');
        [ filtErrorTensor, compImError] = getConvDeriv(compIm, compHiddenIm, compRecIm, rRate);
        filtErrorTensor_batch = filtErrorTensor_batch + filtErrorTensor; 
        
        
        
        % collecting things for later
        ErrorNormNew = ErrorNormNew + norm(compImError(:))^2;
        sDensity = sum(sum(sum(sum(sum(abs(compHiddenIm_raw),1),2),3),4),5);
        repDensity = repDensity + sDensity;
        sSignatures(s,:) = sDensity(:)/norm(sDensity(:));
        sSig_raw(s,:) = sDensity(:);
        sHiddenImMats(:,:,:,:,:,:,s) = compHiddenIm;
        sCompIms(:,:,:,s) = compIm;

        averageInMag = averageInMag + norm(compIm(:));
        averageRecMag = averageRecMag + norm(compRecIm(:))/numS;


        % get Orthoganality error 
        sigs(s,:) = sSignatures(s,:);
        [OrthoError, orthoFiltErrorTensor]  = getOrthoDeriv(sigs, s, compHiddenIm, compIm, filtShape);
        
        
        
        % get error projected into base feature space

        % apply gradient descent to base features
        %disp('5. Apply Gradiant Descent on Feature Template');
        %filtErrorTensor = zeros(filtShape);
        baseErrorTensor = baseProjection(RBTensor, matShape, filtErrorTensor  + oRate*orthoFiltErrorTensor, filtShape, inStyle);

        baseErrorTensor_Batch = baseErrorTensor/numS + baseErrorTensor_Batch; 

        if(~suppressItemOutput)
            disp(['Reconstruction Error: ' mat2str(norm(compImError(:))^2)]);
            disp(['Magnitude Im          ' mat2str(norm(compIm(:))   )]);
            disp(['Magnitude Rec         ' mat2str(norm(compRecIm(:)))]);
            disp(' ');
        end



        % display some pics :) to check what you are doing %%%%%%%%%%%
        
        set(0, 'CurrentFigure', mainFigure);

        if strcmp(inStyle, 'vector')
            subplot(3,4,1);
            [handles] = dispRecodedIm(compIm);
            subplot(3,4,2);
            [handles] = dispRecodedIm(compRecIm);
        elseif strcmp(inStyle, 'multi-chan');
            subplot(3,4,1);
            [handles] = dispRecodedIm(compIm, 'multi-chan');
            subplot(3,4,2);
            [handles] = dispRecodedIm(compRecIm, 'multi-chan');
        else
            outIm = compIm;
            inMin = min(outIm(:));
            inMax = max(outIm(:));
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(3,4,1); image(outFill); daspect([1 1 1]);

            outIm = compRecIm;
            outIm = outIm-min(outIm(:));
            outIm = outIm/max(outIm(:));
            %outIm = outIm-inMin;
            %outIm = outIm/inMax;
            outIm = max(min(outIm, 1), 0);

            nr = min(size(outIm, 3),3);
            outFill = zeros(size(outIm,1),size(outIm,2), 3);
            outFill(:,:,1:nr) = outIm(:,:,1:nr); 
            subplot(3,4,2); image(outFill); daspect([1 1 1]);
        end

        % display non-sparse recoded hidden image
        subplot(3,4,3);
        [recodedHiddenIm_raw] = recodeMultiChan(compHiddenIm_raw, filtShape);
        [handles] = dispRecodedIm(recodedHiddenIm_raw, 'multi-chan');

        % display recoded hidden image
        subplot(3,4,4);
        if strcmp(recodeStyle, 'vector')
            [recodedHiddenIm] = recodeSimple(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'vector');
        elseif strcmp(recodeStyle, 'multi-chan')
            [recodedHiddenIm] = recodeMultiChan(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
        elseif strcmp(recodeStyle, 'full_orient_chan')
            [recodedHiddenIm] = recodeFullOrientChan(compHiddenIm, filtShape);
            [handles] = dispRecodedIm(recodedHiddenIm, 'multi-chan');
        end

        % Image the error
        set(0, 'CurrentFigure', mainFigure);

        subplot(3,4,[5 6 9 10]); imagesc(filtDispImage(filtTensor, filtShape));
        daspect([1 1 1]);
        subplot(3,4,[7 8 11 12]); imagesc(filtDispImage(-(filtErrorTensor_batch), filtShape)); 
        daspect([1 1 1]);

        colormap('gray');
        pause(.01);

        % apply limit to gradient
        gradMax = max(abs(baseErrorTensor_Batch(:)));
        gradNorm = norm(baseErrorTensor_Batch(:));

        disp(['Grad Max:   ' mat2str(gradMax)]);
        gradLimit = 1;
        if (gradMax>gradLimit);
            baseErrorTensor_Batch = baseErrorTensor_Batch*gradLimit/gradNorm;
        end

        %apply gradient descent batch style % check check the sign
        baseRepNew = baseRep - learnRate * baseErrorTensor_Batch;

        baseRep = baseRepNew;
        
        % full objective function .. with sparse learning..
        % obj = (ErrorNormNew + rRate * ((numNodesH*rGoal)^2 - avgHiddenResponse^2)^2);
        objNew = ErrorNormNew + oRate * OrthoError;
        obj = objNew;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        % plot final error %%%%%%%%%%%%%%%%%%%%%%%%
        
        
        set(0, 'CurrentFigure', errorFig);
        smoothFilt = [1 1 1 1 1]/5;
        errorHist = [errorHist ErrorNorm];
        errorHist(1) = errorHist(2);
        errorNum = [errorNum ep];
        %set(lineHandle1, 'YData', errorHist, 'XData', errorNum);
        
        avgRespHist = [avgRespHist avgHiddenResponse];
        %set(lineHandle2, 'YData', avgRespHist, 'XData', errorNum);
        
        orthoHist = [orthoHist orthoReg];
        %set(lineHandle3, 'YData', orthoHist, 'XData', errorNum);
        
        objHist = [objHist obj];
        %objHist(1) = objHist(2);
        set(lineHandle4, 'YData', conv(objHist, smoothFilt, 'same'), 'XData', errorNum);
        
        
        pause(.1);


    end
    
    % final parameters
    disp(' ');
    disp('-----------------------Saveing Network Parameters-----------------------');
    disp(' ');
    network.baseRep = baseRep;
    network.RBF = RBTensor;
    network.TensorShape = TensorShape;
    network.matShape = matShape;
    network.filtShape = filtShape;
    network.recodeStyle = recodeStyle;
    network.repDensity = repDensity;
    
    cd ..;
    cd Layer_Params;
    save(paramFileName, 'network');
    cd ..;
    cd Convolution_Nets_ND;
    
    


end
