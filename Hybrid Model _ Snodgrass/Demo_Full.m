function [] = Demo_Full()

    % Cross-Validation Loop
    runs = 2
    run = 1


    try 
        load run.mat
        disp(['Continuiung on Run ' num2str(run)])
    catch
        disp('Starting A new set of Runs')
    end

    if(run == -1)
        run = 1
    end

    for i=run:runs

        [Train, Test, all] = GetFolds(runs,i);

        % Train the GCFM model
        cd Convolution_nets_ND
        DEMO_BuildOnline(Train, all)
        DEMO_BuildOnline_Color(Train, all)
        DEMO_BuildOnline_Rez_And_Color()

        cd ..

        % Create the other components for H-VICT and test in experiments
        cd Transformation_nets
        DEMO_Build(run, Test)

        cd ..

        run = i+1;
        % Keep track of what run we are on in case of early termination
        save run.mat run

        disp(['Completed Run ' num2str(i)])
        pause(3)
    end

    run = -1
    save run.mat run

    disp(['All Runs Completed'])

    disp('Compiling Results into tabular .txt format')
    CollectAll(runs)
end

