function [ Train, Test, all  ] = GetFolds( nFolds, run)

    % For the Snodgrass dataset the number of stimuli is 260,
    % Our rotated version has 260*12 stimuli, we want to grab
    % In chunks of 12 matched items.
    
    numS = 260;
    numT = 40;
    
    inter = numS/nFolds;
    a = (run-1)*inter+1;
    b = a+inter;
    
    % Get objects for each set
    inds = 1:numS;
    
    Otest  = a<=inds & inds<b;
    Otrain = ~Otest;
    
    % Reduce the number of Training items for GPU/Memory load
    %Otrain = Otrain&(rand(size(Otrain))>.75);
    
    bets = Otrain.*rand(size(Otrain));
    betRanks = sort(bets);
    splitVal = betRanks(260-numT);
    Otrain = bets>splitVal;
    
    disp(sum(Otrain));
    
    % Expend for orientations
    or = ones(1,12);
    
    all = ones(1, numS*12);
    Test  = kron(Otest,  or);
    Train = kron(Otrain, or);
    
    figure('name', ['Fold' num2str(run)]);
    subplot(2,1,1);
    imagesc(Train);
    subplot(2,1,2);
    imagesc(Test);
    pause(.5);
end

