function [  ] = ByFeature( mfIm )
%BYFEATURE Summary of this function goes here
%   Detailed explanation goes here

    fig = figure();
    
    % 2 rows of 5
    
    rows = ceil(size(mfIm,3)/5);
    
    for i = 1:size(mfIm,3)  
        subplot(rows,5, i);
        imagesc(mfIm(:,:,i));
        daspect([1 1 1]);
        colormap('gray');
    end


end

