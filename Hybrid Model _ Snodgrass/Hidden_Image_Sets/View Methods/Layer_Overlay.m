function [  ] = Layer_Overlay( Stim, filtSz )

    cd '../layer1_HiddenImages/';
    listing = dir();    
    temp = struct2cell(load(listing(Stim+2).name));
    l1 = temp{1};
    
    cd '../layer2_HiddenImages/';
    listing = dir();    
    temp = struct2cell(load(listing(Stim+2).name));
    l2 = temp{1};
    
    cd '../layer3_HiddenImages/';
    listing = dir();    
    temp = struct2cell(load(listing(Stim+2).name));
    l3 = temp{1};
    
    cd '../layer4_HiddenImages/';
    listing = dir();    
    temp = struct2cell(load(listing(Stim+2).name));
    l4 = temp{1};
    
    cd '../View Methods/';
    
    

    padImSz = size(l1,1);

    filtSz = filtSz+1;
    
    imSz = padImSz - filtSz;

    
    a = filtSz/2;
    b = a+imSz+1;
    
    % only 1 time
    l2 = l2(a:b,a:b);
    l2 = imresize(l2, [padImSz padImSz]);
    
    
    % 2 times
    l3 = l3(a:b,a:b);
    l3 = imresize(l3, [padImSz padImSz]);
    l3 = l3(a:b,a:b);
    l3 = imresize(l3, [padImSz padImSz]);
    
    % 3 times
    l4 = l4(a:b,a:b);
    l4 = imresize(l4, [padImSz padImSz]);
    l4 = l4(a:b,a:b);
    l4 = imresize(l4, [padImSz padImSz]);
    l4 = l4(a:b,a:b);
    l4 = imresize(l4, [padImSz padImSz]);
    
    
    overlay(:,:,1) = l2;
    overlay(:,:,2) = l3;
    overlay(:,:,3) = l4;
    
    overlay = (overlay/max(overlay(:))).*(overlay>0);
    
    figure();
    imagesc(overlay/max(overlay(:)));
    daspect([1 1 1]);
    
    figure();
    subplot(1,4,1); imagesc(l1); daspect([1 1 1]);
    subplot(1,4,2); imagesc(l2); daspect([1 1 1]);
    subplot(1,4,3); imagesc(l3); daspect([1 1 1]);
    subplot(1,4,4); imagesc(l4); daspect([1 1 1]);
    colormap(hot);
    
    % Later use the display recoded image function so u can use more colors!
    
    % sigs
    
    signature = [sum(l1(:)) sum(l2(:)) sum(l3(:)) sum(l4(:))];
    signature = signature/norm(signature);
    
    signature
    
end

