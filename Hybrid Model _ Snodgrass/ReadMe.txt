Typing "Demo_Short" will run though all of the experiments for this project
without retraining the base GCFM network. Typing "Demo_Full" will retrain 
the base GCFM network to replicate the entire procedure used in the paper. 
Makes sure you have ample hard-drive space before running Demo_Full as it
will store intermediary stages of processed stimuli and may consume a
gigabyte of memory. Further, a lot of pause statements were put into the
code to prevent the GPU we were using from overheating. The dev work was 
done on an imac with a poorly cooled gpu. It still snow crashes from time
to time. 

The demo requirements:
1. Matlab 2013
2. Parrallel Computing Toolbox
3. Cuda Capable Graphics card
4. Cuda Installed correctly such that matlab recongizes it

Notes:
    Other versions of matlab may work correctly, but the entire suite of
functions was written using Matlab 2013 and Matlab is not always fully
backward compatable. 
    All data collected for the experiments are stored in the
Var"Model"_exp#.txt files which are generated in the /Results directory.
Because the model has no memory of previous trials, except when told to,
the item order is squential and will match the item order in the snodgrass
dataset.
    The Graphs printed in the paper were created using R and a suite of
Utilities for making them is located in /Results/R_Chart_Scripts. Source
the scripts and then type "exp1Graph()" and "exp3Graph()" to print the
results. RStudio was used to develope these and is recommended for running
them. Also check that you have ggplot2, ExPosition, and gridExtra packages
installed.