function [ ] = Experiment1ToTxt( struct1, struct2, struct3 )
                                    %HATF  %GCNN 

    itemByOrient(:,:,1) = struct1.itemOrientPerf;
    itemByOrient(:,:,2) = struct2.itemByOrient;
    itemByOrient(:,:,3) = struct3.itemByOrient;
    
    
    algo = {'HATF', 'GCNN', 'LINN'};
    rot = {'r000', 'r030', 'r060', 'r090', 'r120', 'r150', 'r180', 'r210', 'r240', 'r270', 'r300', 'r330'};
    rotR = (0:30:360)/180*pi;
    identStrings = {'Incorrect', 'Correct'};
    
    numItems = size(itemByOrient,1);
    
    
    % ItemNum, Rot, Algo, Identified
    data = cell(numItems*12*3, 4);
    
    o = 1;
    
    for i = 1:numItems
        for r = 1:12
            for a = 1:3
                
                data{o,1} = ['Item' num2str(i)];
                data{o,2} = rot(r);
                data{o,3} = algo(a);
                data{o,4} = itemByOrient(i,r,a);
                data{o,5} = rotR(r);
                data{o,6} = identStrings{itemByOrient(i,r,a)+1};
                
                
                o = o + 1;
            end
        
        end
    
    end
    
    table = cell2table(data,...
            'VariableNames', {'ItemID', 'Orient', 'Algorithm', 'Identified', 'Rad', 'identString'})
    
    writetable(table, 'All_Performance_table', 'Delimiter', ',');
    
    
end