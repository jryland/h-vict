function [ ] = Experiment1_Graph( struct1, struct2, struct3 )
                                    %HATF  %SIG   #IMAGE
    yDataSets{1} = struct1.itemOrientPerf;
    yDataSets{2} = struct2.itemByOrient;
    yDataSets{3} = struct3.itemByOrient;
    
    orients = struct1.orients(1:(end-1));
    
    colors = {'red', 'black', 'blue'};
    
    markers = {'^','s','d'};
    
    %linestyle = {'-^','-s','-d' };
    
    fig = figure();
    ax = axes();
    
    title({'Classification Accuracy: Interaction', 'between Algorithm and Orientation'},'FontSize',20);
    
    set(get(ax,'xlabel') , 'string', 'Orientation of Stimulus in Degrees', 'FontSize', 16, 'FontWeight', 'normal');
    set(get(ax,'ylabel') , 'string', 'Probablility of Correct Identification', 'FontSize', 16, 'FontWeight', 'demi');
    set(ax,'xtick', 0:30:360);
    
    % XtickLabel this is used to specify a vector of string to replace the
    % xTicks
    for i=1:3

        yData = yDataSets{i};

        numTrials = size(yData, 1);

        yData = sum(yData,1)/numTrials;


        hold on;

        %bar(orients , yData);
        

        eBars = errorbar(orients , yData, (numTrials*yData.*(1-yData)).^.5 / numTrials * 1.65);

        hold off;

        set(eBars, 'color', colors{i}, 'LineWidth', 1.5); 
        set(eBars, 'marker', markers{i}, 'markeredgecolor', colors{i}, 'markersize', 10); 
    end
    
    set(gca,'ylim', [0 1], 'xlim', [-10 340] );
        
    legend('TRANSFORM', 'SIGNATURE', 'IMAGE');
    
    savefig(fig, 'Experiment1');
    
end

