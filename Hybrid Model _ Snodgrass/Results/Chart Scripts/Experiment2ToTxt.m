function [  ] = Experiment2ToTxt( struct1 )
                                    %HATF

    stepsOrient(:,:) = struct1.itemOrientSteps2;
    perfOrient(:,:) = struct1.itemOrientPerf;

    
    
    algo = {'HATF', 'GCNN', 'LINN'};
    rot = {'r000', 'r030', 'r060', 'r090', 'r120', 'r150', 'r180', 'r210', 'r240', 'r270', 'r300', 'r330'};
    rotR = (0:30:360)/180*pi;
    
    numItems = size(stepsOrient,1);
    
    
    % ItemNum, Rot, Algo, Identified
    data = cell(30*12, 4);
    
    o = 1;
    
    
%     for i = 1:numItems
%         for r = 1:12
%                 
%             if perfOrient(i,r)==1 && o<=(40*12)
%                 
%                 data{o,1} = ['Item' mat2str(i)];
%                 data{o,2} = rot(r);
%                 data{o,3} = stepsOrient(i,r);
%                 data{o,4} = rotR(r);
% 
%                 o = o + 1;
%             end
%         end
%     
%     end
    
    o = 1;

    for r = 1:12
        
        d = 1;
        
        for i = 1:numItems
        
            if perfOrient(i,r)==1 && d<=(30);
                
                data{o,1} = ['Item' mat2str(i)];
                data{o,2} = rot(r);
                data{o,3} = stepsOrient(i,r);
                data{o,4} = rotR(r);
                d = d + 1;
                o = o + 1;
            end
        end
    
    end               
    
    
    table = cell2table(data,...
            'VariableNames', {'ItemID', 'Orient', 'Steps', 'Rad'})
    
    writetable(table, 'HATF_ReactionTimeV2', 'Delimiter', ',');
    
    
end