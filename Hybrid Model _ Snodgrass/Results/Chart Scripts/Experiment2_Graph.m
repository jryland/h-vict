function [ ] = Experiment2_Graph( struct1 )
                                    %HATF
    stepsOrient(:,:) = struct1.itemOrientSteps2;
    perfOrient(:,:) = struct1.itemOrientPerf; 
    
    
    data = zeros(30, 12);
    
    numItems = size(perfOrient, 1);
    
    
    for r = 1:12
        
        o = 1;
        
        for i = 1:numItems
        
            if perfOrient(i,r)==1 && o<=(30);
                
                data(o,r) = stepsOrient(i,r);

                o = o + 1;
            end
        end
    
    end               
    
    
  
                                    
    figB = figure();
    imagesc(data);
                                    
                                    
                                    
    yDataSets{1} = data;
 
    orients = struct1.orients(1:(end-1));
    
    colors = {'red', 'black', 'blue'};
    
    markers = {'^','s','d'};
    
    linestyle = {'-^','-s','-d' };
    
    fig = figure();
    ax = axes();
    
    title({'Reaction Time and Orientation'},'FontSize',20);
    
    set(get(ax,'xlabel') , 'string', 'Orientation of Stimulus in Degrees', 'FontSize', 16, 'FontWeight', 'normal');
    set(get(ax,'ylabel') , 'string', 'Number of Steps needed To Identify', 'FontSize', 16, 'FontWeight', 'demi');
    set(ax,'xtick', 0:30:360);
    
    % XtickLabel this is used to specify a vector of string to replace the
    % xTicks

    yData = yDataSets{1};

    numTrials = size(yData, 1);

    yData = sum(yData,1)/numTrials;


    hold on;

    %plot(orients , yData,  linestyle{1}, 'LineWidth', 2);

    std(yDataSets{1}(:,:), 1, 1)
    
    eBars = errorbar(orients , yData, std(yDataSets{1}, 1, 1)/(40^.5)*2.69);

    hold off;

    %set(eBars, 'color', colors{i}, 'LineWidth', 1.5); 
    %set(eBars, 'marker', markers{i}, 'markeredgecolor', colors{i}, 'markersize', 10); 
    
    
    set(gca,'ylim', [0 30], 'xlim', [-10 340] );
        
    legend('TRANSFORM', 'SIGNATURE', 'IMAGE');
    
    savefig(fig, 'Experiment1');
    
end
