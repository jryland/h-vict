function [  ] = Experiment3ToTxt( struct )
%EXPERIMENT3TOTXT Summary of this function goes here
%   Detailed explanation goes here
    probePerfs = struct.probePerfs;
    pairInds = struct.pairInds;

    numStim = size(probePerfs,1);
    
    % ID, Prime, Probe, Identified 
    data = cell(numStim*4, 4);
    
    
    prime = {'up','up', 'mis', 'mis'};
    probe = {'con','in', 'in','con'};
    
    o = 1;
    
    for i = 1:numStim
        
        for c = 1:4
            
            data{o,1} = ['ItemID' num2str(i)];
            data{o,2} = prime{c};
            data{o,3} = probe{c};
            data{o,4} = probePerfs(i,c);
        
            
            o = o+1;
        end
        
    end
    
    table = cell2table(data,...
            'VariableNames', {'ItemID', 'PrimeOr', 'ProbeOr', 'Identified'})
    
    writetable(table, 'HATF_Congruency_table', 'Delimiter', ',');
end

