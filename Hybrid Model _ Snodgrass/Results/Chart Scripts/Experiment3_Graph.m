function [ bData, eBars ] = Experiment3_Graph( struct )

    fig = figure()
    
    ax = axes();
    
    
    
    
    struct.probePerfs;

    numTrials = size(struct.probePerfs, 1) - 1;
    
    yData = sum(struct.probePerfs,1)/numTrials;
    
    barGraph = bar( yData);
    
    set(barGraph, 'FaceColor', [.5 .5 .5]); 
    
    hold on;
    
    eBars = errorbar(yData, (yData.*(1-yData)/numTrials).^.5  * 1.65);
    
    hold off;
    
    
    set(eBars, 'linestyle', 'none', 'color', 'black' ); 
    
    
    
    xTickLabels = {'Prime 0 : Probe 0', 'Prime 0 : Probe 90', 'Prime 90 : Probe 0', 'Prime 90 : Probe 90'};
    
    title({'Classification Accuracy: Orientation', 'Congruency Effects for HATF'},'FontSize',20);
    
    set(get(ax,'xlabel') , 'string', 'Orientation of Stimuli in Degrees', 'FontSize', 16, 'FontWeight', 'normal');
    set(get(ax,'ylabel') , 'string', 'Probablility of Correct Identification', 'FontSize', 16, 'FontWeight', 'demi');
    
    set(ax, 'xticklabel', xTickLabels);
    
    %set(ax, 'fontsize', 14);
    
    set(gca,'ylim', [0 1]);
    

    
    savefig(fig, 'Experiment3');
    
    
end

