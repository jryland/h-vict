function [ ] = Experiment4ToTxt( struct )

    
    % Process into csv observation form
    
    probePerfs = struct.probePerfs;
    
    data = cell(length(probePerfs(:)),3);
    
    
    o = 1;
    
    for i = 1:size(probePerfs,1)
        for j = 1:size(probePerfs,2);
            
            data{o,1} = ['ItemID'  num2str(i)];
            
            if j == 1
                data{o,2} = 't';
            else
                data{o,2} = 'a';
            end
            
            data{o,3} = probePerfs(i,j);
                
            
            o = o +1;
        end
    end

    %csvwrite('HATF_Rotation', dataTable);
    %save('HATF_Rotation', 'dataTable', '-ascii');
    
    table = cell2table(data,...
            'VariableNames', {'ItemID', 'Rotation', 'Identified'})
    
    writetable(table, 'HATF_Rotation_table', 'Delimiter', ',');
    
end

