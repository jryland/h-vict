function [ bData, eBars ] = Experiment4_Graph( struct )

    fig = figure
    
    ax = axes();
    
    
    struct.probePerfs;

    numTrials = size(struct.probePerfs, 1) - 1;
    
    yData = sum(struct.probePerfs,1)/numTrials;
    
    %xData = {'Prime 0 Probe 0', 'Prime 0 Probe 90', 'Prime 90 Probe 90', 'Prime 0 Probe 90'};
    
    barGraph = bar( yData);
    
    set(barGraph, 'FaceColor', [.5 .5 .5]); 
    
    hold on;
    
    eBars = errorbar(yData, (yData.*(1-yData)/numTrials).^.5  * 1.65);
    
    hold off;
    
    set(eBars, 'linestyle', 'none', 'color', 'black' ); 
    
    set(gca,'ylim', [0 1])

    
    xTickLabels = {'Towards', 'Away'};
    
    title({'Classification Accuracy: Rotary Motion', 'Effects for HATF'},'FontSize',20);
    
    set(get(ax,'xlabel') , 'string', 'Rotation w/r to Upright', 'FontSize', 16, 'FontWeight', 'normal');
    set(get(ax,'ylabel') , 'string', 'Probablility of Correct Identification', 'FontSize', 16, 'FontWeight', 'demi');
    
    set(ax, 'fontsize', 16);
    
    set(ax, 'xticklabel', xTickLabels);

    
    savefig(fig, 'Experiment4');
    
end

