function [ a_chan ] = A_fromRt( AutoMat, r_chan )
% takes an r and gets a reconstruction from an auto-associative network
% specified by the matrix AutoMat. This should help clean up competition in
% the transformation fitting.

    % change the shape of r
    numChan = size(r_chan,2);
    
    r = convertProto(r_chan, numChan, 'chan-to-vec');
    
    r = gpuArray(r);
    AutoMat = gpuArray(AutoMat);
    
    a = gather(AutoMat*r);
    
    
    a_chan = convertProto(a, numChan, 'vec-to-chan');



end

