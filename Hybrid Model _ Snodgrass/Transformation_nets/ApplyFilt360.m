function [ fPoolImg fOrImg] = ApplyFilt360( img, filt360 )
%APPLYFILT360 Summary of this function goes here
%   Detailed explanation goes here

    f360imgFull = convn(img, filt360, 'full'); % full is important here
    
    [sx sy] = size(img);
    [fx fy dummy] = size(filt360);
    
    ax = ceil(fx/2);
    bx = ax+sx-1;
    ay = ceil(fy/2);
    by = ay+sy-1;
    
    %Cropped Version should be filter ceil(size/2)
    f360img = f360imgFull(ax:bx,ay:by,:);
    
    
    %imagesc(f360img);
    %pause()
    
    %for i = 1:(size(f360img, 3))
        
    %    subplot(1,2,1); imagesc(filt360(:,:,i));
    %    subplot(1,2,2); imagesc(f360img(:,:,i));
    %    pause(.1);
    %end
    
    fPoolImg = max(f360img,[], 3);
    %fPoolImg = sum(f360img, 3);
    fPoolImg = squeeze(fPoolImg);
    
    %fPoolImg = double(fPoolImg>0);

    fPoolImg = fPoolImg.*(fPoolImg>.0);

    
    %orientations image still working on
    fOrImg = [];
    %[s1 s2 s3] = size(f360img);
    %[X Y Z] = meshgrid(1:s2, 1:s1, 1:s3);
    %fOrImg = sum(f360img.*Z,3)./sum(f360img,3);
    %fOrImg(isnan(fOrImg)) = 0;
    %figure();
    %subplot(1,2,1); imagesc(fPoolImg); daspect([1 1 1]);
    %subplot(1,2,2); imagesc(fOrImg);   daspect([1 1 1]);
    
end