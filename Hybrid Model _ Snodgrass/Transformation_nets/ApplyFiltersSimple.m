function [ multi_f_img ] = ApplyFiltersSimple( img, filters360 )

    [imgSx, imgSy] = size(img);

    numFilters = length(filters360);
    
    multi_f_img = zeros(imgSx, imgSy, length(filters360));
    
    for i=1:numFilters
        
        %size(ApplyFilt360(img, filters360{i}))
        %size(multi_f_img(:,:,i))
        
        multi_f_img(:,:,i) = ApplyFilt360(img, filters360{i});
    
    end
    
    

end

