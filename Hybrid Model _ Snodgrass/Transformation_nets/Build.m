function [] = Build( )


    inputInfo.inRez = 9;                    % the rezolution to scale the stimuli to be
    inputInfo.inNum = 150*12; %150 *12;     % the number of stimuli to be used
    inputInfo.inStyle = 'multi-chan';       % the style of the input images
    inputInfo.inSample = 'full';            % how the images are sampled
    inputInfo.inHidden = 'hidden';          % original or hidden, tells program where to look for the images
    inputInfo.numOrients = 12;
    
    
    %inputFolderName = 'layer2_HiddenImages';
    %inputFolderName = 'layer2_ConjunctionImages';
    
    %inputFolderName = 'layer2_HiddenImages';
    %inputFolderName = 'layer2Col_HiddenImages';
    inputFolderName = 'layer1_Rez_Col_HiddenImages';
    
    
    variantInfo.saveAs = '';
    variantInfo.varTitle = '';
    

    % Experiment 1, 2, and 3
    
    % VARIANT V ___________________________________________________________
    HVICT_performance = [];
    variantInfo.saveAs = 'Var1V_';
    variantInfo.varTitle = 'Variant -V-';
    variantInfo.transformFreeze = 1;
    variantInfo.invariantOnly = 0;
    variantInfo.centerOnly = 1;
    
    tNetInfo.GCNN_influence = 0; %.1  %.1; %.1; %.2; %;.1; % .3
    epochLim =  1; %30;% 30; %50; % 3
    HVICT_performance = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim);
   
    
    variantInfo.transformFreeze = 0;
    tNetInfo.GCNN_influence = 0;
    epochLim = 30;
    epochLim2 = 5;
    
    % Possible primes (0 & 90)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        primes60_150 = [ 13    14    33    41    69    77    80    93   109   124   127   150];
    else
        primes60_150 = find(HVICT_performance.itemOrientPerf(:,1)&HVICT_performance.itemOrientPerf(:,4))';
    end
    
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [1 4]);
    
    if (isempty(HVICT_performance))
        
        primes60_150 = [ 14    18    33    41    93   109   119];
         
    else
        primes60_150 = find(  HVICT_performance.itemOrientSteps(:,3)>-1  & HVICT_performance.itemOrientSteps(:,6)>-1  ... % had to comprimise 
                            & HVICT_performance.itemOrientSteps(:,3)<25 & HVICT_performance.itemOrientSteps(:,6)<25 ...
                            & HVICT_performance.itemOrientPerf(:,3)... 
                            & HVICT_performance.itemOrientPerf(:,6))';
    end
    CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [3 6]);
    
    tNetInfo.GCNN_influence = 0;
    epochLim = 1;
    %RotationExp(inputFolderName, inputInfo, tNetInfo, 150, epochLim);
    %______________________________________________________________________
    
    
    
    
    
    
    % VARIANT I ___________________________________________________________
    
    % THIS ONE NEEDS SOME FIXING.. Need a param that freezes the transform
    % Otherwise this one becomes VI
    HVICT_performance = [];
    variantInfo.saveAs = 'Var2I_';
    variantInfo.varTitle = 'Variant -I-';
    variantInfo.transformFreeze = 1;
    variantInfo.invariantOnly = 1;
    variantInfo.centerOnly = 1;
    
    tNetInfo.GCNN_influence = 1; %.1  %.1; %.1; %.2; %;.1; % .3
    epochLim =  1; %30;% 30; %50; % 3
    HVICT_performance = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim);
    
    % DO NOT RUN THIS ONE
    %tNetInfo.GCNN_influence = 1;
    %epochLim = 30;
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, 1);
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, 2);
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, 3);
    
    
    tNetInfo.GCNN_influence = 1;
    epochLim = 10;
    %RotationExp(inputFolderName, inputInfo, tNetInfo, 150, epochLim);
    %______________________________________________________________________
    
    
    
    
    
    
    % VARIANT VI __________________________________________________________
    HVICT_performance = [];
    variantInfo.saveAs = 'Var3VI_';
    variantInfo.varTitle = 'Variant -VI-';
    variantInfo.transformFreeze = 1;
    variantInfo.invariantOnly = 0;
    variantInfo.centerOnly = 1;
    
    tNetInfo.GCNN_influence = .3; %.1  %.1; %.1; %.2; %;.1; % .3
    epochLim =  1; %30;% 30; %50; % 3
    HVICT_performance = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim);
    
    
    variantInfo.transformFreeze = 0;
    tNetInfo.GCNN_influence = .3;
    epochLim = 30;
    epochLim2 = 5;
    
    % Possible primes (0 & 90)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        primes60_150 = [ 1     4     5     6     8     9    10    13    14    15    17    18    20    21    23    24    25    26    27    29    30 31    33    34    35    36 40    41    42    43    46    49    57    60    62    63    64    65    68    69    70    71 72    73    76    77    80    81    83    84    86    87    88    90    91    93    95    96    98    99   100   102   105  106   107   108   109   113   115   116   118   119   121   122   123   124   125   127   131   133   135   136   137   139 143   144   145   146   148   149   150];
    else
        primes60_150 = find(HVICT_performance.itemOrientPerf(:,1)&HVICT_performance.itemOrientPerf(:,4))';
    end
    
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [1 4]);
    
    % Possible primes (60 & 150)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        
        primes60_150 = [ 1     4     5     6     8     9    10    14    15    17    18    20    21    24    25    26    27    29    33    34    35,...
                         36    40    41    42    43    47    49    57    63    64    65    69    70    71    72    76    77    80    83    84    86,...
                         87    90    91    93    95    96    98    99   100   101   105   106   108   109   113   115   116   118   119   121   122,...
                         124   127   131   133   135   136   137   139   143   144   146   150];
         
    else
        primes60_150 = find(  HVICT_performance.itemOrientSteps(:,3)>-1  & HVICT_performance.itemOrientSteps(:,6)>-1  ... % had to comprimise 
                            & HVICT_performance.itemOrientSteps(:,3)<25 & HVICT_performance.itemOrientSteps(:,6)<25 ...
                            & HVICT_performance.itemOrientPerf(:,3)... 
                            & HVICT_performance.itemOrientPerf(:,6))';
    end
    CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [3 6]);
    
    tNetInfo.GCNN_influence = .3;
    epochLim = 10;
    %RotationExp(inputFolderName, inputInfo, tNetInfo, 150, epochLim);
    %______________________________________________________________________
    
    
    
    
    
    
    % VARINAT VT __________________________________________________________
    HVICT_performance = [];
    variantInfo.saveAs = 'Var4VT_';
    variantInfo.varTitle = 'Variant -VT-';
    variantInfo.transformFreeze = 0;
    variantInfo.invariantOnly = 0;
    variantInfo.centerOnly = 0;
    
    tNetInfo.GCNN_influence = 0; %.1  %.1; %.1; %.2; %;.1; % .3
    epochLim =  30; %30;% 30; %50; % 3
    HVICT_performance = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim);
    
    
    
    tNetInfo.GCNN_influence = 0;
    epochLim = 30;
    epochLim2 = 5;
    
    % Possible primes (0 & 90)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        primes60_150 = [13    14    24    33    41    69    77    93   101   109   116   122   124   127   131   140   150];
    else
        primes60_150 = find(HVICT_performance.itemOrientPerf(:,1)&HVICT_performance.itemOrientPerf(:,4))';
    end
    
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes0_90, [1 4]);
    
    % Possible primes (60 & 150)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        
        primes60_150 = [ 45    93   131];
         
    else
        primes60_150 = find(  HVICT_performance.itemOrientSteps(:,3)>3  & HVICT_performance.itemOrientSteps(:,6)>3  ... % had to comprimise 
                            & HVICT_performance.itemOrientSteps(:,3)<25 & HVICT_performance.itemOrientSteps(:,6)<25 ...
                            & HVICT_performance.itemOrientPerf(:,3)... 
                            & HVICT_performance.itemOrientPerf(:,6))';
    end
    CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [3 6]);
    
    
    tNetInfo.GCNN_influence = .0;
    epochLim = 10;
    %RotationExp(inputFolderName, inputInfo, tNetInfo, 150, epochLim);
    %______________________________________________________________________
    
    
    
    
    
    
    
    % VARIANT VIT _________________________________________________________
    HVICT_performance = [];
    variantInfo.saveAs = 'Var5VIT_';
    variantInfo.varTitle = 'Variant -VIT-';
    variantInfo.transformFreeze = 0;
    variantInfo.invariantOnly = 0;
    variantInfo.centerOnly = 0;
    
    tNetInfo.GCNN_influence = .3; %.1  %.1; %.1; %.2; %;.1; % .3
    epochLim =  30; %30;% 30; %50; % 3
    HVICT_performance = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim);
    
    
    
    tNetInfo.GCNN_influence = .3;
    epochLim = 30;
    epochLim2 = 5;
    
    % Possible primes (0 & 90)
    primes0_90 = [];
    if (isempty(HVICT_performance))
        primes0_90 = [1     3     4     5     6     7     8     9    10    12    13    14    15    16    17    18    20    21    22    23    24 25    26    27    28    29    30    31    33    34    35    36    39    40    41    42    43    45    46    47    48    49 50    51    52    57    58    60    61    62    63    64    65    67    68    69    70    71    72    75    76    77    79 80    81    83    84    85    86    87    88    89    90    91    93    94    95    96    97    98    99   100   101   102 105   106   107   108   109   110   112   113   114   115   116   117   118   119   120   121   122   123   124   125   127 129   131   133   135   136   137   139   140   141   143   144   145   146   147   148   149   150];
    else
        primes0_90 = find(HVICT_performance.itemOrientPerf(:,1)&HVICT_performance.itemOrientPerf(:,4))';
    end
    
    %CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes0_90, [1 4]);
    
    % Possible primes (60 & 150)
    primes60_150 = [];
    if (isempty(HVICT_performance))
        
        primes60_150 = [ 62    93   131   135];
         
    else
        primes60_150 = find(  HVICT_performance.itemOrientSteps(:,3)>5  & HVICT_performance.itemOrientSteps(:,6)>5  ... 
                            & HVICT_performance.itemOrientSteps(:,3)<25 & HVICT_performance.itemOrientSteps(:,6)<25 ...
                            & HVICT_performance.itemOrientPerf(:,3)... 
                            & HVICT_performance.itemOrientPerf(:,6))';
    end
    CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, 100, epochLim, epochLim2, primes60_150, [3 6]);
    
    
    
    tNetInfo.GCNN_influence = .3;
    epochLim = 10;
    %RotationExp(inputFolderName, inputInfo, tNetInfo, 150, epochLim);
    %______________________________________________________________________
    
    
    
    
    
    
    
    % Find effective Primes for experiment 3
    tNetInfo.GCNN_influence = .3;
    epochLim = 30;
    %TransformationSearchSingle(inputFolderName, inputInfo, tNetInfo, epochLim, 31, 23);
    %TransformationSearchSingle(inputFolderName, inputInfo, tNetInfo, epochLim, 31, 4);


    % FOUND at 0 and 90 degrees
    % Items: 1, 3, 5, 8, 10,
    % Items: 21, 22, 23?, 26, 27, 29?
    
end

