function [ successRate] = CongruencyExp(variantInfo, inputFolderName, inputInfo, tNetInfo, numPairs, epochLim, epochLim2, primes, primeOrients)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here

    span = variantInfo.span;
    Dist = variantInfo.dist;

    %Graphics
    scoreFigure = figure();
    imageFigure = figure();
    
    
    
    scoreListBox = uicontrol('Parent', scoreFigure, 'style', 'listbox', 'Units', 'normalized',...
                    'Position', [0.1 0.1 .8 .8]);
    
    scoreCell = {};       
    
    disp('Creating learned prototype matrix: Please Wait');
    
    
    % load a set of base or hidden representations
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    weightFilters =  'none'; % 'inverse-frequency'; %'inverse-frequency';
    
    compIm_c = loadInputFrom(inputFolderName, inRez, inNum, inStyle, inHidden);
    %compIm_c = compIm_c(1,1:inNum);
    numS = length(compIm_c)
    numChan = size(compIm_c{1}, 3);
    
    numProto = numS/inOrients;
    numNodes = inRez.^2;
    
    
    %remove negative values (don't know why they are there to begin with)
    for i = 1:length(compIm_c)
        compIm_c{i} = compIm_c{i}.*(compIm_c{i}>0);
    end
    
    % Get prototypes
    protoIm_c = cell(1,1);
    protoIm_c_p30 = cell(1,1);
    protoIm_c_n30 = cell(1,1);
    protoIm_c_dist = cell(1,1);
    protoIm_ID = [];
    pInd = 1;
    pInd3 = 1;
    pIndD = 1;
    for i = 1:inOrients:length(compIm_c)
        
        % Setup Single and flanking
        protoIm_c{pInd} = compIm_c{i};
        protoIm_c_p30{pInd} = compIm_c{i+1};
        protoIm_c_n30{pInd} = compIm_c{i+11};
        pInd = pInd + 1;
        
        % Setup Span option
        protoIm_c_span{pInd3} = compIm_c{i};    pInd3 = pInd3+1;
        protoIm_c_span{pInd3} = compIm_c{i+1};  pInd3 = pInd3+1;
        protoIm_c_span{pInd3} = compIm_c{i+11}; pInd3 = pInd3+1;
        
        % Setup Dist option
        for j = 0:11
            if rand(1,1)<Dist(j+1)
                pIndD
                protoIm_c_dist{pIndD} = compIm_c{i+j};
                protoIm_ID(pIndD) = pInd-1;
                pIndD = pIndD+1;
            end
        end
        
    end
    
    % make ProtoTypes matrix and get the filterWeighting 0-Deg
    [FilteredPrototypes0, filterWeighting0, protoWeighting0] = MakeFilteredPrototypes(protoIm_c, weightFilters);
    
    % Choose set of prototypes to send to the v and t models
    FilteredPrototypes = []; filterWeighting = []; protoWeighting = [];
    if strcmp(span, 'single')
        FilteredPrototypes = FilteredPrototypes0;
        filterWeighting = filterWeighting0;
        protoWeighting = protoWeighting0;
        numProto2 = numProto;
    elseif strcmp(span, 'span')
        
        [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c_span, weightFilters);
        numProto2 = numProto*3;
    
    elseif strcmp(span, 'dist')
        
        [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c_dist, weightFilters);
        numProto2 = length(protoIm_c_dist);
        variantInfo.protoIm_ID = protoIm_ID;
    end
    
    % ProtoTypes mat for +30 and -30 deg
    [FilteredPrototypesP30, filterWeightingP30, protoWeightingP30] = MakeFilteredPrototypes(protoIm_c_p30, weightFilters);
    [FilteredPrototypesN30, filterWeightingN30, protoWeightingN30] = MakeFilteredPrototypes(protoIm_c_n30, weightFilters);
    
    
    % make Auto-Associator matrix A
    PrototypesMat = convertProto(permute(FilteredPrototypes, [2 1 3]), numChan, 'chan-to-mat'); %organize to columns
    [A] = LinSolveAuto(PrototypesMat);
    
    %make a simple GCNN Signiture classifier trained upon stimuli
    FilteredPrototypesFlanking = (FilteredPrototypes0+FilteredPrototypesP30+FilteredPrototypesN30)/3;
    signatures = squeeze(sum(FilteredPrototypesFlanking, 2))';
    size( eye(numProto))
    
    [C_gcnn] = LinSolveClassifier(signatures, eye(numProto));
    
    %Make simple GCNN Image Classifier
    PrototypeVecs = reshape(FilteredPrototypes, numProto2, numChan * numNodes);
    [C_lin] = LinSolveClassifier(PrototypeVecs' , eye(numProto2));
    
    
    
    successes = 0;
    successesRate = 0;
    
    %Binned collection vectors for experiment
    % Pair by condition
    primePerf = zeros(numPairs,2); % was the prime identified
    probePerfs = zeros(numPairs, 4); % were the probes identified
    pairInds = zeros(numPairs, 2); % image numbers
    numScores = 0;
    
    
    barFigure = figure();
    barAxes = axes();
    barGraph = bar([0 0 0 0]);
    hold on;
    eBars = errorbar([0 0 0 0], [0 0 0 0]);
    set(eBars, 'linestyle', 'none', 'color', 'black' ); 
    hold off;
    set(gca, 'Ylim', [0 1]);
    
    pause(5);
    
    disp('Please Press Enter to start the simulations');
    
    s = 1;
    
    % ORIENTATIONS TO BE USED!!
    A =  primeOrients(1); 
    B =  primeOrients(2); 
    
    aDir = '0';
    bDir = '90';
    if(A == 3 && B == 6)
        aDir = '60';
        bDir = '150';
    end
    
    
    
    maskRate = tNetInfo.GCNN_influence; 
    
    
    %pause();
    
    for i = 1:numPairs
        
        primeNum = primes(floor(rand(1,1)*(length(primes))+1));
        
        primeNum
        
        % PROBE AT 60
        
        for o = 1:2
        
            %%%%%%%%%%%%%%% PRIME MATCH and  get yPARAMS
            
            % USING STIMULI 1 as the prime
            if o ==1
                orientP = (primeNum-1)*12+A;  % (i-1) * 12 + A;
                pDir = aDir;
            else
                orientP = (primeNum-1)*12+B; % (i-1) * 12 + B;
                pDir = bDir;
            end

            
            % Rand Probe
            randProbe = (floor(rand(1,1)*numProto)+1);
            probeNum =  i;  %randProbe; % or i
            
            % Skip priming the prime with itself
            if ceil(orientP/inOrients)==ceil(probeNum/inOrients)
                break;
            end
            
            matchPrototype = compIm_c{ orientP};

            numStim = ceil(orientP/inOrients);
            

            % Adjust the firing rates based on the convolutional classification
            % this is a type of fusion.
            signature = squeeze(sum(sum(matchPrototype, 1),2));
            size(signature);
            [r, order] = SimpleClassify(C_gcnn, signature);
            mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
            %mask = [];
            
            [numGuess, steps, yParams] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim, [], [], variantInfo);


            disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

            if numGuess == numStim
                scorePrime = 'Correct   : ';
                primePerf(i,o) = 1;
            else
                scorePrime = 'Incorrect : ';
            end


            %%%%%%%%%%%%%%% PROBE MATCH ORIENTATION 1
            orientA = (probeNum-1) * 12 + A;

            matchPrototype = compIm_c{ orientA};

            numStim = ceil(orientA/inOrients);

            % Adjust the firing rates based on the convolutional classification
            % this is a type of fusion.
            signature = squeeze(sum(sum(matchPrototype, 1),2));
            size(signature);
            [r, order] = SimpleClassify(C_gcnn, signature);
            mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
            %mask = [];

            [numGuess, steps, ~] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim2, yParams, [], variantInfo);


            disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

            if numGuess == numStim
                scoreProbe1 = 'Correct   : ';
                probePerfs(i,1+(o-1)*2) = 1;
            else
                scoreProbe1 = 'Incorrect : ';
            end


            %%%%%%%%%%%%%%% PROBE MATCH ORIENTATION 2
            orientB = (probeNum-1) * 12 + B;

            matchPrototype = compIm_c{ orientB};

            numStim = ceil(orientB/inOrients);

            % Adjust the firing rates based on the convolutional classification
            % this is a type of fusion.
            signature = squeeze(sum(sum(matchPrototype, 1),2));
            size(signature);
            [r, order] = SimpleClassify(C_gcnn, signature);
            mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
            %mask = [];

            [numGuess, steps, ~] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim2, yParams, [], variantInfo);


            disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

            if numGuess == numStim
                scoreProbe2 = 'Correct   : ';
                probePerfs(i,2+(o-1)*2) = 1;
            else
                scoreProbe2 = 'Incorrect : ';
            end

            scoreCell{s} = ['Prime ' pDir  scorePrime aDir scoreProbe1 bDir scoreProbe2];
            set(scoreListBox, 'String', scoreCell);
            set(scoreListBox, 'Value', s);
            s = s +1;

            % DISPLAY 
        
            barPrime60 = [sum(probePerfs(:,1).*primePerf(:,1)) sum(probePerfs(:,2).*primePerf(:,1)) ]/sum(primePerf(:,1));
            barPrime120 = [sum(probePerfs(:,3).*primePerf(:,2)) sum(probePerfs(:,4).*primePerf(:,2)) ]/sum(primePerf(:,2));

            %set(barGraph, 'YData',[barPrime60 barPrime120]);
            
            barPrimes = [barPrime60 barPrime120];
            
            errBars = (barPrimes.*(1-barPrimes)/i).^.5  * 1.65;
            
            barGraph = bar('parent', barAxes, barPrimes);
            hold on
            eBars = errorbar( barPrimes, errBars, 'parent', barAxes);
            hold off
            set(eBars,'parent',barAxes, 'linestyle', 'none', 'color', 'black' ); 
            
            disp(['Relative Performances || ' mat2str([barPrime60 barPrime120])]);
            disp(['Error Bars            || ' mat2str(errBars)])
            
        end
        
        
    end
    
    HVICT_congruency.primePerf = primePerf; 
    HVICT_congruency.probePerfs = probePerfs;
    HVICT_congruency.pairInds = pairInds;
    HVICT_congruency.orientA = aDir;
    HVICT_congruency.orientB = bDir;
    
    
    addpath(pwd)
    oldFolder = cd('..');
    cd Runs;
    
    MakeExp3R(HVICT_congruency, variantInfo.saveAs, variantInfo.run);
    
    dateStr = datestr(date,'mmm_dd_yy');
    fullTime = clock;
    %save([ 'Run' num2str(variantInfo.run) variantInfo.saveAs 'Congruency' '.mat'], 'HVICT_congruency');
    %savefig(barFigure ,['Run' num2str(variantInfo.run) variantInfo.saveAs 'Congruency' '.fig']);
    cd ..;
    cd(oldFolder);


end


