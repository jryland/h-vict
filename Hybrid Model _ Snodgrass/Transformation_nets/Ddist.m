function [dBeta_dy1 dBeta_dy2 ] = Ddist( y1, y2, Bvec, a, Nvec)
%DDIST Summary of this function goes here


%   this function needs to be paired down mathematically right now it is
%   terribly ineficient!

%   called scriptB or beta
%   B is the prefered Distence
%   y1 is centers in dim 1
%   y2 is centers in dim 2%


    %disp('Finding Neighbor Derivative');

    sz = length(y1);
    
    osz = sparse(ones(sz,1));
    Isz = sparse(eye(sz));
    
    
    %figures out the destences and compares them to the prefered distance
    D1vec = sparse((kron(y1,osz) - kron(osz,y1)) ); %.*Nvec);
    D2vec = sparse((kron(y2,osz) - kron(osz,y2)) ); %.*Nvec);
    
    %size(D1vec)
    %size(Bvec)
    
    Dvec = 1/(sz^2) * ( D1vec.^2+D2vec.^2 - a*Bvec); %.*Nvec;
    
    
    %Calculate the gradient
    dBeta_dDvec = sparse( 2*(Dvec)' );
    
    %d w/t respect to dim1 centers
    dDvec_dD1vec = sparse( 2*diag(D1vec) );    
    dD1vec_dy1 = kron(Isz, osz) - kron(osz, Isz); %these are also really big
    
    %d w/t respect to dim2 centeres
    dDvec_dD2vec = sparse( 2*diag(D2vec) );
    dD1vec_dy2 = kron(Isz, osz) - kron(osz, Isz); %should be sparse or not generated on command// save and reuse
    
    %gradiant should be 1 x sz
    dBeta_dy1 = dBeta_dDvec * dDvec_dD1vec * dD1vec_dy1;
    dBeta_dy2 = dBeta_dDvec * dDvec_dD2vec * dD1vec_dy2;
    
    %gpu version of gradient
    %dBeta_dDvec_g = gpuArray(full(dBeta_dDvec));
    %dDvec_dD1vec_g = gpuArray(full(dDvec_dD1vec));
    %dDvec_dD2vec_g = gpuArray(full(dDvec_dD2vec));
    %dD1vec_dy1_g = gpuArray(full(dD1vec_dy1));
    %dD1vec_dy2_g = gpuArray(full(dD1vec_dy2));
    
    %dBeta_dy1_g = dBeta_dDvec_g * dDvec_dD1vec_g * dD1vec_dy1_g;
    %dBeta_dy2_g = dBeta_dDvec_g * dDvec_dD2vec_g * dD1vec_dy2_g;
    
    %dBeta_dy1 = gather(dBeta_dy1_g);
    %dBeta_dy2 = gather(dBeta_dy2_g);
    
    
    %size(dBeta_dy1)
    %size(dBeta_dy2)
    
    

end

