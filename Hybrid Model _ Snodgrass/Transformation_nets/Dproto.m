function [ df_dp] = Dproto( V, p , o, r)
%DPROTO Summary of this function goes here
%   Detailed explanation goes here
    
    % df_dP
    % dB(p)_dp
    % dT(sum(p))_dp
    
    %disp('Finding Prototype Derivative');
    
    
    dE_dp = -V*r; 
    
    %forces p nodes to 0 and 1
    Bp = ((p).*(p-1)).^2;
    dBp_dp = 2*p .* ( 2*p.^2 - 3*p + 1);
    
    %pushes the total activation to 1
    T = (sum(p)-1).^2;
    pT = sum(p);
    dT_dp = 2*(pT-1)*p;
    
    %push to zeros
    dS_dp = p;
    
    df_dp = dE_dp + 0*dBp_dp + 1000*dT_dp;
    
    
end

