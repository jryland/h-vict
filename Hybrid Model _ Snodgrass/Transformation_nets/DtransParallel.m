function [ df_dy1, df_dy2] = DtransParallel( y1, y2, x1, x2, O, S, G, thetaSpeed, eta, imgSize)
%DTRANS Summary of this function goes here
%   Detailed explanation goes here

    %disp('Finding Translation Derivative');
    % Find Translation Derivative
    % Find Rotation Derivative
    % Find Scaling Derivative
    


    osz = gpuArray(ones(length(y1),1));
    sz = length(y1);
    
    
    Y1 = kron(y1, osz');
    Y2 = kron(y2, osz');
    X1 = kron(x1', osz);
    X2 = kron(x2', osz);
    
    
    % Individuals force ---------
    %df_dy1I = 2 * ( ( (o*s') .* (G.^2) .* (Y1-X1) ) * osz )';
    %df_dy2I = 2 * ( ( (o*s') .* (G.^2) .* (Y2-X2) ) * osz )';
    
    % Individuals force GPU version
    G_g = gpuArray(G);
    X1_g = gpuArray(X1);
    X2_g = gpuArray(X2);
    Y1_g = gpuArray(Y1);
    Y2_g = gpuArray(Y2);
    
    
    % Pairwise Similarity
    %O(pix,feature)     % OLD :: os_g = gpuArray(o*s');
    O = permute(O, [1 3 2]);
    S = permute(S, [3 1 2]);
    
    os_g = gpuArray(mdaTimes(O, S, 3, 'yes'));
    
    
    
    % these are the derivatives when using 1/((x-y)^2+T) and it is
    % correct now, but later we should use gaussains
    % New will be  (1/SD * 2^.5 * PI)e^(-(x-y)^2/SD)
    % this will be more stables
    df_dy1I_g = 2 * ( ( (os_g) .* (G_g.^2) .* (Y1_g-X1_g) ) * osz )';
    df_dy2I_g = 2 * ( ( (os_g) .* (G_g.^2) .* (Y2_g-X2_g) ) * osz )';
    
    df_dy1I = gather( df_dy1I_g );
    df_dy2I = gather( df_dy2I_g );
    
    osz = gather(osz);
    
    %figure()
    %imagesc(reshape(df_dy1I, 15, 15));
    
    
    % Meaned force XY ---------------------------------
    df_dy1M = sum(df_dy1I,1)*osz;
    df_dy2M = sum(df_dy2I,1)*osz;
    
    
    % Rotational Torque -------------------------------
    sigTheta = thetaSpeed;
    
    % Centered Coordinates
    yc1 = y1-mean(y1,1); 
    yc2 = y2-mean(y2,1);
    
    r = (yc1.^2+yc2.^2).^.5;
    
    %normalized component
    yc1_nc = yc1./r;
    yc1_nc( r==0 ) = 0;
    yc2_nc = yc2./r;      % used later
    yc2_nc( r==0 ) = 0;
    theta = acos(yc1_nc).*(1-(yc2<0)*2);
    
    %rotated vector/tangent vector
    yTan1 =  (yc2);
    yTan2 = -(yc1);
    
    %imagesc([ reshape(yTan1, 15,15)  reshape(yTan2, 15,15)]) 
    
    %fThetaI = [df_dy1I' df_dy2I'] * [yTan1'; yTan2'];
    fProj1 = df_dy1I' .* yTan1;
    fProj2 = df_dy2I' .* yTan2;
    fThetaI = fProj1+fProj2;
    
    fThetaI(r==0) = 0;
    
    %figure()
    %imagesc([ reshape(df_dy1I, 15,15) reshape(df_dy2I, 15,15) ]);
    
    %max(max(fThetaI))
    
    m = 0;%.95;
    fTheta = sum(fThetaI,1)*(1-m);
    thetaMoment = fTheta;
    %fTheta = .1;
    
    df_f1R = r .* cos( -sigTheta*fTheta*osz + theta) - yc1;
    df_f2R = r .* sin( -sigTheta*fTheta*osz + theta) - yc2;
    
    %df_f1R = yTan1*fTheta*sigTheta;
    %df_f2R = yTan2*fTheta*sigTheta;
    
    
    % Scaling Force -----------------------------------
    
    fProjSc1 = df_dy1I' .* yc1_nc;
    fProjSc2 = df_dy2I' .* yc2_nc;
    
    %fRad = -sum((fProjSc1.^2+fProjSc2.^2).^5)
    
    goalRad11 = ((imgSize/2)^2+(imgSize/2)^2)^.5;
    thisRad11 = r(1,1);
    
    fRad = -(goalRad11 - thisRad11); % constant size
    
    rMax = max(r);
    
    df_dy1Sc = fRad * yc1/(rMax); 
    df_dy2Sc = fRad * yc2/(rMax);
    
    
    
     
    % Total -------------------------------------------
    df_dy1 = eta*( .000*df_dy1I + .2*df_dy1M + .012*df_dy1Sc' ) + df_f1R';
    df_dy2 = eta*( .000*df_dy2I + .2*df_dy2M + .012*df_dy2Sc' ) + df_f2R';
    
    
    %nnz(G);
    
end