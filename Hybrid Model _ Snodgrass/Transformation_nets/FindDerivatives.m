function [G, r, rt, dBeta_dy1, dBeta_dy2, dTrans_dy1, dTrans_dy2, df_dp] = FindDerivatives(y1, y2, x1, x2, S_filt, SD, Bvec, a, Nvec, V_filts, p, O_filt, thetaSp, eta, sz)

    numFilts = size(S_filt,2);

    nz = size(S_filt, 1);
    
    np = size(p,1);
    
    dBeta_dy1_all   = zeros( 1,nz);
    dBeta_dy2_all   = zeros( 1,nz);
    dTrans_dy1_all  = zeros( 1,nz,numFilts);
    dTrans_dy2_all  = zeros( 1,nz,numFilts);
    df_dp_all       = zeros(np, numFilts);
    
    [G_all, r_all, rt_all] = RtransformAll(y1, y2, x1, x2, S_filt, SD);
        
    %Find Derivative w/r Beta
    %[dBeta_dy1_all, dBeta_dy2_all] = Ddist(y1, y2, Bvec, a, Nvec);

    
    %find the derivative for each feature space
    for i = 1:numFilts        
        
        %Find Derivative w/r Trans
        %[dTrans_dy1_all(:,:,i), dTrans_dy2_all(:,:,i), thetaMoment] = Dtrans(y1, y2, x1, x2, O_filt(:,i), S_filt(:,i), G_all, thetaMoment, sz);

        %Find Derivative w/r p
        %[df_dp_all(:,i)] = Dproto(V_filts(:,:,i), p, O_filt(:,i), r_all(:,i));

    end

    
    [dTrans_dy1, dTrans_dy2] = DtransParallel(y1, y2, x1, x2, O_filt(:,i), S_filt(:,i), G_all, thetaSp, eta, sz);
    
    G = G_all;
    r = r_all;
    rt = rt_all;
    dBeta_dy1 = dBeta_dy1_all;
    dBeta_dy2 = dBeta_dy2_all;
    %dTrans_dy1(:,:)      = sum(dTrans_dy1_all, 3);
    %dTrans_dy2(:,:)      = sum(dTrans_dy2_all, 3);
    df_dp(:,:)           = sum(df_dp_all, 2);
end

