function [ gn, sn ] = FitFeatureTransProto( imageFigure, filters, V_filts, filterWeighting, protoWeighting, sz)

    if isempty(imageFigure)
        imageFigure = figure();
    end
    
    
    filters = MakeOddFilters();
    
    numFilters = length(filters);
    
    %this should really be done elsewhere and save to be efficient
    %[Vraw] = MakePrototypeLetters(sz);
    %[V_filts filterWeighting, protoWeighting] = MakeFilteredPrototypeLetters(Vraw, filters, sz);
    [s o dummy sn] = MakeStimLetter(sz);
    Simg = reshape(s, sz, sz);
    Simg_filt = ApplyFiltersSimple(Simg, filters);
    
    %applying filter weighting
    for i = 1:size(filterWeighting,1)
        Simg_filt(:,:,i) = Simg_filt(:,:,i)*filterWeighting(i);
    end
    
    S_filt = reshape(Simg_filt, sz*sz, numFilters);
    
    
    Simg_filt = Simg_filt/max(Simg_filt(:));
    S_filt = S_filt/sum(S_filt(:))*numFilters;
    
    
    gn = 0;
    
    p = ones(size(V_filts,1),1);
    p = p/size(p,1);
    p_sim = ones(size(p))/size(p,1);
    
    %p = protoWeighting;
    
    %Make a new variable to record the Highest Attractions/and Matches
    

    [y1 y2 Bvec Nvec] = MakeGrid(sz);
    
    
    %setup input references
    x1 = y1;
    x2 = y2;
    
       
    %Gradient Parameters
    a = 1;
    sigma = .06;
    eta   = 600;
    gamma = .5;
    dMax = .017;
    dMax_dp = .2;
    pNoise = 0;
    SD = .75;
    Criterion = .95; % cosine similarity criterion
    epochLim = 40;
    matchFound = 0;
    sim_max = 0;
    
    %maximum similarity and error match jugements
    mMaxs = zeros(size(p));
    p_sim_max = zeros(size(p));
    
    if isempty(imageFigure)
        imageFigure = figure();
    end
    
    
    [G r rt] = RtransformAll(y1, y2, x1, x2, S_filt, SD);
    O_filt = O_allFromP(V_filts, p_sim);
    
    
    % graphics Stuff
    set(0,'CurrentFigure', imageFigure);
    subplot(2,4,1); SimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]); 
    subplot(2,4,2); RimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]);
    subplot(2,4,4); OimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]);
    subplot(2,4,3); scatH = scatter(y1, y2); daspect([1 1 1]);
    hold on;
    scatH2 = scatter(x1, x2);
    hold off;
    subplot(2,4,5:7); PimgHandle = image(zeros(sz*4,sz*size(p,1),3)); daspect([1 1 1]);
    subplot(2,4,8); WinimgHandle = image(zeros(sz,sz,3)); daspect([1 1 1]);
    
    
    
    %daspect([1 1 1]);
    
    %figure();
    pause(.2);
    
    fit = 1000;
    
    for i=1:epochLim
        
        % Here is where we left off!
        [G, r, rt, dBeta_dy1, dBeta_dy2, dTrans_dy1, dTrans_dy2, df_dp ]  = ...
            FindDerivatives(y1, y2, x1, x2, S_filt, SD, Bvec, a, Nvec, V_filts, p_sim, O_filt);
        
        %apply speed limits
        dy_norm = norm([dTrans_dy1(:); dTrans_dy2(:)]);
        if dy_norm>dMax
            dTrans_dy1 = dTrans_dy1/dy_norm*dMax;
            dTrans_dy2 = dTrans_dy2/dy_norm*dMax; 
        end
        dp_norm = norm(df_dp(:));
        if dp_norm>dMax_dp
            df_dp = df_dp/dp_norm*dMax_dp;
        end
        
        
        %Perform Gradient Descent on transformation Parameters
        y1 = y1 - sigma*dBeta_dy1' - (eta*dTrans_dy1');%/norm(dTrans_dy1))';
        y2 = y2 - sigma*dBeta_dy2' - (eta*dTrans_dy2');%/norm(dTrans_dy2))';
        
        %Perform Gradient Descent on prototype contributions using
        %attraction
        p = p - gamma*df_dp;
        
        p = p+(rand(size(p)).^1)*pNoise;
        
        p(p<0) = 0;
        
        %Rectification Normalization _ makes other terms in df_dp more crit
        p(p>1) = 1;
        
        %prototype activation from similarity to transformed input 
        p_sim = P_allFromRt(V_filts, rt);
        
        %calculate prototype error match to transformed input
        m = MfromRt_all(V_filts, S_filt);
        
        %update the maximum similarity judgements
        mMaxs = max(m, mMaxs);
        p_sim_max = max(p_sim_max, p_sim);
        
        % Graphics stuff
        O_filt = O_allFromP(V_filts, p_sim);
        
        Simg_filt = reshape(S_filt, sz, sz, numFilters);
        Oimg_filt = reshape(O_filt, sz, sz, numFilters);
        Rtimg_filt= reshape(rt, sz, sz, numFilters);
        Simg_filt = Simg_filt/max(Simg_filt(:));
        Oimg_filt = Oimg_filt/max(Oimg_filt(:));
        Rtimg_filt = Rtimg_filt/max(Rtimg_filt(:));
        
        set(SimgHandle, 'CData', Simg_filt(:,:,1:3));
        set(RimgHandle, 'CData', Rtimg_filt(:,:,1:3));
        set(OimgHandle, 'CData', Oimg_filt(:,:,1:3));
        set(scatH , 'XData', y1);
        set(scatH , 'YData', y2);
        
        %decide on a winner previously used 
        sim_max = max(p_sim_max)
        win = find(p_sim_max==max(p_sim_max));
        win = win(1);
        winVec = zeros(size(p_sim_max));
        winVec(win) = 1;
        gn = win;
        
        
        Winimg = reshape(V_filts(win(1),:,:), sz, sz, numFilters);
        Winimg = Winimg/max(Winimg(:));
        set(WinimgHandle, 'CData', Winimg(:,:,1:3));
        
        
        %Create indicators of Activity Due to Attraction Principles
        Img_activity = reshape(kron(ones(sz*sz,numFilters),p_sim'), sz, sz*size(p,1), numFilters );
        
        Img_activity_sim = reshape(kron(ones(sz*sz,numFilters),p_sim_max'), sz, sz*size(p_sim_max,1), numFilters );
        
        ProtoImgs = reshape( permute(V_filts, [2 1 3]) , sz, sz*size(p,1), numFilters);
        
        Activity_Display = Img_activity.*ProtoImgs;
        Activity_Display = Activity_Display/max(Activity_Display(:));
        
        interval = max(p_sim) - min(p_sim);
        
        Img_activity_rel = (Img_activity-min(p_sim))/interval;
        
        Img_activity_sim = Img_activity_sim/max(p_sim_max);
        
        Img_activity_rel(:,:,1) = Img_activity_rel(:,:,1).*(Img_activity_rel(:,:,1)<(mean(Img_activity_rel(:))+.25));
        
        Activity_Display_rel = Img_activity_rel.*ProtoImgs;
        Activity_Display_rel = Activity_Display_rel/max(Activity_Display_rel(:));
        
        
        %create indicators of Activity Due to Error Minimization Principles
        interval_m = max(m) - min(m);
        
        Img_activity_m = reshape(kron(ones(sz*sz,numFilters),m'), sz, sz*size(m,1), numFilters );
        
        Img_activity_m_rel = (Img_activity_m-min(m))/interval_m;
        
        Img_activity_m_rel(:,:,3) = Img_activity_m_rel(:,:,3).*(Img_activity_m_rel(:,:,3)<(mean(Img_activity_m_rel(:))+.25));
        
        
        %Display all Activity Indicators
        set(PimgHandle, 'CData', [Activity_Display(:,:,1:3); Activity_Display_rel; Img_activity_rel(:,:,1:3); Img_activity_sim]);
        
        
        fit = sum((O_filt(:)-rt(:)).^2);
        disp(['Epoch = ' mat2str(i) ' || Fit = ' mat2str(fit) '|| Mag O = ' mat2str(norm(o)) ]);
        
        pause(1);
        
        if sim_max>(Criterion)
            matchFound = 1;
            disp('Match');
            break;
        end
        
    end
    
    if sim_max<(Criterion)
        disp('No-match');
    end
    
    %delete(imageFigure);
    

end

