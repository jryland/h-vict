function [ matchFound match ] = FitTest( imageFigure )
%FITTEST Summary of this function goes here
%   Detailed explanation goes here

    sz = 15;

    [s o match] = MakeStimLetter(sz);

    

    [y1 y2 Bvec Nvec] = MakeGrid(sz);
    
    
    %setup input references
    x1 = y1;
    x2 = y2;
    
       
    %Gradient Parameters
    a = 1;
    sigma = .02;
    eta   = .50;
    SD = 14;
    Criterion = 2;
    epochLim = 40;
    matchFound = 0;
    
    if isempty(imageFigure)
        imageFigure = figure();
    end
    [dummy dymmy o] = Rtransform(x1, x2, x1, x2, o, SD);
    [G r rt] = Rtransform(y1, y2, x1, x2, s, SD);
    %[dummy dummy o] = Rtransform(y1, y2, x1, x2, o, 1);
    
    set(0,'CurrentFigure', imageFigure);
    subplot(1,4,1); SimgHandle = imagesc(reshape(s, sz, sz)); daspect([1 1 1]); 
    subplot(1,4,2); RimgHandle = imagesc(reshape(r, sz, sz)); daspect([1 1 1]);
    subplot(1,4,4); OimgHandle = imagesc(reshape(o, sz, sz)); daspect([1 1 1]);
    subplot(1,4,3); scatH = scatter(y1, y2); daspect([1 1 1]);
    hold on;
    scatH2 = scatter(x1, x2);
    hold off;
    
    %daspect([1 1 1]);
    
    %figure();
    pause(.2);
    
    fit = 1000;
    
    for i=1:epochLim
        
        
        %Get the current output
        [G r rt] = Rtransform(y1, y2, x1, x2, s, SD);
        
        %Find Derivative w/r Beta
        [dBeta_dy1 dBeta_dy2] = Ddist(y1, y2, Bvec, a, Nvec);
        
        %Find Derivative w/r Trans
        [dTrans_dy1 dTrans_dy2] = Dtrans(y1, y2, x1, x2, o, s, G);
        
        %Perform Gradient Descent
        y1 = y1 - sigma*dBeta_dy1' - (eta*dTrans_dy1');%/norm(dTrans_dy1))';
        y2 = y2 - sigma*dBeta_dy2' - (eta*dTrans_dy2');%/norm(dTrans_dy2))';
        
        
        
        set(SimgHandle, 'CData', reshape(s, sz, sz));
        set(RimgHandle, 'CData', reshape(rt, sz, sz));
        set(OimgHandle, 'CData', reshape(o, sz, sz));
        set(scatH , 'XData', y1);
        set(scatH , 'YData', y2);
        
        
        fit = sum((o-rt).^2);
        disp(['Epoch = ' mat2str(i) ' || Fit = ' mat2str(fit)]);
        
        pause(.01);
        %pause(.01);
        
        if fit<Criterion
            matchFound = 1;
            disp('Match');
            break;
        end
        
    end
    
    if fit>=Criterion
        disp('No-match');
    end
    
    %delete(imageFigure);
    

end

