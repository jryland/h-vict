function [ gn sn] = FitTransProto( imageFigure )
%FITTEST Summary of this function goes here
%   Detailed explanation goes here

    sz = 15;
    
    
    [Vraw] = MakePrototypeLetters(sz);
    
    [s o dummy sn] = MakeStimLetter(sz);
    s = s/sum(s);
    
    gn = 0;
    
    p = ones(size(Vraw,1),1);
    p = p/size(p,1);
    

    [y1 y2 Bvec Nvec] = MakeGrid(sz);
    
    
    %setup input references
    x1 = y1;
    x2 = y2;
    
       
    %Gradient Parameters
    a = 1;
    sigma = .02;
    eta   = 1.8;
    gamma = .04;
    dMax = .15;
    SD = .5;
    Criterion = .01;
    epochLim = 80;
    matchFound = 0;
    
    if isempty(imageFigure)
        imageFigure = figure();
    end
    [dummy dymmy Vt] = Rtransform(x1, x2, x1, x2, Vraw', SD);
    V = Vt';
    o = OfromP(V, p);
    
    [G r rt] = Rtransform(y1, y2, x1, x2, s, SD);
    %[dummy dummy o] = Rtransform(y1, y2, x1, x2, o, 1);
    
    set(0,'CurrentFigure', imageFigure);
    subplot(2,4,1); SimgHandle = imagesc(reshape(s, sz, sz)); daspect([1 1 1]); 
    subplot(2,4,2); RimgHandle = imagesc(reshape(r, sz, sz)); daspect([1 1 1]);
    subplot(2,4,4); OimgHandle = imagesc(reshape(o, sz, sz)); daspect([1 1 1]);
    subplot(2,4,3); scatH = scatter(y1, y2); daspect([1 1 1]);
    subplot(2,4,5); PimgHandle = imagesc(p); daspect([1 1 1]);
    subplot(2,4,8); WinimgHandle = imagesc(zeros(sz,sz)); daspect([1 1 1]);
    
    
    hold on;
    scatH2 = scatter(x1, x2);
    hold off;
    
    %daspect([1 1 1]);
    
    %figure();
    pause(.2);
    
    fit = 1000;
    
    for i=1:epochLim
        
        
        %Get the current output
        [G r rt] = Rtransform(y1, y2, x1, x2, s, SD);
        
        %Find Derivative w/r Beta
        [dBeta_dy1 dBeta_dy2] = Ddist(y1, y2, Bvec, a, Nvec);
        
        %Find Derivative w/r Trans
        [dTrans_dy1 dTrans_dy2] = Dtrans(y1, y2, x1, x2, o, s, G);
        
        %Find Derivative w/r p
        [df_dp] = Dproto(V, p, o, r);
        
        %Perform Gradient Descent on transformation Parameters
        y1 = y1 - sigma*dBeta_dy1' - (eta*dTrans_dy1');%/norm(dTrans_dy1))';
        y2 = y2 - sigma*dBeta_dy2' - (eta*dTrans_dy2');%/norm(dTrans_dy2))';
        
        %Perform Gradient Descent on prototype contributions
        p = p - gamma*df_dp;
        
        p = p/sum(p);
        
        o = OfromP(V, p);
        
        set(SimgHandle, 'CData', reshape(s, sz, sz));
        set(RimgHandle, 'CData', reshape(rt, sz, sz));
        set(OimgHandle, 'CData', reshape(o, sz, sz));
        set(scatH , 'XData', y1);
        set(scatH , 'YData', y2);
        
        win = find(p==max(p));
        win = win(1);
        winVec = p'==max(p);
        
        gn = win;
        
        set(WinimgHandle, 'CData', reshape(V(win(1),:), sz, sz));
        set(PimgHandle, 'CData', p);
        
        
        fit = sum((o-rt).^2);
        disp(['Epoch = ' mat2str(i) ' || Fit = ' mat2str(fit) '|| Mag O = ' mat2str(norm(o)) ]);
        
        pause(.01);
        %pause(.01);
        
        if fit<Criterion
            matchFound = 1;
            disp('Match');
            break;
        end
        
    end
    
    if fit>=Criterion
        disp('No-match');
    end
    
    %delete(imageFigure);
    

end

