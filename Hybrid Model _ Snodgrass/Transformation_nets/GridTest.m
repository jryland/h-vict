function [ ] = GridTest( )
%TEST Summary of this function goes here
%   Detailed explanation goes here

    [y1 y2 Bvec Nvec] = MakeGrid(15);
    
    x1 = y1;
    x2 = y2;
    
    y1 = y1 + 1*(rand(size(y1))-.5);
    y2 = y2 + 1*(rand(size(y2))-.5);
    
    theta = rand(1,1)*pi;
    
    
    r = [cos(theta) -sin(theta); sin(theta) cos(theta)];
    ry = (r*([y1 y2])')';
    y1 = ry(:,1); 
    y2 = ry(:,2);
    
    
    
    %scale multiplier
    a = 1.8;
    
    sigma = .10;
    
    
    figure();
    
    hold on;
    scatH = scatter(y1, y2);
    scatH2 = scatter(x1, x2);
    hold off;
    
    daspect([1 1 1]);
    
    pause(.1);
    
    for i=1:50
        
        disp(i)
        
        %Find Derivative w/r Beta
        [dBeta_dy1 dBeta_dy2] = Ddist(y1, y2, Bvec, a, Nvec);
        
        %Perform Gradient Descent
        y1 = y1 - sigma*dBeta_dy1'
        y2 = y2 - sigma*dBeta_dy2'
        
        
        set(scatH , 'XData', y1);
        set(scatH , 'YData', y2);
        
        
        pause(.1);
        
    end

end

