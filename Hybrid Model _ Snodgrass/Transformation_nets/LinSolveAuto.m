function [ W ] = LinSolveAuto( input )
%Find the weight matrix for a reconstruction auto-associative network

%     subplot(1,2,1);
%     imagesc(input);

    A = input';
    B = input';
    [X, R]= linsolve(A, B);

    W = X';
    
    
    
%     subplot(1,2,2);
%     imagesc( W*input );
%     
%     disp(['Rank of Prototypes: ', num2str(R)]);
    
end

