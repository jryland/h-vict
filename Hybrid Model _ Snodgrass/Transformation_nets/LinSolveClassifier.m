function [ W ] = LinSolveClassifier( input, classes )

    A = input';
    B = classes';
    [X, R]= linsolve(A, B);

    W = X';
    
end

