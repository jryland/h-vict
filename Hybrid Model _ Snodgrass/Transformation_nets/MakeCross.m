function [ s o ] = MakeCross( sz )
%MAKECROSS Summary of this function goes here
%   Detailed explanation goes here
    %s will be the stimulus
    %o will be the prototype to match to

    midPoint = round(sz/2);
    
    % create the test cross image
    Oimg = zeros(sz,sz);
    
    spoke = [0 0 1 1 1 0 0];
    spoke2 =[1 1 0 0 0 1 1];
    
    Oimg(midPoint,(midPoint-3):(midPoint+3)) = spoke;
    Oimg((midPoint-3):(midPoint+3),midPoint) = spoke2';
    
    
    %Oimg(midPoint,midPoint) = 1;
    
    
    
    
    % create the stimulus cross image
    shift = [round((rand(1,1)-.5)*9) round((rand(1,1)-.5)*9)];
    Simg = imrotate(Oimg, rand(1,1)*90, 'Bilinear', 'crop');
    Simg = circshift(Simg, shift);
    
    
    
    %Oimg blur
    %gauss = fspecial('gaussian', []);
    %Oimg = convn(Oimg, gauss, 'same');
    
    o = Oimg(:);
    s = Simg(:);
    
    %display thie images side by side
    %figure();
    %subplot(1,2,1); imagesc(Oimg);
    %subplot(1,2,2); imagesc(Simg);
    
    



end

