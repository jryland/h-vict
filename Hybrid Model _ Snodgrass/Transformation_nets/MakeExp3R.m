function [  ] = MakeExp3R( HVICT_congruency, varPrefix, run )

    HVICT_congruency.primePerf; 
    HVICT_congruency.probePerfs;
    HVICT_congruency.pairInds;
    orientA = HVICT_congruency.orientA;
    orientB = HVICT_congruency.orientB;

    
    fullMat = HVICT_congruency.probePerfs;
    
    ppOrients = {['prime' orientA '_probe' orientA ],...
                 ['prime' orientA '_probe' orientB ],...
                 ['prime' orientB '_probe' orientA ],...
                 ['prime' orientB '_probe' orientB ]};
    
    fullTable = array2table(fullMat, 'VariableNames', ppOrients);

    writetable(fullTable, ['Run' num2str(run) varPrefix 'exp3.txt']);
    
end

