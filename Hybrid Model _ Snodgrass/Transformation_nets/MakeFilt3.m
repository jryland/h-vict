function [ filt ] = MakeFilt3( fNum)
%MAKEFILT Summary of this function goes here
%   Detailed explanation goes here


    % need to use higher resolution features

    filts(:,:,1) = [0 0 0;...
                    0 1 0;...
                    0 0 0; ];
   
    filts(:,:,2) = [0 1 0;...
                    0 1 0;...
                    0 1 0; ];
   
    filts(:,:,3) = [0 0 0;...
                    0 1 0;...
                    1 0 1; ];
    
    
    filt = filts(:,:,fNum);
    
    filt = ( filt-mean(filt(:)) );
    
    filt = filt/norm(filt(:));
    
end