function [ filt360 ] = MakeFilt360( filt, steps )
    
    if isempty(filt)
        testFilt = [0 1 0; 0 1 0; 0 1 0];
        filt = testFilt;
    end
    
    inc = 360/steps;
    
    fsz = size(filt,1); 
    fpsz = fsz*2+1;
    hf = round(fsz/2);
    hfp = round(fpsz/2);
    filtPad = zeros(fpsz, fpsz);
    a = (hfp-hf+1);
    b = (hfp-hf+fsz);
    
    size(a:b)
    size(filt)
    
    
    filtPad( a:b, a:b ) = filt;
    
    %imagesc(filtPad); daspect([1 1 1]); 
    
    filt360 = zeros(fpsz, fpsz, steps);
    
    for i = 1:steps
       filt360(:,:,i) = imrotate(filtPad, (i)*inc, 'bilinear', 'crop');
       
       %pause(.1);
       %imagesc(filt360(:,:,i));
       
    end
    

end