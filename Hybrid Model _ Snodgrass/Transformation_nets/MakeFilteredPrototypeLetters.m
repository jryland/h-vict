function [ FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypeLetters( PrototypesRaw, filters, sz )
    
    numFilters = length(filters);

    numPrototypes = size(PrototypesRaw,1);

    FilteredPrototypes = zeros(numPrototypes, sz*sz, numFilters);
    
    
    for i = 1:numPrototypes
        
        img = reshape(PrototypesRaw(i,:), sz, sz ); 
        FilteredPrototypeImg = ApplyFiltersSimple(img, filters);
        FilteredPrototypes(i,:,:) = reshape(FilteredPrototypeImg, 1, sz*sz, numFilters);
        
        
    end
    
    %normalize contributions of features, unique features are more
    %important than overused features! This is a good static weighting for
    %matching.
   
    filterWeighting = ones(numFilters,1)./squeeze(sum(sum(FilteredPrototypes,1),2));
    
    for i=1:numFilters
        
        FilteredPrototypes(:,:,i) = FilteredPrototypes(:,:,i)*filterWeighting(i);
        
    end
    
    %also normalize filters by their average sum of pixel strength
    protoWeighting = ones(numPrototypes,1)./squeeze(sum(sum(FilteredPrototypes,2),3));
    
    for i=1:numPrototypes
        
        FilteredPrototypes(i,:,:) = FilteredPrototypes(i,:,:)*protoWeighting(i);
        
    end
    
    %magnitude normalization
    for i = 1:numPrototypes
        
        proto = FilteredPrototypes(i,:,:);
        FilteredPrototypes(i,:,:) = proto/norm(proto(:));
        
        
    end
    
    
    
    
end

