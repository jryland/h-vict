function [ FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes( PrototypesRaw_cell, weightFilters )
% produce a filtered prototype matrix    
    

    numFilters = size(PrototypesRaw_cell{1},3);
    sz = size(PrototypesRaw_cell{1}, 1);

    numPrototypes = length(PrototypesRaw_cell);

    
    for i = 1:numPrototypes
        
        FilteredPrototypes(i,:,:) = reshape(PrototypesRaw_cell{i}, 1, sz*sz, numFilters);

    end
    
    %normalize contributions of features, unique features are more
    %important than overused features! This is a good static weighting for
    %matching. At least this was true for letters with only 3 features...
   
    filterWeighting = ones(numFilters,1);
    
    if strcmp(weightFilters, 'inverse-frequency')
        
        filterWeighting = ones(numFilters,1)./squeeze(sum(sum(FilteredPrototypes,1),2));
        for i=1:numFilters

            FilteredPrototypes(:,:,i) = FilteredPrototypes(:,:,i)*filterWeighting(i);

        end
    end
        
    %also normalize filters by their average sum of pixel strength
    protoWeighting = ones(numPrototypes,1)./squeeze(sum(sum(FilteredPrototypes,2),3));
    
    for i=1:numPrototypes
        
        FilteredPrototypes(i,:,:) = FilteredPrototypes(i,:,:)*protoWeighting(i);
        
    end
    
    %magnitude normalization
    for i = 1:numPrototypes
        
        proto = FilteredPrototypes(i,:,:);
        FilteredPrototypes(i,:,:) = proto/norm(proto(:));
        
        
    end
    
    
    
    
end

