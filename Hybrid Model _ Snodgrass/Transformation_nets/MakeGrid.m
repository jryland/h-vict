function [ y1 y2 Bvec Nvec] = MakeGrid( sz )
%MAKEGRID Summary of this function goes here
%   Detailed explanation goes here

    %B is the prefered distance squared
    %y1 and 2 represent the centers of each neurons current receptive feild
    
    
    [Y1 Y2] = meshgrid(1:sz, 1:sz);
    
    y1 = Y1(:); 
    y2 = Y2(:);
    
    oneSZ = ones(sz*sz, 1);
    
    size(oneSZ);
    size(y1);
    
    
            %Dim 1                  %Dim 2
    B = (y1*oneSZ' - oneSZ*y1').^2 + (y2*oneSZ' - oneSZ*y2').^2;
    
                %Dim 1                                       %Dim 2
    Bvec = (kron(y1,oneSZ) - kron(oneSZ,y1)).^2 + (kron(y2,oneSZ) - kron(oneSZ,y2)).^2;
    
    %Neighbor Matrix % helps to make things sparse
    Nvec = [];
    for i=1:(sz^2)
        
        board = zeros(sz,sz);
        board(y1(i),y2(i)) = 1;
        N = convn(board , ones([3,3]), 'same');
        Nvec = [Nvec; N(:)];
    end
    Nvec = sparse(Nvec);
    
    
    %Nvec = ones(sz^3,1);
    %figure();
    %subplot(1,3,1); imagesc(B);
    %subplot(1,3,2); imagesc(reshape(Bvec,sz^2,sz^2));
    %subplot(1,3,3); imagesc(reshape(Bvec.*Nvec,sz^2,sz^2));
    

end

