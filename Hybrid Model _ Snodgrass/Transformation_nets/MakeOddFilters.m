function [ oddFilts] = MakeOddFilters(  )
% Generates a set of 360 degree filters for pooling.


    filt1 = fspecial('log', 5, .5); 
    %filt = fspecial('motion', 9, 45);
    filt1 = (filt-mean(filt1(:)))/sum(abs(filt1(:)));
    f1 = MakeFilt360(filt1, 180);
    
    filt2 = fspecial('motion', 9, 45);
    filt2 = (filt2-mean(filt2(:)))/sum(abs(filt2(:)));
    f2 = MakeFilt360(filt2, 180);
    
    ex = fspecial('log', 5, .5);
    filt3 = convn(fspecial('disk', 5),ex, 'full');
    filt3 = (filt3-mean(filt3(:)))/sum(abs(filt3(:)));
    f3 = MakeFilt360(filt3, 180);
    
    
    
    oddFilts = {f1, f2, f3};
    
end

