function [ Vraw ] = MakePrototypeLetters( sz )
%MAKECROSS Summary of this function goes here
%   Detailed explanation goes here
    %V is a collection of vectorized prototype images

    midPoint = round(sz/2);
    maxRot = 60;
    % create the test cross image
    Oimg = zeros(sz,sz);
    
    
    
    %[X I] = prprob();
    
    % saved list of small scale character recognition stuff
    load prprobStruct;
    X = prprobStruct.X;
    I = prprobStruct.I;
    
    
    VrawT = zeros(sz*sz, size(X,2));
    
    %format into the correct size
    
    for i = 1:size(X,2)
        Oimg = zeros(sz,sz);
        Limg = reshape(X(1:35, i), 5,7)';
        Oimg((midPoint-3):(midPoint+3),(midPoint-2):(midPoint+2)) = Limg;
        VrawT(:,i) = Oimg(:)/sum(Oimg(:));
        
    end
    
    Vraw = VrawT';
    size(Vraw)
    size(sum(Vraw,1))
    
    
    %test
    %imagesc(reshape(sum(Vraw(:,:),1)',sz,sz));
    
end

