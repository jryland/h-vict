function [ s o match sn] = MakeStimLetter( sz )
%MAKECROSS Summary of this function goes here
%   Detailed explanation goes here
    %s will be the stimulus
    %o will be the prototype to match to

    
    %force match!
    forceMatch = 1;
    
    midPoint = round(sz/2);
    maxRot = 30;
    maxShift = 3;
    match = 1;
    % create the test cross image
    Oimg = zeros(sz,sz);
    
    % list of small scale character recognition stuff
    %[X I] = prprob();
    
    % saved list of small scale character recognition stuff
    load prprobStruct;
    X = prprobStruct.X;
    I = prprobStruct.I;
    
    
    l = round(rand(1,1)*25)+1;
    l2 = 0;
    
    Limg = reshape(X(1:35, l), 5,7)';
    Oimg((midPoint-3):(midPoint+3),(midPoint-2):(midPoint+2)) = Limg;
    
    Simg = zeros(sz,sz);
    if ( rand(1,1)>.5 ) || ( forceMatch )
        Simg = Oimg;
        l2 = l;
    else
        match = 0;
        l2 = round(rand(1,1)*25)+1;  
        L2img = reshape(X(1:35, l2), 5,7)';
        Simg((midPoint-3):(midPoint+3),(midPoint-2):(midPoint+2)) = L2img;
        if l==l2
            match = 1;
        end
    end
    
    %Oimg(midPoint,midPoint) = 1;
    
    sn = l2;
    
    
    % create the stimulus cross image
    shift = [round((rand(1,1)-.5)*maxShift) round((rand(1,1)-.5)*maxShift)];
    Simg = imrotate(Simg, (rand(1,1)-.5)*2*maxRot, 'Bilinear', 'crop');
    Simg = circshift(Simg, shift);
    
    
    
    %Oimg blur
    %gauss = fspecial('gaussian', []);
    %Oimg = convn(Oimg, gauss, 'same');
    
    o = Oimg(:);
    s = Simg(:);
    
    %display thie images side by side
    %figure();
    %subplot(1,2,1); imagesc(Oimg);
    %subplot(1,2,2); imagesc(Simg);
    
    

end

