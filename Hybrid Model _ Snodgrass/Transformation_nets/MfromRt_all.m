function [ m  ] = MfromRt_all( V_filts, S_filts )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    % m for match by error
    numFilters = size(V_filts,3);
    numProto = size(V_filts,1);
    
    m_all = zeros(numProto, numFilters);
    
    for i = 1:numFilters
        
        m_all(:,1) = V_filts(:,:,i)*S_filts(:,i);
        
    end
    
    m = sum(m_all,2);
    
    m = m/max(m(:));



end

