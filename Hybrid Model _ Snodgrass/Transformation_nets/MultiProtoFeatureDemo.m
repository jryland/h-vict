function [ successRate] = MultiProtoFeatureDemo( numRuns)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here


    %Graphics
    scoreFigure = figure();
    imageFigure = figure();

    scoreListBox = uicontrol('Parent', scoreFigure, 'style', 'listbox', 'Units', 'normalized',...
                    'Position', [0.1 0.1 .8 .8]);
    
    scoreCell = {};       
    
    disp('Creating filters and prototype Letters Please Wait');
    
    sz = 21;
    
    filters = MakeOddFilters();
    [Vraw] = MakePrototypeLetters(sz);
    [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypeLetters(Vraw, filters, sz);
    
    successes = 0;
    successesRate = 0;
    
    disp('Please Press Enter to start the simulations');
    
    pause();
    
    for i = 1:numRuns
        [numGuess numStim] = FitFeatureTransProto(imageFigure, filters, FilteredPrototypes, filterWeighting, protoWeighting, sz);
        
        if numGuess == numStim
            successes = successes + 1;
            score = 'Correct   : ';
        else
            score = 'Incorrect : ';
        end
        
        successRate = successes/i*100;
        
        scoreCell{i} = [score mat2str(successRate) '%'];
        set(scoreListBox, 'String', scoreCell);
        set(scoreListBox, 'Value', i);
        
    end
    



end


