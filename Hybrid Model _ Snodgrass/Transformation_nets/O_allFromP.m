function [ O_filt ] = O_allFromP(V_filt, p )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

    nz = size(V_filt, 2);
    numFilters = size(V_filt, 3);
    O_filt = zeros(nz,numFilters);

    for i = 1:numFilters
        O_filt(:, i) = OfromP(V_filt(:,:,i), p);
    end

end

