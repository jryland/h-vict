function [ p_sim ] = P_allFromRt( V_filts, Rt_filt)
% this is a similarity based way to adjust the O target

    %may require another normalization of V_flits such that each prototype
    %has a magnitude of 1.

    numFilters = size(V_filts, 3);
    
    nz = size(Rt_filt,1);
    
    pz = size(V_filts,1);
    
    p_all = zeros(pz, numFilters);
    
    for i = 1:numFilters
        
        p_all(:,i) = V_filts(:,:,i) * (Rt_filt(:,i) / norm(Rt_filt(:)) );
    
    end
   
    p_sim = sum(p_all, 2);
    

end

