function [ successRate] = RotationExp(inputFolderName, inputInfo, tNetInfo, numPairs, epochLim)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here


    %Graphics
    scoreFigure = figure();
    imageFigure = figure();
    orientFigure = figure();
    
    
    scoreListBox = uicontrol('Parent', scoreFigure, 'style', 'listbox', 'Units', 'normalized',...
                    'Position', [0.1 0.1 .8 .8]);
    
    scoreCell = {};       
    
    disp('Creating learned prototype matrix: Please Wait');
    
    
    % load a set of base or hidden representations
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    weightFilters =  'none'; % 'inverse-frequency'; %'inverse-frequency';
    
    compIm_c = loadInputFrom(inputFolderName, inRez, inNum, inStyle, inHidden);
    compIm_c = compIm_c(1:inNum);
    numS = length(compIm_c);
    numChan = size(compIm_c{1}, 3);
    
    numProto = numS/inOrients;
    numNodes = inRez.^2;
    
    
    %remove negative values (don't know why they are there to begin with)
    for i = 1:length(compIm_c)
        compIm_c{i} = compIm_c{i}.*(compIm_c{i}>0);
    end
    
    protoIm_c = cell(1,1);
    pInd = 1;
    for i = 1:inOrients:length(compIm_c)
        protoIm_c{pInd} = compIm_c{i};
        pInd = pInd + 1;
    end
    
    
    % make ProtoTypes matrix and get the filterWeighting
    [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c, weightFilters);
    
    % make Auto-Associator matrix A
    PrototypesMat = convertProto(permute(FilteredPrototypes, [2 1 3]), numChan, 'chan-to-mat'); %organize to columns
    [A] = LinSolveAuto(PrototypesMat);
    
    %make a simple GCNN Signiture classifier trained upon stimuli
    signatures = squeeze(sum(FilteredPrototypes, 2))';
    size(signatures)
    size( eye(numProto))
    [C_gcnn] = LinSolveClassifier(signatures, eye(numProto));
    
    %Make simple GCNN Image Classifier
    PrototypeVecs = reshape(FilteredPrototypes, numProto, numChan * numNodes);
    [C_lin] = LinSolveClassifier(PrototypeVecs' , eye(numProto));
    
    %imagesc(SimpleClassify(C, signatures));
    %colormap(hot);
    
    
    successes = 0;
    successesRate = 0;
    
    %Binned collection vectors for experiment
    % Pair by condition
    probePerfs = zeros(numPairs, 2); % were the probes identified
    numScores = 0;
    
    
    set(0,'CurrentFigure', orientFigure);
    barGraph = bar([0 0]);
    set(gca, 'Ylim', [0 1]);
    
    
    rotationRate = 3;
    
    disp('Please Press Enter to start the simulations');
    
    %pause();
    
    
    maskRate = tNetInfo.GCNN_influence;
    
    
    A = 4; aDir = 90;
    B = 3; bDir = 60;
    AngleTraversed = rotationRate*epochLim;
    
    
    for i = 1:numPairs
        
        
        orientA =  (i-1) * 12 + 4;
        orientB =  (i-1) * 12 + 3;
        
        %%%%%%%%%%%%%%% PROBE MATCH ROTATE TOWARDS CONGRUENT
        
        matchPrototype = compIm_c{ orientA};
        
        numStim = ceil(orientA/inOrients);
        
        % Adjust the firing rates based on the convolutional classification
        % this is a type of fusion.
        signature = squeeze(sum(sum(matchPrototype, 1),2));
        size(signature);
        [r, order] = SimpleClassify(C_gcnn, signature);
        mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
        %mask = [];
        
        [numGuess, steps, ~] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim, [], -rotationRate);
        
        
        disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

        if numGuess == numStim
            scoreProbe1 = 'Correct   : ';
            probePerfs(i,1) = 1;
        else
            scoreProbe1 = 'Incorrect : ';
        end
        
        
        %%%%%%%%%%%%%%% PROBE MATCH ROTATE AWAYFROM CONGRUENT
        
        matchPrototype = compIm_c{ orientB};
        
        numStim = ceil(orientB/inOrients);
        
        % Adjust the firing rates based on the convolutional classification
        % this is a type of fusion.
        signature = squeeze(sum(sum(matchPrototype, 1),2));
        size(signature);
        [r, order] = SimpleClassify(C_gcnn, signature);
        mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
        %mask = [];
        
        [numGuess, steps, ~] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim, [], rotationRate);
        
        
        disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

        if numGuess == numStim
            scoreProbe2 = 'Correct   : ';
            probePerfs(i,2) = 1;
        else
            scoreProbe2 = 'Incorrect : ';
        end
        
        
        % DISPLAY 
        scoreCell{i} = [' Towards ' scoreProbe1 ' Away ' scoreProbe2];
        set(scoreListBox, 'String', scoreCell);
        set(scoreListBox, 'Value', i);
        
        set(barGraph, 'ydata', [sum(probePerfs(:,1)) sum( probePerfs(:,2) )]/i)
        
        
    end
    
    HATF_Rotation.probePerfs = probePerfs;
    HATF_Rotation.aOrient = aDir;
    HATF_Rotation.bOrient = bDir;
    HATF_Rotation.AngleTraversed = AngleTraversed; 
    
    oldFolder = cd('..');
    cd Results;
    dateStr = datestr(date,'mmm_dd_yy');
    fullTime = clock;
    save(['HATF_Rotation_' dateStr '_h' num2str(fullTime(4)) '.mat'], 'HATF_Rotation');
    savefig(orientFigure ,['HATF_Rotation_' dateStr '_h' num2str(fullTime(4)) '.fig']);
    cd ..;
    cd(oldFolder);


end


