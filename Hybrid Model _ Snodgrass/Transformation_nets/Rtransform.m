function [G r rt] = Rtransform(y1, y2, x1, x2, s, SD)
%RTRANSFORM Summary of this function goes here
%   Detailed explanation goes here
    
    nz = length(y1);
    
    osz = ones(nz,1);
    
    %figure this out!!!
    Y1 = kron(y1, osz');
    Y2 = kron(y2, osz');
    X1 = kron(x1', osz);
    X2 = kron(x2', osz);
    
    
    G = ( (X1-Y1).^2 + (X2-Y2).^2 + SD ).^(-1);
    
    %Redo with using gaussian and its derivative for the attraction !!!
    W = exp( -( (X1-Y1).^2 + (X2-Y2).^2 )/1  );
    
    %figure()
    %imagesc(G);
    
    r = G*s;
    
    rt = W*s;
    
    %display thie images side by side
    %figure();
    %subplot(1,2,1); imagesc(reshape(s, nz^.5,nz^.5 ));
    %subplot(1,2,2); imagesc(reshape(r, nz^.5,nz^.5 ));
    
end

