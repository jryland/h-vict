function [ successRate] = SingleTemplateDemo( numRuns)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here


    %Graphics
    scoreFigure = figure();
    imageFigure = figure();

    scoreListBox = uicontrol('Parent', scoreFigure, 'style', 'listbox', 'Units', 'normalized',...
                    'Position', [0.1 0.1 .8 .8]);
    
    scoreCell = {};            
    
    
    successes = 0;
    successesRate = 0;
    
    pause();
    
    for i = 1:numRuns
        [matchFound match] = FitTest(imageFigure);
        
        if matchFound==match
            successes = successes + 1;
            score = 'Correct   : ';
        else
            score = 'Incorrect : ';
        end
        
        successRate = successes/i*100;
        
        scoreCell{i} = [score mat2str(successRate) '%'];
        set(scoreListBox, 'String', scoreCell);
        set(scoreListBox, 'Value', i);
        
    end
    



end

