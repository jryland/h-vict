function [] = TestFilters(  )

    %[s o]= MakeCross(15);
    
    %load clown;
    %lf = fspecial('log');
    
    %[sx1 sx2] = size(X);
    
    %clown = imresize(X, .2);
    %clown1 = convn( clown/256 , lf);
    %clown2 = imrotate(clown1, 20, 'bilinear', 'crop');
    
    sz = 20;
    
    [s o match sn] = MakeStimLetter(sz);
    
    disp('stimpuli created');
    
    Simg = reshape(s, sz, sz);
    Oimg = reshape(o, sz, sz);
    gf = fspecial('Gaussian');
    Simg = convn(Simg, gf, 'same');
    Oimg = convn(Oimg, gf, 'same');
    
    %Oimg = convn(clown1,gf, 'same');
    %Simg = convn(clown2,gf, 'same');
    
    %filt = MakeFilt3(1);
    filt = fspecial('log', 5, .5); 
    %filt = fspecial('motion', 9, 45);
    filt = (filt-mean(filt(:)))/sum(abs(filt(:)));
    f1 = MakeFilt360(filt, 180);
    [OfImg(:,:,1) OorImg(:,:,1)] = ApplyFilt360(Oimg, f1);
    [SfImg(:,:,1) SorImg(:,:,1)] = ApplyFilt360(Simg, f1);
    
    %filt = MakeFilt3(1);
    filt = fspecial('motion', 9, 45);
    filt = (filt-mean(filt(:)))/sum(abs(filt(:)));
    %filt = fspecial('log', 5, .5); 
    f2 = MakeFilt360(filt, 180);
    [OfImg(:,:,2) OorImg(:,:,2)] = ApplyFilt360(Oimg, f2);
    [SfImg(:,:,2) SorImg(:,:,2)] = ApplyFilt360(Simg, f2);
    
    %filt = MakeFilt3(1);
    ex = fspecial('log', 5, .5);
    %filt = fspecial('motion', 9, 45);
    
    filt = convn(fspecial('disk', 5),ex, 'full');
    filt = (filt-mean(filt(:)))/sum(abs(filt(:)));
    f3 = MakeFilt360(filt, 180);
    [OfImg(:,:,3) OorImg(:,:,3)] = ApplyFilt360(Oimg, f3);
    [SfImg(:,:,3) SorImg(:,:,3)] = ApplyFilt360(Simg, f3);
    
    m = max([OfImg(:); SfImg(:)])
    
    
    
    OfImg = OfImg/m;
    SfImg = SfImg/m;
    
    
    figure();
    %subplot(2,3,1); imagesc(Oimg); daspect([1 1 1]);
    subplot(1,2,1); image(OfImg(:,:,:)); daspect([1 1 1]);
    %subplot(2,3,3); image(OorImg);   daspect([1 1 1]);
    
   %subplot(2,3,4); imagesc(Simg); daspect([1 1 1]);
    subplot(1,2,2); image(SfImg(:,:,:)); daspect([1 1 1]);
    %subplot(2,3,6); image(SorImg);   daspect([1 1 1]);
    

end

