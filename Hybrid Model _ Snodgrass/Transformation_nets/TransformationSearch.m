function [ gn, i, yParams, fastMatchSteps, sim_max ] = TransformationSearch( imageFigure, V_filts, AutoMat, stim_image, filterWeighting, mask, C_lin, sz, epochLim, yParams, spinExp, variantInfo)

    span = variantInfo.span;

    if isempty(imageFigure)
        imageFigure = figure();
    end
    
    set(0,'CurrentFigure', imageFigure);
    
    numFilters = size(stim_image, 3);
    
    Simg_filt = stim_image;
    
    %applying filter weighting
    for i = 1:size(filterWeighting,1)
        Simg_filt(:,:,i) = Simg_filt(:,:,i)*filterWeighting(i);
    end
    
    S_filt = reshape(Simg_filt, sz*sz, numFilters);
    
    
    Simg_filt = Simg_filt/max(Simg_filt(:));
    S_filt = S_filt/sum(S_filt(:))*numFilters;
    
    
    gn = 0;
    cgn_last = gn;
    stay_count = 0;
    singleRounds = 0;
    
    p = ones(size(V_filts,1),1);
    p = p/size(p,1);
    p_sim = ones(size(p))/size(p,1);
    timeout = ones(size(p));
    singleTest = ones(size(p));
    
    if isempty(mask)
        mask = ones(size(p));
    end
    
    pID = [];
    if strcmp(span,'span')
        mask = kron(mask, ones(3,1));
    elseif strcmp(span,'dist')
        pID = variantInfo.protoIm_ID;
        max(pID)
        size(mask)
        mask = mask(pID); % THINK ABOUT THIS
    end
    
    
    %p = protoWeighting;
    
    [y1, y2, Bvec, Nvec] = MakeGrid(sz);
    
    %setup input references
    x1 = y1;
    x2 = y2;
    
    % Setup old transformation Parameters
    if ~isempty(yParams)
        disp('USING OLD TRANSFORMATION PARAMETERS');
        y1 = yParams.y1;
        y2 = yParams.y2;
    end
    
       
    %Global Important Parameters
    c_neighb = 1;
    sigma = .06;
    eta   = 2; % 600
    thetaSpeed = 1;
    if(variantInfo.centerOnly)
        thetaSpeed = 0;
    end
    strength = .20; %.20
    gamma = .5;
    dMax = .017;
    dMax_dp = .2;
    pNoise = 0;
    SD =  1.5; %.75;  %1.6; % .75;
    Criterion = .96; % cosine similarity criterion
    Criterion2 = .91; % Cutoff for an early match
    matchFound = 0;
    sim_max = 0;
    sim_max_last = 0;
    thetaMoment = 0;
    stayCap =  epochLim; %epochLim; %10 20 are good epochLim is equevolent to none
    singleRoundCap = 0; % if 
    
    maskRate_lin = 0;
    
    fastMatch = 0;
    fastMatchSteps = epochLim;
    
    pauseLength = .001;
    
    %maximum similarity and error match jugements
    mMaxs = zeros(size(p));
    p_sim_max = zeros(size(p));
    
 
    [G r rt] = RtransformAll(y1, y2, x1, x2, S_filt, SD);
    O_filt = O_allFromP(V_filts, p_sim);
    
    
    etaBoost = 1; 
    
    %Transform all of V_filts to increase similarity judgements
    for i=1:length(p)
        v_filt = squeeze(V_filts(i,:,:));
        [G r rt] = RtransformAll(x1, x2, x1, x2, v_filt, SD);
        V_filts(i,:,:) = rt/norm(rt(:));
    end
    
    
    
    % graphics Stuff
    set(0,'CurrentFigure', imageFigure);
    %subplot(2,4,1); SimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]); 
    %subplot(2,4,2); RimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]);
    %subplot(2,4,4); OimgHandle = image(Simg_filt(:,:,1:3)); daspect([1 1 1]);
    
    ax1 = subplot(2,4,1,'Parent', imageFigure); dispRecodedIm(Simg_filt(:,:,:), 'multi-chan', ax1); daspect([1 1 1]); 
    ax2 = subplot(2,4,2,'Parent', imageFigure); dispRecodedIm(Simg_filt(:,:,:), 'multi-chan', ax2); daspect([1 1 1]);
    ax4 = subplot(2,4,4,'Parent', imageFigure); dispRecodedIm(Simg_filt(:,:,:), 'multi-chan', ax4); daspect([1 1 1]);
    
    ax3 = subplot(2,4,3,'Parent', imageFigure); scatH = scatter(ax3, y1, y2); daspect([1 1 1]);
    hold on;
    scatH2 = scatter(x1, x2);
    hold off;
    ax567 = subplot(2,4,5:7,'Parent', imageFigure); PimgHandle = image(zeros(sz*4,sz*size(p,1),3)); daspect([1 1 1]);
    ax8 = subplot(2,4,8,'Parent', imageFigure); WinimgHandle = image(zeros(sz,sz,3)); daspect([1 1 1]);
    
    
    sim_max = 0; 
    
    pause(.02);
    
    S_filt_Original = S_filt;
    
    fit = 1000;
    
    i = 0;
    
    for i=1:epochLim
        
        
        if ~isempty(spinExp)
            Simg_filt = reshape(S_filt_Original, sz, sz, numFilters);
            Simg_filt = imrotate(Simg_filt, spinExp*i, 'bicubic','crop');
            S_filt = reshape(Simg_filt, sz*sz, numFilters);
        end
        
        
        % Here is where we left off!
        [G, r, rt, dBeta_dy1, dBeta_dy2, dTrans_dy1, dTrans_dy2, ~]  = ...
            FindDerivatives(y1, y2, x1, x2, S_filt, SD, Bvec, c_neighb, Nvec, V_filts, p_sim, O_filt, thetaSpeed, eta, sz);
        
        %apply speed limits
%         dy_norm = norm([dTrans_dy1(:); dTrans_dy2(:)]);
%         if dy_norm>dMax
%             dTrans_dy1 = dTrans_dy1/dy_norm*dMax;
%             dTrans_dy2 = dTrans_dy2/dy_norm*dMax; 
%         end
        
        
        %Perform Gradient Descent on transformation Parameters
        if ~variantInfo.transformFreeze;
            y1 = y1 - sigma*dBeta_dy1' - (eta*dTrans_dy1');%/norm(dTrans_dy1))';
            y2 = y2 - sigma*dBeta_dy2' - (eta*dTrans_dy2');%/norm(dTrans_dy2))';
        end
        
        %Get reconstruction from auto-associative matrix
        %a = A_fromRt(AutoMat, rt);
        
        %Get GCNN image classifier guess
        rt_vec = rt(:);
        [p_lin, order] = SimpleClassify(C_lin, rt_vec);
        mask_lin = (p_lin-min(p_lin))/(max(p_lin)-min(p_lin))*(maskRate_lin)+(1-maskRate_lin);
        
        %prototype activation from similarity to transformed input 
        p_sim = P_allFromRt(V_filts, rt);
        p_sim = p_sim.*timeout.*mask.*mask_lin.*singleTest;
        %p_sim = P_allFromRt(V_filts, a);
        
        
        %calculate prototype error match to transformed input
        m = MfromRt_all(V_filts, S_filt);
        
        %update the maximum similarity judgements
        mMaxs = max(m, mMaxs);
        p_sim_max = max(p_sim_max, p_sim);
        
        %update target image 
        O_filt = etaBoost * strength * O_allFromP(V_filts, p_sim); %+ 1*a;
        O_filt_pos = O_filt;
        %O_filt = a; % remove negative values to prevent repulsion
        %O_filt_pos = a.*(a>0);
        
        % Graphics stuff
        Simg_filt = reshape(S_filt, sz, sz, numFilters);
        Oimg_filt = reshape(O_filt_pos, sz, sz, numFilters);
        Rtimg_filt= reshape(rt, sz, sz, numFilters);
        Simg_filt = Simg_filt/max(Simg_filt(:));
        Oimg_filt = Oimg_filt/max(Oimg_filt(:));
        Rtimg_filt = Rtimg_filt/max(Rtimg_filt(:));
        
        %set(SimgHandle, 'CData', Simg_filt(:,:,1:3));
        %set(RimgHandle, 'CData', Rtimg_filt(:,:,1:3));
        %set(OimgHandle, 'CData', Oimg_filt(:,:,1:3));
        set(0,'CurrentFigure', imageFigure);
        dispRecodedIm(Simg_filt(:,:,:), 'multi-chan', ax1); %daspect([1 1 1]);
        set(0,'CurrentFigure', imageFigure);
        dispRecodedIm(Rtimg_filt(:,:,:), 'multi-chan', ax2); %daspect([1 1 1]);
        set(0,'CurrentFigure', imageFigure);
        dispRecodedIm(Oimg_filt(:,:,:), 'multi-chan', ax4); %daspect([1 1 1]);
        set(scatH , 'XData', y1);
        set(scatH , 'YData', y2);
        
        %decide on a winner previously used 
        sim_max = max(p_sim_max);
        win = find(p_sim_max==max(p_sim_max));
        win = win(1);
        winVec = zeros(size(p_sim_max));
        winVec(win) = 1;
        gn = win;
        if strcmp(span,'span')
            gn = floor((win-1)/3)+1;
        elseif strcmp(span,'dist')
            gn = pID(win);
        end
        
        %decide current winner
        currentSimMax = max(p_sim);
        currentWin = find(p_sim==currentSimMax);
        currentWin = currentWin(1);
        
        cgn = currentWin;
        
        
        % if transform stops turn off current winner
        %dy_norm = norm([dTrans_dy1(:); dTrans_dy2(:)]);
        %if dy_norm < .03
        %    disp('Transfrom Stopped');
        %    timeout(cgn) = 0;
        %end
        
        
        % if a prototype is winning for too long it is put in timeout
        % after it gets a limited single activation
        if singleRounds == 0
            
            singleTest(:) = 1;
            
            if sim_max_last == sim_max
                stay_count = stay_count + 1;
            else
                stay_count = 0;
            end
            if stay_count>stayCap
                
                singleRounds = singleRoundCap+1;
            
            end
            
        elseif singleRounds > 0
            
            singleTest(:) = 0;
            singleTest(cgn) = 1;
            
            singleRounds = singleRounds-1;
            
            etaBoost = 20;
            
            if singleRounds == 0
                disp(['Timout ProtoID: ' mat2str(cgn)]);
                timeout(cgn) = 0;
                stay_count = 0;
                etaBoost = 1;
            end
            
            
            
        end
        
        
        
        recover = .00;
        timeout = ones(size(timeout))*(recover) + (1-recover)*timeout;
        
        cgn_last = cgn;
        sim_max_last = sim_max;
        
        % Show the winning prototype
        Winimg = reshape(V_filts(win(1),:,:), sz, sz, numFilters);
        Winimg = Winimg/max(Winimg(:));
        %set(WinimgHandle, 'CData', Winimg(:,:,1:3));
        set(0,'CurrentFigure', imageFigure);
        dispRecodedIm(Winimg(:,:,:), 'multi-chan', ax8); %daspect([1 1 1]);
        
        
        p_sim_pos = p_sim.*(p_sim>0);
        
        %Create indicators of Activity Due to Attraction Principles
        Img_activity = reshape(kron(ones(sz*sz,numFilters),p_sim_pos'), sz, sz*size(p_sim,1), numFilters );
        
        Img_activity_sim = reshape(kron(ones(sz*sz,numFilters),p_sim_max'), sz, sz*size(p_sim_max,1), numFilters );
        
        ProtoImgs = reshape( permute(V_filts, [2 1 3]) , sz, sz*size(p_sim,1), numFilters);
        
        Activity_Display = Img_activity.*ProtoImgs;
        Activity_Display = Activity_Display/max(Activity_Display(:));
        
        interval = max(p_sim_pos) - min(p_sim_pos);
        
        Img_activity_rel = (Img_activity-min(p_sim_pos))/interval;
        
        Img_activity_sim = Img_activity_sim/max(p_sim_max);
        
        Img_activity_rel(:,:,1) = Img_activity_rel(:,:,1).*(Img_activity_rel(:,:,1)<(mean(Img_activity_rel(:))+.25));
        
        Activity_Display_rel = Img_activity_rel.*ProtoImgs;
        Activity_Display_rel = Activity_Display_rel/max(Activity_Display_rel(:));
        
        
        %create indicators of Activity Due to Error Minimization Principles
        %interval_m = max(m) - min(m);
        
        %Img_activity_m = reshape(kron(ones(sz*sz,numFilters),m'), sz, sz*size(m,1), numFilters );
        
        %Img_activity_m_rel = (Img_activity_m-min(m))/interval_m;
        
        %Img_activity_m_rel(:,:,3) = Img_activity_m_rel(:,:,3).*(Img_activity_m_rel(:,:,3)<(mean(Img_activity_m_rel(:))+.25));
        
        
        %Display all Activity Indicators
        set(0,'CurrentFigure', imageFigure);
        set(PimgHandle, 'CData', [Activity_Display(:,:,1:3); Activity_Display_rel(:,:,1:3); Img_activity_rel(:,:,1:3); Img_activity_sim(:,:,1:3)]);
        
        
        fit = sum((O_filt(:)-rt(:)).^2);
        disp(['Epoch = ' mat2str(i) ' || Fit = ' mat2str(fit) ' || Max Cos = ' mat2str(sim_max) ' || Cur Cos = ' mat2str(max(p_sim))]);  % '|| Mag O = ' mat2str(norm(o)) ]);
        
        %disp(['a = ' num2str( sum(O_filt(:).*r(:)) )]);
        
        
        pause(pauseLength);
        
        if sim_max>(Criterion2)&&(~fastMatch)
            fastMatch = 1;
            fastMatchSteps = i;
            disp('Early Match');
        end
        
        if sim_max>(Criterion)
            matchFound = 1;
            disp('Match');
            break;
        end
        
    end
    
    if sim_max<(Criterion)
        disp('No-match');
    end
    
    %delete(imageFigure);
    
    yParams.y1 = y1;
    yParams.y2 = y2;

end



