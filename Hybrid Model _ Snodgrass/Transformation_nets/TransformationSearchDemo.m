function [ HVICT_performance ] = TransformationSearchDemo(variantInfo, inputFolderName, inputInfo, tNetInfo, epochLim)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here


    %Graphics
    scoreFigure = figure();
    imageFigure = figure();
    orientFigure = figure();
    
    
    scoreListBox = uicontrol('Parent', scoreFigure, 'style', 'listbox', 'Units', 'normalized',...
                    'Position', [0.1 0.1 .8 .8]);
    
    scoreCell = {};       
    
    disp('Creating learned prototype matrix: Please Wait');
    
    
    % load a set of base or hidden representations
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    span = variantInfo.span;
    Dist = variantInfo.dist;
    
    weightFilters =  'none'; %'inverse-frequency'; %'none'; % 'inverse-frequency'; %'inverse-frequency';
    
    compIm_c = loadInputFrom(inputFolderName, inRez, inNum, inStyle, inHidden);
    %compIm_c = compIm_c(1:inNum);
    numS = length(compIm_c);
    numChan = size(compIm_c{1}, 3);
    
    numProto = numS/inOrients;
    numNodes = inRez.^2;
    
    
    %remove negative values (don't know why they are there to begin with)
    for i = 1:length(compIm_c)
        compIm_c{i} = compIm_c{i}.*(compIm_c{i}>0);
    end
    
    %disp(size(compIm_c))
    
    % Get prototypes
    protoIm_c = cell(1,1);
    protoIm_c_p30 = cell(1,1);
    protoIm_c_n30 = cell(1,1);
    protoIm_c_dist = cell(1,1);
    protoIm_ID = [];
    pInd = 1;
    pInd3 = 1;
    pIndD = 1;
    for i = 1:inOrients:length(compIm_c)
        
        % Setup Single and flanking
        protoIm_c{pInd} = compIm_c{i};
        protoIm_c_p30{pInd} = compIm_c{i+1};
        protoIm_c_n30{pInd} = compIm_c{i+11};
        pInd = pInd + 1;
        
        % Setup Span option
        protoIm_c_span{pInd3} = compIm_c{i};    pInd3 = pInd3+1;
        protoIm_c_span{pInd3} = compIm_c{i+1};  pInd3 = pInd3+1;
        protoIm_c_span{pInd3} = compIm_c{i+11}; pInd3 = pInd3+1;
        
        % Setup Dist option
        for j = 0:11
            if rand(1,1)<Dist(j+1)
                pIndD
                protoIm_c_dist{pIndD} = compIm_c{i+j};
                protoIm_ID(pIndD) = pInd-1;
                pIndD = pIndD+1;
            end
        end
        
    end
    
    % make ProtoTypes matrix and get the filterWeighting 0-Deg
    [FilteredPrototypes0, filterWeighting0, protoWeighting0] = MakeFilteredPrototypes(protoIm_c, weightFilters);
    
    % Choose set of prototypes to send to the v and t models
    FilteredPrototypes = []; filterWeighting = []; protoWeighting = [];
    if strcmp(span, 'single')
        FilteredPrototypes = FilteredPrototypes0;
        filterWeighting = filterWeighting0;
        protoWeighting = protoWeighting0;
        numProto2 = numProto;
    elseif strcmp(span, 'span')
        
        [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c_span, weightFilters);
        numProto2 = numProto*3;
    
    elseif strcmp(span, 'dist')
        
        [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c_dist, weightFilters);
        numProto2 = length(protoIm_c_dist);
        variantInfo.protoIm_ID = protoIm_ID;
    end
    
    
    % ProtoTypes mat for +30 and -30 deg
    [FilteredPrototypesP30, filterWeightingP30, protoWeightingP30] = MakeFilteredPrototypes(protoIm_c_p30, weightFilters);
    [FilteredPrototypesN30, filterWeightingN30, protoWeightingN30] = MakeFilteredPrototypes(protoIm_c_n30, weightFilters);
    
    
    % make Auto-Associator matrix A
    PrototypesMat = convertProto(permute(FilteredPrototypes, [2 1 3]), numChan, 'chan-to-mat'); %organize to columns
    [A] = LinSolveAuto(PrototypesMat);
    
    %make a simple GCNN Signiture classifier trained upon stimuli
    FilteredPrototypesFlanking = (FilteredPrototypes0+FilteredPrototypesP30+FilteredPrototypesN30)/3;
    signatures = squeeze(sum(FilteredPrototypesFlanking, 2))';
    size( eye(numProto))
    
    [C_gcnn] = LinSolveClassifier(signatures, eye(numProto));
    
    %Make simple GCNN Image Classifier
    PrototypeVecs = reshape(FilteredPrototypes, numProto2, numChan * numNodes);
    [C_lin] = LinSolveClassifier(PrototypeVecs' , eye(numProto2));
    
    %imagesc(SimpleClassify(C, signatures));
    %colormap(hot);
    
    
    successes = 0;
    successesRate = 0;
    
    %Binned collection vector for experiment
    numScores = zeros(1, inOrients+1);
    numCorrect = zeros(1, inOrients+1);
    numSteps = zeros(1, inOrients+1);
    numSteps2 = zeros(1, inOrients+1);
    simsTotal = zeros(1, inOrients+1);
    
    percOrient = zeros(1, inOrients+1);
    stepsOrient = zeros(1, inOrients+1);
    stepsOrient2 = zeros(1, inOrients+1);
    simOrient = zeros(1, inOrients+1);
    itemOrientPerf = zeros(numS/inOrients, inOrients);
    itemOrientSteps = zeros(numS/inOrients, inOrients);
    itemOrientSteps2 = zeros(numS/inOrients, inOrients);
    itemOrientSim = zeros(numS/inOrients, inOrients);
    
    degInc = 360/inOrients;
    orients = 0:(degInc):(360); %-degInc);
    set(0,'CurrentFigure', orientFigure);
    
    subplot(2,1,1);
    %axes(axH);
    lineHandle = line('YData', percOrient, 'XData', orients);
    set(gca, 'Ylim', [0 1], 'Xlim', [0 360]);
    
    subplot(2,1,2);
    lineHandle2 = line('YData', stepsOrient, 'XData', orients);
    set(gca, 'Ylim', [0 50], 'Xlim', [0 360]);
    
    hold on;
    lineHandle3 = line('YData', stepsOrient2, 'XData', orients);
    %set(gca, 'Ylim', [0 50], 'Xlim', [0 360]);
    hold off;
    
    hold on;
    lineHandle4 = line('YData', simOrient, 'XData', orients);
    %set(gca, 'Ylim', [0 50], 'Xlim', [0 360]);
    hold off;
    
    pause(.1);
    
    disp('Please Press Enter to start the simulations');
    
    maskRate = tNetInfo.GCNN_influence;
    
    %pause();
    
    for i = 1:numS
        
        matchPrototype = compIm_c{i};
        

        
        % Adjust the firing rates based on the convolutional classification
        % this is a type of fusion.
        signature = squeeze(sum(sum(matchPrototype, 1),2));
        sgsz = size(signature);
        
        [r, order] = SimpleClassify(C_gcnn, signature);
        mask = [];
        
        if ~variantInfo.invariantOnly
            mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
            
        else    % Added for variants
            maskBinary = zeros(size(r));
            maskBinary(r==max(r)) = 1;
            mask = maskBinary.*r;
            
        end
        
        
        [numGuess, steps, ~, steps2, sim_max] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim, [],[], variantInfo);
        
        
        numStim = ceil(i/inOrients);
        
        
        disp([variantInfo.varTitle ' || ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

        orient = mod(i, inOrients);
        orient = orient+inOrients*(orient==0);
        numScores(1,orient) = 1 + numScores(1,orient);
        numSteps(1,orient) = steps + numSteps(1,orient);
        numSteps2(1,orient) = steps2 + numSteps2(1,orient);
        if ~variantInfo.invariantOnly
            simsTotal(1,orient) = sim_max + simsTotal(1,orient);
        else
            simsTotal(1,orient) = max(mask) + simsTotal(1,orient);
        end
        
        
        if numGuess == numStim
            successes = successes + 1;
            score = 'Correct   : ';
            numCorrect(1,orient) = 1 + numCorrect(orient);
            itemOrientPerf(numStim, orient) = 1;
        else
            score = 'Incorrect : ';
        end
        
        itemOrientSteps(numStim, orient) = steps;
        itemOrientSteps2(numStim, orient) = steps2;
        itemOrientSim(numStim, orient) = sim_max;
        if ~variantInfo.invariantOnly
            itemOrientSim(numStim, orient) = sim_max;
        else
            itemOrientSim(numStim, orient) = max(mask);
        end
        
        successRate = successes/i*100;
        
        scoreCell{i} = [score mat2str(successRate) '%'];
        set(scoreListBox, 'String', scoreCell);
        set(scoreListBox, 'Value', i);
        
        percOrient = numCorrect./numScores;
        percOrient(end) = percOrient(1);
        percOrient(isinf(percOrient)) = 0;
        
        stepsOrient = numSteps./numScores;
        stepsOrient(end) = stepsOrient(1);
        stepsOrient(isinf(stepsOrient)) = 0;
        
        stepsOrient2 = numSteps2./numScores;
        stepsOrient2(end) = stepsOrient2(1);
        stepsOrient2(isinf(stepsOrient2)) = 0;
        
        simOrient = simsTotal./numScores;
        simOrient(end) = simOrient(1);
        simOrient(isinf(simOrient)) = 0;
        
        
        set(0,'CurrentFigure', orientFigure);
        set(lineHandle, 'YData', percOrient, 'XData', orients);
        set(lineHandle2, 'YData', stepsOrient, 'XData', orients);
        set(lineHandle3, 'YData', stepsOrient2, 'XData', orients);
        
        %ylim = get(lineHandle4, 'ylim');
        
        interval= max(simOrient)-min(simOrient);
        scaledSimOrient = (simOrient-min(simOrient))/(interval);
        
        set(lineHandle4, 'YData',scaledSimOrient*40, 'XData', orients);
        
        
    end
    
    
    
    HVICT_performance.percOrient = percOrient;
    HVICT_performance.orients = orients;
    HVICT_performance.stepsOrient = stepsOrient;
    HVICT_performance.itemOrientPerf = itemOrientPerf;
    HVICT_performance.itemOrientSteps = itemOrientSteps;
    HVICT_performance.itemOrientSteps2 = itemOrientSteps2;
    HVICT_performance.itemOrientSim = itemOrientSim;
    
    
    addpath(pwd)
    oldFolder = cd('..');
    cd Runs;
    
    makeExp12R(HVICT_performance, variantInfo.saveAs, variantInfo.run);
    
    dateStr = datestr(date,'mmm_dd_yy');
    fullTime = clock;
    %save(['Run' num2str(variantInfo.run) variantInfo.saveAs '.mat'], 'HVICT_performance');
    %savefig(orientFigure ,['Run' num2str(variantInfo.run) variantInfo.saveAs '.fig']);
    cd ..;
    cd(oldFolder);


end


