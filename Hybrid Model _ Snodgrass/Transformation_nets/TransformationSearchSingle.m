function [ successRate] = TransformationSearchSingle(variantInfo,inputFolderName, inputInfo, tNetInfo, epochLim, item, itemOrient)
%SINGLETEMPLATEDEMO Summary of this function goes here
%   Detailed explanation goes here


    %Graphics
    imageFigure = figure();
    
    
    disp('Creating learned prototype matrix: Please Wait');
    
    
    % load a set of base or hidden representations
    inRez = inputInfo.inRez;        % the rezolution to scale the stimuli to be
    inNum = inputInfo.inNum;        % the number of stimuli to be used
    inStyle = inputInfo.inStyle;    % the style of the input images
    inSample = inputInfo.inSample;    % how the images are sampled
    inHidden = inputInfo.inHidden;  % whether or not the input is a hidden image
    inOrients = inputInfo.numOrients;
    
    weightFilters =  'none'; % 'inverse-frequency'; %'inverse-frequency';
    
    compIm_c = loadInputFrom(inputFolderName, inRez, inNum, inStyle, inHidden);
    compIm_c = compIm_c(1:inNum);
    numS = length(compIm_c);
    numChan = size(compIm_c{1}, 3);
    
    numProto = numS/inOrients;
    numNodes = inRez.^2;
    
    
    %remove negative values (don't know why they are there to begin with)
    for i = 1:length(compIm_c)
        compIm_c{i} = compIm_c{i}.*(compIm_c{i}>0);
    end
    
    protoIm_c = cell(1,1);
    pInd = 1;
    for i = 1:inOrients:length(compIm_c)
        protoIm_c{pInd} = compIm_c{i};
        pInd = pInd + 1;
    end
    
    
    % make ProtoTypes matrix and get the filterWeighting
    [FilteredPrototypes, filterWeighting, protoWeighting] = MakeFilteredPrototypes(protoIm_c, weightFilters);
    
    % make Auto-Associator matrix A
    PrototypesMat = convertProto(permute(FilteredPrototypes, [2 1 3]), numChan, 'chan-to-mat'); %organize to columns
    [A] = LinSolveAuto(PrototypesMat);
    
    %make a simple GCNN Signiture classifier trained upon stimuli
    signatures = squeeze(sum(FilteredPrototypes, 2))';
    size(signatures)
    size( eye(numProto))
    [C_gcnn] = LinSolveClassifier(signatures, eye(numProto));
    
    %Make simple GCNN Image Classifier
    PrototypeVecs = reshape(FilteredPrototypes, numProto, numChan * numNodes);
    [C_lin] = LinSolveClassifier(PrototypeVecs' , eye(numProto));
    
    %imagesc(SimpleClassify(C, signatures));
    %colormap(hot);
    
    
    successes = 0;
    successesRate = 0;
   
    disp('Please Press Enter to start the simulations');
    
    maskRate = tNetInfo.GCNN_influence;
    
    %pause();
    
    i = (item-1)*12 + itemOrient;
        
    matchPrototype = compIm_c{i};

    % Adjust the firing rates based on the convolutional classification
    % this is a type of fusion.
    signature = squeeze(sum(sum(matchPrototype, 1),2));
    size(signature);
    [r, order] = SimpleClassify(C_gcnn, signature);
    mask = (r-min(r))/(max(r)-min(r))*(maskRate)+(1-maskRate);
    %mask = [];

    [numGuess, steps, ~, steps2] = TransformationSearch(imageFigure, FilteredPrototypes, A, matchPrototype, filterWeighting, mask, C_lin, inRez, epochLim, [],[],variantInfo);

    numStim = ceil(i/inOrients);


    disp(['ProtoID = ' mat2str(numStim) ' || GuessID =' mat2str(numGuess)] );

    
    if numGuess == numStim
        successes = successes + 1;
        score = 'Correct   : ';
    else
        score = 'Incorrect : ';
    end

    
    
    

end
