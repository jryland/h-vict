function [ conjIm, conjMat ] = conjunctionIm( im,  conjMat)
% This function looks for conjunctions of features in a multi channel image
% this could make it much easier for a transformational network to do
% matching since the hotspots of the conjunction image will be more unique
% with respect to location and channel.


    numChan = size(im, 3);
    rez = size(im,1);
    
    % by default it will make one of the same size using a random
    % permutation.
    if isempty(conjMat)
        chans1 = (1:numChan)';
        chans2 = randperm(numChan)';
        
        conjMat = [chans1 chans2];
    end
    

    numConj = size(conjMat, 1);
    
    
    conjIm = zeros(rez,rez, numConj);
    
    for i = 1:numConj
        if conjMat(i,1)~=conjMat(i,2)
            conjIm(:,:,i) = im(:,:,conjMat(i,1)).*im(:,:,conjMat(i,2));  
        end
        
    end
    

end

