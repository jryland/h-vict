function [ X ] = convertProto( X, numChan, to )
% designed to take stimuli in columns

    %numChan
    %X_size = size(X)

    if strcmp(to, 'chan-to-mat')
        numPix = size(X,1)
        numIm = size(X,2)
        X = permute(X, [1 3 2]);
        X = reshape(X, numChan*numPix, numIm);
    
    elseif strcmp(to, 'mat-to-chan')
        numPixXnumChan = size(X,1);
        numPix = numPixXnumChan/numChan;
        numIm = size(X,2);
        X = reshape(X, numPix, numChan, numIm);
        X = ipermute(X, [1 3 2]);
        
    elseif strcmp(to, 'chan-to-vec')
        numPix = size(X,1);
        X = reshape(X, numChan*numPix, 1);
        
    elseif strcmp(to, 'vec-to-chan')
        numPixXnumChan = size(X,1);
        numPix = numPixXnumChan/numChan;
        X = reshape(X, numPix, numChan);
    
    else
        disp('Failed To specify conversion Type');
        X = [];
    end
    
    
end

