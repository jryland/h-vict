function [ handles] = dispRecodedIm( recodedHiddenIm, recodeStyle, ax)
%DISPRECODEDIM displays a recoded hidden image in a specialized manner
%using lines and arrows to indicate the directionality of features.


%recoded (imrez, imrez, numParam*numRep, 1, 1, 1,);


    if strcmp(recodeStyle, 'vector')
        
        numParam = 4;

        imRez = size(recodedHiddenIm, 1);
        numRep = size(recodedHiddenIm, 3)/numParam;

        handles = zeros(numRep,1);

        [posX, posY] = meshgrid(1:imRez, 1:imRez);

        featureVec = reshape(recodedHiddenIm(:, :, :), [imRez*imRez numRep*numParam]);

        colors = {[1 0 0], [0 1 0], [0 0 1], [1 1 0], [0 1 1], [1 0 1]};

        maxFields = 5;

        cla;
        set(gca,'YDir','reverse')
        hold on;
        for r = 0:min((numRep-1), maxFields)
            handles(r+1) = quiver(posX(:), posY(:), featureVec(:,r*numParam+1), featureVec(:,r*numParam+2), 'color',  colors{r+1});

            %handles(r+1) = quiver(posX(:), posY(:), featureVec(:,r*numParam+3), featureVec(:,r*numParam+4), 'color',  colors{r+1} );  
        end
        hold off;

        set(gca, 'Color', 'black');
        daspect([1 1 1]);
        
    elseif strcmp(recodeStyle, 'multi-chan')
        
        
        numChan = size(recodedHiddenIm, 3);
        
        colors = [1 .5 .5;
                  .5 1 .5;
                  .5 .5 1;
                  1 1 0;
                  0 1 1;
                  1 0 1;
                  1 0 0;
                  0 1 0;
                  0 0 1;
                  1 1 .5;
                  .5 1 1;
                  1 .5 1;
                  .2 .8 1;
                  1 .8 .2;
                  .8 1 .2;
                  1 .8 .2;
                  .2 1 .8;
                  .8 .2 1;
                    ];
                
                
        if numChan>16
            rng(1000000020);
            colors = rand(numChan,3);
            rng('shuffle');
            
            maxCol = max(colors,[], 2);
            colors = mdaTimes(colors, 1./maxCol, [], 'yes');
        end
        
        colorMDA(1, 1, :, :) = colors(1:numChan, :); 
        
        recodedRepSum = mdaTimes(sum(recodedHiddenIm>0, 3), ones(1,1,3), [], 'yes');
        
        colorSums = permute(mdaTimes(recodedHiddenIm, colorMDA, 3,'yes'), [1 2 4 3]);
        
        dispIm = (colorSums./recodedRepSum).*(recodedRepSum~=0) ;
        
        if sum(isinf(dispIm(:))>0)
                   
            percent = sum(isinf(dispIm(:)))/length(dispIm(:))*100;
            disp(['Infinity Detected: ' num2str(percent) '%']);
            
        end
        
        dispIm(isinf(dispIm)) = 0;
        
        
        dispIm = max(dispIm, 0)/max(abs(dispIm(:)));
        
        %min(dispIm(:))
        %max(dispIm(:))
        
        cla(ax);
        handles = imagesc(dispIm, 'parent', ax);
        daspect(ax, [1 1 1]);
        
    end

end

