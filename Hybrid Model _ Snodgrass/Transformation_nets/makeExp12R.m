function [  ] = makeExp12R( HVICT_performance, varPrefix, run )

    fullMat =  [HVICT_performance.itemOrientPerf;...
                HVICT_performance.itemOrientSteps;...
                HVICT_performance.itemOrientSteps2;...
                HVICT_performance.itemOrientSim;...
                ];
        
    fullMat = [fullMat, fullMat(:,1)];
    
    
    orientNames = cell(1,13);
    for i=1:13
        orientNames{i} = ['r' num2str(HVICT_performance.orients(i))];
    end
    
    for i=1:(150*4)
        stimNames{i} = ['i' num2str(i)];
    end
    
    
    fullTable = array2table(fullMat, 'VariableNames', orientNames); % , 'RowNames', stimNames);
    
    %set(fullTable, 'VariableNames', orientNames)

    writetable(fullTable, ['Run' num2str(run) varPrefix 'exp12.txt']);
    

end

