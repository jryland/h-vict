Copyright [2018] [James W. Ryland]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


# README #

### What is this repository for? ###

* This repository is an implementation of the H-VICT model of object recognition
* 1.00

### Setup Requirements ###

* Matlab 2013
* Matlab Parrallel Computing Toolbox with GPU support
* CUDA
* To run read the more detailed instructions in /Hybrid Model_Snodgrass/ReadMe
* If configured correctly you can just run _Demo_Full.m_
* Has only been tested on Mac OS

### Contribution guidelines ###

* Do Not Contribute
* Feel free to clone and make your own version
* This is a preserved copy for research and replication

### Who do I talk to? ###

* james.w.ryland@gmail.com
